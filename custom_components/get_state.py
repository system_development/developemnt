#!/usr/bin/env python
import requests
import json
import time

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'get_state'
ATTR_NAME = 'entity_id'


def setup(hass,config=None):
    """Set up is called when Home Assistant is loading our component."""

    def get_entity_id(call):
        entity_id = call.data.get('entity_id')
        device_entity_id = call.data.get('device_entity_id')
        print(entity_id)
        print(device_entity_id)
        state_object = hass.states.get(device_entity_id)

        if state_object != None:
            state = state_object.state
            if bool(state_object.attributes) == True:
                attributes = state_object.attributes
            else:
                attributes = {}
        else:
            state = 'off'
            attributes = {}

        hass.bus.async_fire('result', event_data={'entity_id':device_entity_id,'state':state,'attributes':attributes})

    hass.services.register(DOMAIN, 'get_entity_id', get_entity_id)
    return True
