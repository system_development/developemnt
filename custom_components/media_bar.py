import requests, json, time, logging
import os
import sys
from pathlib import Path
from homeassistant.core import valid_entity_id, split_entity_id
from voluptuous import *
home = str(Path.home())
_LOGGER = logging.getLogger(__name__)

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'media_bar'

def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""
    master_box_ip = config.get('sequence')['master_box_ip']

    with open(home + '/si_data.json') as data_file:
        data = json.loads(data_file.read())

    si_data = dict(data)

    # Dictionary to fetch kodi device_id as required
    okas_boxes = si_data['okasBox']
    okas_mplayer_dict = {}
    for kodi in okas_boxes:
        okas_box_mplayer_id = kodi['okasbox_id']
        okas_box_type = kodi['okasboxtype']
        okas_box_room_id = kodi['room']
        if okas_box_type != 'master':
            okas_mplayer_dict[okas_box_room_id] = okas_box_mplayer_id

    def volume_mute(call):
        room_id = call.data.get('room_id')
        volume = call.data.get('volume')
        mute = call.data.get('mute')
        zone2_volume = call.data.get('zone2_volume')
        zone2_mute = call.data.get('zone2_mute')
        udid = call.data.get('udid')
        room_config = si_data["room"]

        schema_volume_mute = Schema({Required('room_id'): All(int),
                                            Optional('volume'): All(int),
                                            Optional('mute'): All(str),
                                            Required('udid'): All(str),
                                            })
        temp_vol_mute_dict = {}
        temp_vol_mute_dict = {'room_id': room_id,
                              'udid':udid}
        if volume is not None:
            temp_vol_mute_dict['volume'] = volume
        if mute is not None:
            temp_vol_mute_dict['mute'] = mute
        try:
            schema_volume_mute(temp_vol_mute_dict)
        except Exception as e:
            _LOGGER.info(e)





        media_bar_dict = {}
        for room_info in si_data['room']:
            if room_id == room_info['room_id']:

                for device in room_info['device']:
                    device_type_room = device['device_type']
                    media_bar_dict[device_type_room] = {'device_id': device['device_id'],
                                                        'device_name': device['device_name'],
                                                        'device_type': device['device_type'],
                                                        'deviceparams': device['deviceparams'],
                                                        }
        for room_info in room_config:
            current_room_id = room_info["room_id"]

            if str(current_room_id) == str(room_id):

                # Handling media bar for AOD
                if 'avp' not in media_bar_dict.keys() and 'tv' not in media_bar_dict.keys():
                    device_id = okas_mplayer_dict[current_room_id]

                    if volume != None and mute == None:
                        mute = 'off'
                        DOMAIN = 'media_player'
                        SERVICE = 'volume_set'
                        service_data = {"entity_id": 'media_player.' + str(device_id), "volume_level": volume / 100}
                        hass.services.call(DOMAIN, SERVICE, service_data, False)

                        DOMAIN = 'media_player'
                        SERVICE = 'volume_mute'
                        service_data = {"entity_id": 'media_player.' + str(device_id), "is_volume_muted": False}
                        hass.services.call(DOMAIN, SERVICE, service_data, False)

                    elif volume == None and mute != None:

                        if mute == 'on':
                            is_volume_muted = True
                        else:
                            is_volume_muted = False

                        DOMAIN = 'media_player'
                        SERVICE = 'volume_mute'
                        service_data = {"entity_id": 'media_player.' + str(device_id), "is_volume_muted": is_volume_muted}
                        hass.services.call(DOMAIN, SERVICE, service_data, False)

                        state_object = hass.states.get('media_player.' + str(device_id))
                        if state_object != None:
                            if bool(state_object.attributes) == True:
                                attributes = state_object.attributes
                                if 'volume_level' in attributes.keys():
                                    volume = (attributes["volume_level"]) * 100
                                else:
                                    volume = None
                            else:
                                volume = None
                        else:
                            volume = None

                    elif volume == None and mute == None:
                        state_object = hass.states.get('media_player.' + str(device_id))

                        if state_object != None:
                            if bool(state_object.attributes) == True:
                                attributes = state_object.attributes
                                if 'volume_level' in attributes.keys():
                                    volume = (attributes["volume_level"]) * 100
                                else:
                                    volume = None

                                if 'is_volume_muted' in attributes.keys():
                                    is_volume_muted = attributes["is_volume_muted"]
                                    if is_volume_muted == True:
                                        mute = 'on'
                                    else:
                                        mute = 'off'
                                else:
                                    mute = None
                            else:
                                volume = None
                        else:
                            volume = None

                    event_data = {'room_id': room_id, 'volume': volume, 'mute': mute, 'udid':udid}
                    hass.bus.fire('volume_mute_result', event_data=event_data)

                else:
                    # Handling media bar for VOD
                    if 'avp' in media_bar_dict.keys():
                        device_name = media_bar_dict['avp']['device_name']
                        device_id = media_bar_dict['avp']['device_id']
                        device_params = media_bar_dict['avp']["deviceparams"]

                        for deviceparam in device_params:
                            config_params = deviceparam["config_param"]

                    elif 'avp' not in media_bar_dict.keys() and 'tv' in media_bar_dict.keys():
                        device_name = media_bar_dict['tv']['device_name']
                        device_id = media_bar_dict['tv']['device_id']
                        device_params = media_bar_dict['tv']["deviceparams"]
                        for deviceparam in device_params:
                            config_params = deviceparam["config_param"]

                    if volume != None and mute == None:
                        mute = 'off'

                        DOMAIN = str(device_name)
                        SERVICE = 'set_volume_level'
                        service_data = {"entity_id": str(device_name) + "." + str(device_id), "volume": volume}
                        hass.services.call(DOMAIN, SERVICE, service_data, False)

                        state_object = hass.states.get(str(device_name) + "." + str(device_id))

                    elif volume == None and mute != None:

                        DOMAIN = str(device_name)
                        SERVICE = 'mute_volume'
                        service_data = {"entity_id": str(device_name) + "." + str(device_id), "mute": mute}
                        hass.services.call(DOMAIN, SERVICE, service_data, False)

                        state_object = hass.states.get(str(device_name) + "." + str(device_id))
                        if state_object != None:
                            state = state_object.state

                            if state == 'on':
                                if bool(state_object.attributes) == True:
                                    attributes = state_object.attributes

                                    if 'volume' in attributes.keys():
                                        volume = attributes["volume"]

                                    else:
                                        volume = None

                                    if 'zone2_volume' in attributes.keys():
                                        zone2_volume = attributes["zone2_volume"]
                                    else:
                                        zone2_volume = None

                                else:
                                    volume = None
                            else:
                                volume = None
                        else:
                            volume = None

                    elif volume == None and mute == None:
                        state_object = hass.states.get(str(device_name) + "." + str(device_id))
                        if state_object != None:
                            state = state_object.state

                            if state == 'on':
                                if bool(state_object.attributes) == True:
                                    attributes = state_object.attributes

                                    if 'volume' in attributes.keys():
                                        volume = attributes["volume"]
                                    else:
                                        volume = None

                                    if 'zone2_volume' in attributes.keys():
                                        zone2_volume = attributes["zone2_volume"]
                                    else:
                                        zone2_volume = None

                                    if 'mute' in attributes.keys():
                                        mute = attributes["mute"]
                                    else:
                                        mute = None

                                    if 'zone2_mute' in attributes.keys():
                                        zone2_mute = attributes["zone2_mute"]
                                    else:
                                        zone2_mute = None

                                else:
                                    volume = None
                                    mute = None
                            else:
                                volume = None
                                mute = None
                        else:
                            volume = None
                            mute = None

                    event_data = {'room_id': room_id, 'volume': volume, 'mute': mute, 'zone2_volume': zone2_volume, 'zone2_mute': zone2_mute, 'udid':udid}
                    hass.bus.fire('volume_mute_result', event_data=event_data)

    def input_device(call):
        room_id = call.data.get('room_id')
        udid = call.data.get('udid')
        room_config = si_data["room"]

        deviceName=None
        zone2_state=None
        zone2_source=None

        for room_info in si_data['room']:
            if room_id == room_info['room_id']:

                media_bar_dict = {}
                for device in room_info['device']:
                    device_type_room = device['device_type']
                    media_bar_dict[device_type_room] = {'device_id': device['device_id'],
                                                        'device_name': device['device_name'],
                                                        'device_type': device['device_type'],
                                                        'okasBoxIp': device['okasBoxIp'],
                                                        'deviceparams': device['deviceparams'],
                                                        }
        for room_info in room_config:
            current_room_id = room_info["room_id"]

            if str(current_room_id) == str(room_id):

                # Handling media bar for AOD
                if 'avp' not in media_bar_dict.keys() and 'tv' not in media_bar_dict.keys():
                    event_data = {'room_id': room_id, 'input_device': 'okas_mplayer','udid':udid}
                    hass.bus.fire('input_device_result', event_data=event_data)

                else:
                    # Handling media bar for VOD
                    if 'avp' in media_bar_dict.keys():
                        device_name = media_bar_dict['avp']['device_name']
                        device_id = media_bar_dict['avp']['device_id']
                        device_params = media_bar_dict['avp']["deviceparams"]

                        for deviceparam in device_params:
                            config_params = deviceparam["config_param"]

                    elif 'avp' not in media_bar_dict.keys() and 'tv' in media_bar_dict.keys():
                        device_name = media_bar_dict['tv']['device_name']
                        device_id = media_bar_dict['tv']['device_id']
                        device_params = media_bar_dict['tv']["deviceparams"]
                        for deviceparam in device_params:
                            config_params = deviceparam["config_param"]

                    state_object = hass.states.get(str(device_name) + "." + str(device_id))
                    if state_object != None:
                        state = state_object.state

                        if state == 'on':
                            if bool(state_object.attributes) == True:
                                attributes = state_object.attributes
                                if 'source' in attributes.keys():
                                    source = attributes["source"]
                                else:
                                    source = None
                                    deviceName = None


                                if 'zone2_state' in attributes.keys():
                                    zone2_state = attributes["zone2_state"]
                                else:
                                    zone2_state = None

                                if 'zone2_source' in attributes.keys():
                                    zone2_source = attributes["zone2_source"]
                                else:
                                    zone2_source = None
                                    

                                for config_param in config_params:
                                    inputPort = config_param["inputPort"]

                                    if inputPort == source:
                                        deviceName = config_param["deviceName"]
                            else:
                                deviceName = None
                        else:
                            deviceName = None
                    else:
                        deviceName = None

                    event_data = {}
                    if zone2_state:
                        event_data = {'room_id': room_id, 'input_device': deviceName, 'zone2_state':zone2_state, 'zone2_source':zone2_source, 'udid':udid}
                    else:
                        event_data = {'room_id': room_id, 'input_device': deviceName,'udid':udid}
                    hass.bus.fire('input_device_result', event_data=event_data)


    def multiroom_volume_mute(call):
        multiroom_id = call.data.get('multiroom_id')
        volume = call.data.get('volume')
        mute = call.data.get('mute')
        udid = call.data.get('udid')

        for rooms in multiroom_id:
            volume_multiroom = volume
            mute_multiroom = mute
            media_bar_dict = {}
            for room_info in si_data['room'] :
                if rooms == room_info['room_id'] :

                    for device in room_info['device'] :
                        device_type_room = device['device_type']
                        media_bar_dict[device_type_room] = {'device_id' : device['device_id'],
                                                            'device_name' : device['device_name'],
                                                            'device_type' : device['device_type'],
                                                            'okasBoxIp' : device['okasBoxIp'],
                                                            'deviceparams' : device['deviceparams'],
                                                            }
                    current_room_id = room_info["room_id"]

                    # Handling media bar for AOD Not enter
                    if 'avp' not in media_bar_dict.keys() and 'tv' not in media_bar_dict.keys() :
                        print('in AOD condition')
                        device_id = okas_mplayer_dict[current_room_id]

                        if volume != None and mute == None :
                            mute = 'off'
                            DOMAIN = 'media_player'
                            SERVICE = 'volume_set'
                            service_data = {"entity_id" : 'media_player.' + str( device_id ), "volume_level" : volume / 100}
                            hass.services.call( DOMAIN, SERVICE, service_data, False )

                            DOMAIN = 'media_player'
                            SERVICE = 'volume_mute'
                            service_data = {"entity_id" : 'media_player.' + str( device_id ), "is_volume_muted" : False}
                            hass.services.call( DOMAIN, SERVICE, service_data, False )

                        elif volume == None and mute != None :

                            if mute == 'on' :
                                is_volume_muted = True
                            else :
                                is_volume_muted = False

                            DOMAIN = 'media_player'
                            SERVICE = 'volume_mute'
                            service_data = {"entity_id" : 'media_player.' + str( device_id ),
                                            "is_volume_muted" : is_volume_muted}
                            hass.services.call( DOMAIN, SERVICE, service_data, False )

                            state_object = hass.states.get( 'media_player.' + str( device_id ) )
                            if state_object != None :
                                if bool( state_object.attributes ) == True :
                                    attributes = state_object.attributes
                                    if 'volume_level' in attributes.keys() :
                                        volume_multiroom = (attributes["volume_level"]) * 100
                                    else :
                                        volume_multiroom = None
                                else :
                                    volume_multiroom = None
                            else :
                                volume_multiroom = None

                        elif volume == None and mute == None :
                            state_object = hass.states.get( 'media_player.' + str( device_id ))


                            if state_object != None :
                                if bool( state_object.attributes ) == True :
                                    attributes = state_object.attributes
                                    if 'volume_level' in attributes.keys() :
                                        volume_multiroom = (attributes["volume_level"]) * 100
                                    else :
                                        volume_multiroom = None

                                    if 'is_volume_muted' in attributes.keys() :
                                        is_volume_muted = attributes["is_volume_muted"]
                                        if is_volume_muted == True :
                                            mute_multiroom = 'on'
                                        else :
                                            mute_multiroom = 'off'
                                    else :
                                        mute_multiroom = None
                                else :
                                    volume_multiroom = None
                            else :
                                volume_multiroom = None

                        event_data = {'room_id' : rooms, 'volume' : volume_multiroom, 'mute' : mute_multiroom, 'udid' : udid}
                        hass.bus.fire( 'multiroom_result', event_data=event_data)

                    else :
                        # Handling media bar for VOD
                        if 'avp' in media_bar_dict.keys() :
                            print('in VOD with avp condition')
                            device_name = media_bar_dict['avp']['device_name']
                            device_id = media_bar_dict['avp']['device_id']
                            device_id_list = []
                            device_id_list.append(device_id)
                            print( "rooms", rooms )
                            print("volume",volume)
                            print("mute",mute)

                        elif 'avp' not in media_bar_dict.keys() and 'tv' in media_bar_dict.keys():
                            print( 'in VOD with tv condition' )
                            device_name = media_bar_dict['tv']['device_name']
                            device_id = media_bar_dict['tv']['device_id']

                        if volume != None and mute == None :
                            print( 'volume not none and mute none++++++++++++++' )
                            mute_multiroom = 'off'

                            DOMAIN = str( device_name )
                            SERVICE = 'set_volume_level'
                            service_data = {"entity_id" : str( device_name ) + "." + str(device_id), "volume" : volume_multiroom}
                            hass.services.call( DOMAIN, SERVICE, service_data, False)

                        elif volume == None and mute != None :
                            print( 'volume none and mute not none' )

                            DOMAIN = str( device_name )
                            SERVICE = 'mute_volume'
                            service_data = {"entity_id" : str( device_name ) + "." + str( device_id ), "mute" : mute}
                            hass.services.call( DOMAIN, SERVICE, service_data, False )

                            state_object = hass.states.get( str( device_name ) + "." + str( device_id ) )
                            if state_object != None :
                                state = state_object.state

                                if state == 'on' :
                                    if bool( state_object.attributes ) == True :
                                        attributes = state_object.attributes

                                        if 'volume' in attributes.keys() :
                                            volume_multiroom = attributes["volume"]

                                    else :
                                        volume_multiroom = None
                                else :
                                    volume_multiroom = None
                            else :
                                volume_multiroom = None

                        elif volume == None and mute == None :
                            print('volume none and mute none')
                            state_object = hass.states.get( str( device_name ) + "." + str( device_id ))
                            if state_object != None :
                                state = state_object.state

                                if state == 'on' :
                                    if bool( state_object.attributes ) == True :
                                        attributes = state_object.attributes

                                        if 'volume' in attributes.keys() :
                                            volume_multiroom = attributes["volume"]

                                        if 'mute' in attributes.keys() :
                                            mute_multiroom = attributes["mute"]

                                    else :
                                        volume_multiroom = None
                                        mute_multiroom = None
                                else :
                                    volume_multiroom = None
                                    mute_multiroom = None
                            else :
                                volume_multiroom = None
                                mute_multiroom = None

                        event_data = {'room_id' : rooms, 'volume' : volume_multiroom, 'mute' : mute_multiroom, 'udid' : udid}
                        hass.bus.fire( 'multiroom_result', event_data=event_data )

    hass.services.register(DOMAIN, 'input_device', input_device)
    hass.services.register(DOMAIN, 'volume_mute', volume_mute)
    hass.services.register(DOMAIN, 'multiroom_volume_mute', multiroom_volume_mute)

    return True





