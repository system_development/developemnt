from panasonic_viera import RemoteControl
import paramiko

class panasonic_tv_class:
    
	def __init__(self,ip,port):

		# Creating socket connection.
		try:
			self.client = RemoteControl(ip, port)
			print("socket connection created")
			print(dir(self.client))
		except Exception as error:
			print ('Failed to create socket. Error message: ' + str(error))

	def transmit(self, command):
		print("command sent")
		self.client.send_key(command)
		print(command)

	def set_volume(self, value):
		print("response received")
		response = self.client.set_volume(value)
		if response:
			print(response)
			return response
		else:
			return False

	def volume_status(self):
		print("response received")
		response = self.client.get_volume()
		if response:
			print(response)
			return response
		else:
			return False

	def mute_status(self):
		print("response received")
		response = self.client.get_mute()
		if response:
			print(response)
			return response
		else:
			return False

class cec_panasonic_tv_class():
	"""Class to encapsulate the projector.
	"""

	def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
		"""Set up serial connection to projector."""

		try:
			self.ssh = paramiko.SSHClient()
			self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			self.ssh.connect(slave_ip,slave_port,slave_user,slave_pwd)
			print("Trying to establish connection through ssh")

		except Exception as error:
			print('Connection Failed')
			print(error)
			return

	def _command_handler(self, command_string):
		print("Invoking command handler")
		print("Command string is below")
		print(command_string)
		stdin,stdout,stderr = self.ssh.exec_command(command_string)
		print(stdout.readline())
		for line in iter(stdout.readline, ""):
			print(line, end="")
			print('finished.')
		return stdout


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'panasonic_tv'
ATTR_NAME = 'entity_id'

def setup(hass, config):
	"""Set up is called when Home Assistant is loading our component."""

	global ip, port
	ip = config.get('panasonic_tv')['ip']
	port = config.get('panasonic_tv')['port']
	device_id = config.get('panasonic_tv')['device_id']
	entity_id = ('panasonic_tv.' + str(device_id))
	serial_port = config.get('panasonic_tv')['serial_port']
	slave_ip = config.get('panasonic_tv')['slave_ip']
	slave_port = config.get('panasonic_tv')['slave_port']
	slave_user = config.get('panasonic_tv')['slave_username']
	slave_pwd = config.get('panasonic_tv')['slave_password']
	print(entity_id)
	print(ip)
	print(port)

	class_obj = panasonic_tv_class(ip,port)


	def power_on(call):
		entity_id = call.data.get("entity_id")
		class_obj = cec_panasonic_tv_class(slave_ip, slave_port, slave_user, slave_pwd)
		print("sending command")
		command = 'echo "on 0" | cec-client -s '+str(serial_port)
		class_obj._command_handler(command)
		hass.states.set(entity_id , 'on')

	def power_off(call):
		entity_id = call.data.get("entity_id")
		class_obj = cec_panasonic_tv_class(slave_ip, slave_port, slave_user, slave_pwd)
		print("sending command")
		command = 'echo "standby 0" | cec-client -s '+str(serial_port)
		class_obj._command_handler(command)
		hass.states.set(entity_id , 'off')

	def volume_up(call):
		entity_id = call.data.get("entity_id")
		class_obj.transmit('NRC_VOLUP-ONOFF')
		response = class_obj.volume_status()
		attributes_dict = {}
		if response:
			print(response)
			try:
				attributes = hass.states.get(entity_id).attributes
				attributes_dict.update(dict(attributes))
				attributes_dict['volume'] = 'up'
				hass.states.set(entity_id, "on", attributes_dict)
			except Exception as error:
				print(error)

	def volume_down(call):
		entity_id = call.data.get("entity_id")
		class_obj.transmit('NRC_VOLDOWN-ONOFF')
		response = class_obj.volume_status()
		attributes_dict = {}
		if response:
			print(response)
			try:
				attributes = hass.states.get(entity_id).attributes
				attributes_dict.update(dict(attributes))
				attributes_dict['volume'] = 'down'
				hass.states.set(entity_id, "on", attributes_dict)
			except Exception as error:
				print(error)
				hass.states.set(entity_id, "on")

	def volume_status(call):
		entity_id = call.data.get("entity_id")
		response = class_obj.volume_status()
		attributes_dict = {}
		if response:
			print(response)
			try:
				attributes = hass.states.get(entity_id).attributes
				attributes_dict.update(dict(attributes))
				attributes_dict['volume'] = 'status'
				hass.states.set(entity_id, "on", attributes_dict)
			except Exception as error:
				print(error)
				hass.states.set(entity_id, "on")

	def set_volume(call):
		entity_id = call.data.get("entity_id")
		volume = call.data.get("volume")
		class_obj.set_volume(volume)
		attributes_dict = {}
		try:
			attributes = hass.states.get(entity_id).attributes
			attributes_dict.update(dict(attributes))
			attributes_dict['volume'] = volume
			hass.states.set(entity_id, "on", attributes_dict)
		except Exception as error:
			print(error)

	def mute_toggle(call):
		entity_id = call.data.get("entity_id")
		class_obj.transmit('NRC_MUTE-ONOFF')
		response = class_obj.mute_status()
		attributes_dict = {}
		print(response)
		try:
			attributes = hass.states.get(entity_id).attributes
			attributes_dict.update(dict(attributes))
			if response == True:
				attributes_dict['mute'] = 'on'
			elif response == False:
				attributes_dict['mute'] = 'off'
			hass.states.set(entity_id, "on", attributes_dict)
		except Exception as error:
			print(error)
			hass.states.set(entity_id, "on")

	def mute_status(call):
		entity_id = call.data.get("entity_id")
		response = class_obj.mute_status()
		attributes_dict = {}
		print(response)
		try:
			attributes = hass.states.get(entity_id).attributes
			attributes_dict.update(dict(attributes))
			if response == True:
				attributes_dict['mute'] = 'on'
			elif response == False:
				attributes_dict['mute'] = 'off'
			hass.states.set(entity_id, "on", attributes_dict)
		except Exception as error:
			print(error)
			hass.states.set(entity_id, "on")

	def select_source(call):
		entity_id = call.data.get("entity_id")
		source = call.data.get("source")
		command = 'NRC_'+str(source)+'-ONOFF'
		class_obj.transmit(command)
		attributes_dict = {}
		try:
			attributes = hass.states.get(entity_id).attributes
			attributes_dict.update(dict(attributes))
			attributes_dict['source'] = source
			hass.states.set(entity_id, "on", attributes_dict)
		except Exception as error:
			print(error)
			hass.states.set(entity_id, "on")

	hass.services.register(DOMAIN, 'power_on', power_on)
	hass.services.register(DOMAIN, 'power_off', power_off)
	hass.services.register(DOMAIN, 'volume_up', volume_up)
	hass.services.register(DOMAIN, 'volume_down', volume_down)
	hass.services.register(DOMAIN, 'volume_status', volume_status)
	hass.services.register(DOMAIN, 'set_volume', set_volume)
	hass.services.register(DOMAIN, 'mute_toggle', mute_toggle)
	hass.services.register(DOMAIN, 'mute_status', mute_status)
	hass.services.register(DOMAIN, 'select_source', select_source)

	# Return boolean to indicate that initialization was successfully.
	return True


