#!/usr/bin/env python

import paramiko

class runco_projector():
    """Class to encapsulate the projector.
    """

    def __init__(self,slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


#Based on the room parameter(configuration.yaml), we have to find out which sub-okas it is.Se configuration.yaml
        ip = self.slave_ip
        port= self.slave_port
        username= self.slave_user
        password= self.slave_pwd
        try:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(ip,port,username,password)
            print("Trying to establish connection through ssh")

        except Exception as error:
            print('Connection Failed')
            print(error)
            return

    def _command_handler(self, command_string):
        print("Invoking command handler")
        print("Command string is below")
        print(command_string)
        stdin,stdout,stderr = self.ssh.exec_command(command_string)
        print(stdout.readline())
        for line in iter(stdout.readline, ""):
            print(line, end="")
            print('finished.')
        return stdout




# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'runco_projector'

ATTR_NAME = 'entity_id'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('runco_projector')['serial_port']
    baudrate = config.get('runco_projector')['baudrate']
    device_id = config.get('runco_projector')['device_id']
    entity_id = ('runco_projector.' + str(device_id))
    slave_ip = config.get('runco_projector')['slave_ip']
    slave_port = config.get('runco_projector')['slave_port']
    slave_user = config.get('runco_projector')['slave_username']
    slave_pwd = config.get('runco_projector')['slave_password']

    def turn_on(call):
        name = call.data.get(ATTR_NAME, entity_id)
        runco_projectorobj = runco_projector(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PWR=1'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = runco_projectorobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def turn_off(call):
        name = call.data.get(ATTR_NAME, entity_id)
        runco_projectorobj = runco_projector(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PON'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = runco_projectorobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'off')

    def set_source_HDMI1(call):
        name = call.data.get(ATTR_NAME, entity_id)
        runco_projectorobj = runco_projector(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = "I01\r\n"
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = runco_projectorobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on',{'operation':'HDMI1'})

    def set_source_HDMI2(call):
        name = call.data.get(ATTR_NAME, entity_id)
        runco_projectorobj = runco_projector(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = "I02\r\n"
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = runco_projectorobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on',{'operation':'HDMI2'})



    hass.services.register(DOMAIN, 'set_source_HDMI2', set_source_HDMI2)
    hass.services.register(DOMAIN, 'set_source_HDMI1', set_source_HDMI1)
    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)



    # Return boolean to indicate that initialization was successfully.
    return True

