#!/usr/bin/env python
import requests
import json
import time

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'custom_scene'
ATTR_NAME = 'entity_id'

def setup(hass,config=None):
    """Set up is called when Home Assistant is loading our component."""

    master_box_ip = config.get('sequence')['master_box_ip']
    # Function to turn on scene
    def select_scene(call):
        print("custom_scene.turn_on")
        entity_id = call.data.get("entity_id")
        scene_name = call.data.get("scene_name")
        service_data = {"entity_id":str(scene_name)}

        hass.services.call('scene','turn_on',service_data,False)

        hass.states.set(entity_id, scene_name)

    hass.services.register(DOMAIN, 'select_scene', select_scene)

    # Return boolean to indicate that initialization was successfully.
    return True
