#!/usr/bin/env python
import socket
import time
import requests
import json

class trinov(object):
    """Class to encapsulate the projector.
    """

    def __init__(self):
        self.ip=ip
        self.port=port

        # Creating socket connection.
        try:
            self.client=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print("socket connection created")
        except Exception as error:
            print ('Failed to create socket. Error message: ' + str(error))

    def create_connection(self):
        # Connecting with the device on its IP and port.
        try:
            self.client.connect((self.ip, self.port))
            print("connection established")
        except Exception as error:
            print ('Failed to connect to client. Error message: ' + str(error))
            return

    def transmit(self, command):
        try:
            print("command sent")
            self.client.sendall(command.encode("utf-8"))
            print(command.encode())
        except Exception as error:
            print ('Failed to transmit command. Error message: ' + str(error))
            return

    def receive(self):
        try:
            print("response received")
            response = self.client.recv(4096)
            print(response.decode())
            return response
        except Exception as error:
            print ('Failed to receive command. Error message: ' + str(error))


    def close_connection(self):
        try:
            self.client.close()
            print("connection closed")
        except Exception as error:
            print ('Failed to close connection. Error message: ' + str(error))
            return


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'trinov'
ATTR_NAME = 'entity_id'
def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""

    global ip, port,device_id
    ip = config.get('trinov')['ip']
    port = config.get('trinov')['port']
    device_id = config.get('trinov')['device_id']
    entity_id = ('trinov.' + str(device_id))
    print(entity_id)
    print(ip)
    print(port)
    print(device_id)

    def power_on(call):
        #name = call.data.get(ATTR_NAME, entity_id)
        entity_id = call.data.get('entity_id')
        #master_box_ip=config.get('sequence')['master_box_ip']
        #print(master_box_ip)
        #print("Turning trinov on KNX switch")
        #url = 'https://'+str(master_box_ip)+':8123/api/services/switch/power_on'
        #headers = {
        #           'content-type': 'application/json'
        #            }
        #data = {
        #          "entity_id": "switch.trinov_power"
        #       }
        service_data = {"entity_id":"switch.trinov_power" }
        hass.services.call('switch','turn_on', service_data, False)
        #response = requests.post(url, headers=headers, data=json.dumps(data))
        #print(response.text)
        #if response:
        state_object = hass.states.get(entity_id)
        attributes_dict={}
        try:
            attributes=hass.states.get(entity_id).attributes
            attributes_dict.update(dict(attributes))
            hass.states.set(entity_id, "on", attributes_dict)

        except Exception as error:
            print(error)
            hass.states.set(entity_id, "on", attributes_dict)

    def power_off(call):
        #name = call.data.get(ATTR_NAME, entity_id)
        entity_id = call.data.get('entity_id')
        #master_box_ip=config.get('sequence')['master_box_ip']
        #print(master_box_ip)
        #print("Turning trinov on KNX switch")
        #url = 'https://'+str(master_box_ip)+':8123/api/services/switch/power_off'
        #headers = {
        #           'content-type': 'application/json'
        #            }
        #data = {
        #          "entity_id": "switch.trinov_power"
        #        }
        #response = requests.post(url, headers=headers, data=json.dumps(data))
        #print(response.text)
        service_data = {"entity_id": "switch.trinov_power"}
        hass.services.call('switch','turn_off',service_data, False)
        #if response:
        state_object = hass.states.get(entity_id)
        attributes_dict={}
        try:
            attributes=hass.states.get(entity_id).attributes
            attributes_dict.update(dict(attributes))
            attributes_dict['source'] = None
            hass.states.set(entity_id, "off", attributes_dict)

        except Exception as error:
            print(error)
            hass.states.set(entity_id, "off", attributes_dict)


    def set_volume_level(call):
        name = call.data.get(ATTR_NAME, entity_id)
        volume = call.data.get("volume")
        volume_new=(99-volume)
        print(volume)
        trinovObj = trinov()
        trinovObj.create_connection()
        command_string = 'id okas_box\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        command_string = 'volume '+str(-volume_new)+'\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        command_string = 'mute 0\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        if response:
            state_object = hass.states.get(entity_id)
            attributes_dict={}
            try:
                attributes=hass.states.get(entity_id).attributes
                attributes_dict.update(dict(attributes))
                attributes_dict['volume'] = volume
                hass.states.set(entity_id, "on", attributes_dict)

            except Exception as error:
                print(error)
                attributes_dict['volume'] = volume
                hass.states.set(entity_id, "off", attributes_dict)
            trinovObj.close_connection()


    def mute_volume(call):
        entity_id = call.data.get("entity_id")
        trinovObj = trinov()
        trinovObj.create_connection()
        command_string = 'id okas_box\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        volume_dict = {'on': 'mute 1\r', 'off': 'mute 0\r'}
        mute = call.data.get("mute")
        print("the volume is=")
        print(mute)
        comm=volume_dict[mute]
        volume_data=trinovObj.transmit(comm)
        print("the volume_data is")
        print(volume_data)
        response_list = trinovObj.receive()
        if response_list:
            attributes_dict={}
            try:
                attributes=hass.states.get(entity_id).attributes
                attributes_dict.update(dict(attributes))
                attributes_dict['mute'] = mute
                hass.states.set(entity_id, "on", attributes_dict)
            except Exception as error:
                print(error)
                attributes_dict['mute'] = mute
                hass.states.set(entity_id, "on", attributes_dict)
            trinovObj.close_connection()


    def select_source(call):

        entity_id = call.data.get("entity_id")
        trinovObj = trinov()
        trinovObj.create_connection()
        command_string = 'id okas_box\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        source_dict = {'hdmi1': 'profile 0\r', 'hdmi2': 'profile 1\r', 'hdmi3': 'profile 2\r', 'hdmi4': 'profile 3\r', 'hdmi5': 'profile 4\r', 'hdmi6': 'profile 5\r', 'hdmi7' :  'profile 6\r', 'hdmi8' :  'profile 7\r', 'hdmi9': 'profile 8\r' }
        source = call.data.get("source")
        print("the source is=")
        print(source)
        comm=source_dict[source]
        source_data=trinovObj.transmit(comm)
        print("the source data is")
        print(source_data)
        response_list = trinovObj.receive()
        if response_list:
            attributes_dict={}
            try:
                attributes=hass.states.get(entity_id).attributes
                attributes_dict.update(dict(attributes))
                attributes_dict['source'] = source
                hass.states.set(entity_id, "on", attributes_dict)
            except Exception as error:
                    print(error)
                    attributes_dict['source'] = source
                    hass.states.set(entity_id, "off", attributes_dict)
            trinovObj.close_connection()

    def select_mode(call):
        entity_id = call.data.get("entity_id")
        trinovObj = trinov()
        trinovObj.create_connection()
        command_string = 'id okas_box\r'
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        mode_dict = {'auro3d': b'upmixer auro3d\r', 'dolby': b'upmixer dolby\r', 'auto': b'upmixer auto\r',
                     'upmixer_feed': b'upmixer \r', 'dts': b'upmixer dts\r', 'upmixer_on_native': b'upmixer upmixer_on_native\r',
                     'native': b'upmixer native\r', 'legacy': b'upmixer legacy\r'}
        mode = call.data.get("mode")
        print("the mode is=")
        print(mode)
        mode_dict[mode]
        command_string = mode_dict[mode]
        trinovObj.transmit(command_string)
        response = trinovObj.receive()
        attributes_dict = {}
        try:
            attributes = hass.states.get(entity_id).attributes
            attributes_dict.update(dict(attributes))
            attributes_dict['mode'] = mode
            hass.states.set(entity_id, "on", attributes_dict)
        except Exception as error:
            print(error)
            attributes_dict['mode'] = mode
            hass.states.set(entity_id, "on", attributes_dict)
        trinovObj.close_connection()

    service_dict = {'power_on' : power_on, 'power_off': power_off, 'mute_volume':mute_volume,'set_volume_level':set_volume_level,'select_source': select_source ,'select_mode': select_mode}
    for service in service_dict:
        hass.services.register(DOMAIN, service, service_dict[service])




    # Return boolean to indicate that initialization was successfully.
    return True
