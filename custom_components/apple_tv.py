#!/usr/bin/env python
import socket
import subprocess
import time
import requests
import json

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'apple_tv'
ATTR_NAME = 'entity_id'
def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""

    global ip, port,device_id, login_id,apple_list
    apple_list = []
    try:
        ip=config.get('apple_tv')['ip']
        port=config.get('apple_tv')['port']
        device_id=config.get('apple_tv')['device_id']
        print('ip=' , ip)
        print('port=' , port)
        print('device_id',device_id)
    except:        
        apple_tv_configs = config.get('apple_tv')
        for config in apple_tv_configs:
            print(config)
            ip = config['ip']
            port = config['port']
            device_id = config['device_id']
            apple_list.append({device_id:ip})
        entity_id = ('apple_tv.' + str(device_id))
        print(entity_id)
        print(ip)
        print(port)
        print(apple_list)

    a=1
    while a:
        global login_id, ip_login_dict
        login_id = str(subprocess.check_output(['atvremote', 'scan']))
        split1 = login_id.split(" - ")
        new_list = split1[1:]
        ip_login_dict = {}
        for items in new_list:
            items_split = items.split(" ")
            ip_to_get = items_split.index('at')
            ip = items_split[ip_to_get+1]
            print(ip)
            login_to_get = items_split.index('id:')
            login = items_split[login_to_get+1].split(')')[0]
            print(login)
            ip_login_dict.update({ip:login})
        # print(some_list)

        break



    def menu(call):
        global ip_login_dict
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]

                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]


                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'menu'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"menu"})

    def left(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]

                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'left'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"left"})

    def right(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'right'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"right"})

    def top_menu(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'top_menu'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"top_menu"})

    def up(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'up'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"up"})

    def down(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'down'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"down"})
  

    def select(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'select'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"up"})

    def pin(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'pin'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"pin"})

    def pause(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'pause'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"pause"})

    def play(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'play'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"play"})

    def stop(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'stop'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"stop"})


    def next(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'next'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"next"})


    def previous(call):
        # global login_id
        name = call.data.get(ATTR_NAME, entity_id)
        device_id = name.split(".")[1]
        for apple_list_val in apple_list:
            if device_id in apple_list_val.keys():
                print(apple_list_val[device_id])
                ip_to_send= apple_list_val[device_id]
                if ip_to_send in ip_login_dict.keys():
                    login_id_to_send = ip_login_dict[ip_to_send]

                    print(ip_to_send)
                    print(login_id_to_send)
                    command_string = subprocess.check_output(['atvremote', '--login_id', login_id_to_send, '--address', ip_to_send, 'previous'])
                    print("command string ====",command_string)
                    hass.states.set(entity_id, "\nOK", {"operation":"previous"})


    hass.services.register(DOMAIN, 'menu', menu)
    hass.services.register(DOMAIN, 'top_menu', top_menu)
    hass.services.register(DOMAIN, 'up', up)
    hass.services.register(DOMAIN, 'down', down)
    hass.services.register(DOMAIN, 'right', right)
    hass.services.register(DOMAIN, 'left', left)
    hass.services.register(DOMAIN, 'select', select)
    hass.services.register(DOMAIN, 'pin', pin)
    hass.services.register(DOMAIN, 'previous', previous)
    hass.services.register(DOMAIN, 'next', next)
    hass.services.register(DOMAIN, 'stop', stop)
    hass.services.register(DOMAIN, 'play', play)
    hass.services.register(DOMAIN, 'pause', pause)
    hass.services.register(DOMAIN, 'pin', pin)
    hass.services.register(DOMAIN, 'pin', pin)


    # Return boolean to indicate that initialization was successfully.
    return True
