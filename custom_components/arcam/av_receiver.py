#!/usr/bin/env python


import time, math, socket, struct, time, threading, select
#import serial
import telnetlib
import logging
import telnetlib
import asyncio
import urllib
import requests
import voluptuous as vol

from homeassistant.helpers.entity import Entity

import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)

global devices_objects_list
devices_objects_list = []
global state_response, source_response, mode_response, mute_response, navigation_response, zone_mute_response, zone_state_response


def setup_platform(hass, config, add_devices, discovery_info=None):
    global slave_ip
    """Set up the Marantz platform."""
    # global ip, port, device_id, class_obj, entity_domain
    arcam_config = dict(config)
    slave_ip = arcam_config['slave_ip']
    serial_port = arcam_config['serial_port']
    baudrate = arcam_config['baudrate']
    device_id = arcam_config['device_id']
    master_ip = arcam_config['master_box_ip']
    class_obj = arcam_class()
    class_obj.create_connection(serial_port, baudrate, device_id,slave_ip)
    #class_obj.update(hass)

class arcam_class():
    """Class to encapsulate the projector.
    """

    def create_connection(self, serial_port, baudrate, device_id,slave_ip):
        data = {'port': serial_port, 'baud': baudrate, 'device_id': device_id}
        data = urllib.parse.urlencode(data)
        connection_url = 'http://' + str(slave_ip) + ':5000/connect?' + data
        response = requests.get(connection_url)
        print(response.text)


    def update(self,hass, response_realtime, device_id):
        print(response_realtime)
        print(device_id)
        entity_id = 'arcam.'+str(device_id)
        attributes_dict = {}
        try:
            attributes = hass.states.get(entity_id).attributes
            attributes_dict.update(dict(attributes))
            if '21010D0001' in response_realtime:
                if len(response_realtime) == 14:
                    vol_string = response_realtime[10:12]
                    volume_to_set = int(vol_string, 16)
                    attributes_dict['volume'] = str(volume_to_set)
                    attributes_dict['mute'] = 'off'
                    hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002107' in response_realtime:
                if '2101080002107B0D' in response_realtime:
                    print("Power on Hit")
                    state = 'on'
                    hass.states.set(entity_id, state, attributes_dict)
                elif '2101080002107C0D' in response_realtime:
                    print("Power on Hit")
                    state = 'off'
                    attributes_dict['source'] = None
                    hass.states.set(entity_id, state, attributes_dict)

                #Merging power with mute because the significant bits are similar
                elif '210108000210770D' in response_realtime:
                    print("Mute on hit")
                    attributes_dict['mute'] = 'on'
                    hass.states.set(entity_id, 'on', attributes_dict )
                elif '210108000210780D' in response_realtime:
                    print("Mute on hit")
                    attributes_dict['mute'] = 'off'
                    hass.states.set(entity_id, 'on', attributes_dict )

            if '2101080002100' in response_realtime:
                source_response = {'210108000210010D': 'stb',
                                   '210108000210020D': 'av',
                                   '210108000210030D': 'tuner',
                                   '210108000210040D': 'bd',
                                   '210108000210050D': 'game',
                                   '210108000210060D': 'vcr',
                                   '210108000210070D': 'cd',
                                   '210108000210080D': 'aux',
                                   '210108000210090D': 'arc',
                                   '210108000210000D': 'sat',
                                   # '210108000210220D': 'pvr',    conflicting case with power and mute
                                   # '210108000210120D': 'usb',
                                   '2101080002100B0D': 'net'
                                   }
                feedback_keys = source_response.keys()
                for sources in feedback_keys:
                    if sources in response_realtime:
                        attributes_dict['source'] = source_response[sources]
                        hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002106' in response_realtime:
                mode_response = {'2101080002106B0D': 'stereo',
                                 '2101080002106E0D': 'dolby_pl',
                                 '2101080002106A0D': 'multi_ch_stereo',
                                 # '210108000210700D': 'dts_music',   corner case conflicting with power and mute
                                 '2101080002106F0D': 'dts_cinema'}
                                 # '210108000210450D': 'ch_stereo'}    corner case conflicting with power and mute
                feedback_keys = mode_response.keys()
                for modes in feedback_keys:
                    if modes in response_realtime:
                        attributes_dict['mode'] = mode_response[modes]
                        hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002105' in response_realtime:
                navigation_response = {'210108000210560D': 'up',
                                       '210108000210550D': 'down',
                                       '210108000210510D': 'left',
                                       '210108000210500D': 'right',
                                       '210108000210570D': 'enter',
                                       '210108000210520D': 'menu_on',
                                       }

                feedback_keys = navigation_response.keys()
                for navigations in feedback_keys:
                    if navigations in response_realtime:
                        attributes_dict['navigation'] = navigation_response[navigations]
                        hass.states.set(entity_id, "on", attributes_dict)
            #corner case in navigation: response stream is out of pattern
            if '2101080002102B0D' in response_realtime:
                attributes_dict['mode'] = 'menu_off'
                hass.states.set(entity_id, "on", attributes_dict)


            if '2101080002177' in response_realtime:
                zone_state_response = {'2101080002177B0D': 'on','2101080002177C0D': 'off'}
                feedback_keys = zone_state_response.keys()
                for zone2_state in feedback_keys:
                    if zone2_state in response_realtime:
                        attributes_dict['zone2'] = zone_state_response[zone2_state]
                        hass.states.set(entity_id, "on", attributes_dict)

            if '2101080002170' in response_realtime:
                zone_mute_response = {'210108000217040D': 'on', '210108000217050D': 'off'}
                feedback_keys = zone_state_response.keys()
                for zone2_mute in feedback_keys:
                    if zone2_mute in response_realtime:
                        attributes_dict['zone2_mute'] = zone_mute_response[zone2_mute]
                        hass.states.set(entity_id, "on", attributes_dict)

        except Exception as error:
            print(error)
            if '21010D0001' in response_realtime:
                if len(response_realtime) == 14:
                    vol_string = response_realtime[10:12]
                    volume_to_set = int(vol_string, 16)
                    attributes_dict['volume'] = str(volume_to_set)
                    attributes_dict['mute'] = 'off'
                    hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002107' in response_realtime:
                if '2101080002107B0D' in response_realtime:
                    print("Power on Hit")
                    state = 'on'
                    hass.states.set(entity_id, state, attributes_dict)
                elif '2101080002107C0D' in response_realtime:
                    print("Power on Hit")
                    state = 'off'
                    attributes_dict['source'] = None
                    hass.states.set(entity_id, state, attributes_dict)

                # Merging power with mute because the significant bits are similar
                elif '210108000210770D' in response_realtime:
                    print("Mute on hit")
                    attributes_dict['mute'] = 'on'
                    hass.states.set(entity_id, 'on', attributes_dict)
                elif '210108000210780D' in response_realtime:
                    print("Mute on hit")
                    attributes_dict['mute'] = 'off'
                    hass.states.set(entity_id, 'on', attributes_dict)

            if '2101080002100' in response_realtime:
                source_response = {'210108000210010D': 'stb',
                                   '210108000210020D': 'av',
                                   '210108000210030D': 'tuner',
                                   '210108000210040D': 'bd',
                                   '210108000210050D': 'game',
                                   '210108000210060D': 'vcr',
                                   '210108000210070D': 'cd',
                                   '210108000210080D': 'aux',
                                   '210108000210090D': 'arc',
                                   '210108000210000D': 'sat',
                                   # '210108000210220D': 'pvr',    conflicting case with power and mute
                                   # '210108000210120D': 'usb',
                                   '2101080002100B0D': 'net'
                                   }
                feedback_keys = source_response.keys()
                for sources in feedback_keys:
                    if sources in response_realtime:
                        attributes_dict['source'] = source_response[sources]
                        hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002106' in response_realtime:
                mode_response = {'2101080002106B0D': 'stereo',
                                 '2101080002106E0D': 'dolby_pl',
                                 '2101080002106A0D': 'multi_ch_stereo',
                                 # '210108000210700D': 'dts_music',   corner case conflicting with power and mute
                                 '2101080002106F0D': 'dts_cinema'}
                # '210108000210450D': 'ch_stereo'}    corner case conflicting with power and mute
                feedback_keys = mode_response.keys()
                for modes in feedback_keys:
                    if modes in response_realtime:
                        attributes_dict['mode'] = mode_response[modes]
                        hass.states.set(entity_id, "on", attributes_dict)
            if '2101080002105' in response_realtime:
                navigation_response = {'210108000210560D': 'up',
                                       '210108000210550D': 'down',
                                       '210108000210510D': 'left',
                                       '210108000210500D': 'right',
                                       '210108000210570D': 'enter',
                                       '210108000210520D': 'menu_on',
                                       }

                feedback_keys = navigation_response.keys()
                for navigations in feedback_keys:
                    if navigations in response_realtime:
                        attributes_dict['mode'] = navigation_response[navigations]
                        hass.states.set(entity_id, "on", attributes_dict)
            # corner case in navigation: response stream is out of pattern
            if '2101080002102B0D' in response_realtime:
                attributes_dict['mode'] = 'menu_off'
                hass.states.set(entity_id, "on", attributes_dict)

            if '2101080002177' in response_realtime:
                zone_state_response = {'2101080002177B0D': 'on', '2101080002177C0D': 'off'}
                feedback_keys = zone_state_response.keys()
                for zone2_state in feedback_keys:
                    if zone2_state in response_realtime:
                        attributes_dict['zone2'] = zone_state_response[zone2_state]
                        hass.states.set(entity_id, "on", attributes_dict)

            if '2101080002170' in response_realtime:
                zone_mute_response = {'210108000217040D': 'on', '210108000217050D': 'off'}
                feedback_keys = zone_state_response.keys()
                for zone2_mute in feedback_keys:
                    if zone2_mute in response_realtime:
                        attributes_dict['zone2_mute'] = zone_mute_response[zone2_mute]
                        hass.states.set(entity_id, "on", attributes_dict)


    def turn_on(self, device_id):
        command = '21010802107B0D'
        data = {'device_id':device_id, 'command':command}
        data = urllib.parse.urlencode(data)
        print(slave_ip)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)

    def turn_off(self, device_id):
        command = '21010802107C0D'
        data = {'device_id': device_id, 'command': command}
        data = urllib.parse.urlencode(data)
        print(slave_ip)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)

    def select_source(self, device_id, source):
        source_dict = {
            'stb': '2101080210010D',
            'av': '2101080210020D',
            'tuner': '2101080210030D',
            'bd': '2101080210040D',
            'game': '2101080210050D',
            'vcr': '2101080210060D',
            'cd': '2101080210070D',
            'aux': '2101080210080D',
            'arc': '2101080210090D',
            'sat': '2101080210000D',
            'pvr': '2101080210220D',
            'usb': '2101080210120D',
            'net': '21010802100B0D'
        }
        print(slave_ip)
        data = {'device_id': device_id, 'command': source_dict[source]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)

    def select_mode(self, device_id, mode):
        mode_dict = {'stereo': '21010802106B0D', 'dolby_pl': '21010802106E0D',
                     'multi_ch_stereo': '21010802106A0D', 'dts_music': '2101080210700D',
                     'dts_cinema': '21010802106F0D', 'ch_stereo': '2101080210450D'}
        data = {'device_id': device_id, 'command': mode_dict[mode]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)

    def mute_volume(self, device_id, mute):
        mute_dict = {'on': '2101080210770D', 'off': '2101080210780D'}
        data = {'device_id': device_id, 'command': mute_dict[mute]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)



    def zone_two_mute(self, device_id, zone_mute):
        zone_mute_dict = {'on': '2101080217040D', 'off': '2101080217050D'}
        data = {'device_id': device_id, 'command': zone_mute_dict[zone_mute]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)


    def select_navigation(self, device_id, navigation):
        navigation_dict = {'up': '2101080210560D', 'down': '2101080210550D', 'left': '2101080210510D', 'right': '2101080210500D',
                           'enter': '2101080210570D', 'menu_on': '2101080210520D', 'menu_off': '21010802102B0D'}
        data = {'device_id': device_id, 'command': navigation_dict[navigation]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)


    def zone_two(self, device_id, zone_two):
        zone_dict = {
                        'on': '21010802177B0D',
                        'off': '21010802177C0D'
                    }
        data = {'device_id': device_id, 'command': zone_dict[zone_two]}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        response = requests.get(transmit_url)
        print(response.text)


    def set_volume_level(self, device_id, volume):
        volume_string = '{0:02x}'.format(volume).upper()
        #print(volume_string)
        #volume_string = '\x'+str(volume_string)
        # volume = volume.strip('')
        #vol_command = '\x21\x01\x0D\x01' + chr(volume) + '\x0D'
        vol_command = '21010D01' + volume_string + '0D'
        print(vol_command)
        data = {'device_id': device_id, 'command': vol_command}
        data = urllib.parse.urlencode(data)
        transmit_url = 'http://' + str(slave_ip) + ':5000/transmit?' + data
        print(transmit_url)
        response = requests.get(transmit_url)
        print(response.text)




