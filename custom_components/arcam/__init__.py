
"""
Component to interface with various media players.

For more details about this component, please refer to the documentation at
https://home-assistant.io/components/media_player/
"""
import asyncio
import base64
from datetime import timedelta
import functools as ft
import collections
import hashlib
import logging
from random import SystemRandom

from aiohttp import web
from aiohttp.hdrs import CONTENT_TYPE, CACHE_CONTROL
import async_timeout
import voluptuous as vol

from homeassistant.core import callback
from homeassistant.components.http import KEY_AUTHENTICATED, HomeAssistantView
from homeassistant.const import (
    STATE_OFF, STATE_IDLE, STATE_PLAYING, STATE_UNKNOWN, ATTR_ENTITY_ID,
    SERVICE_TOGGLE, SERVICE_TURN_ON, SERVICE_TURN_OFF, SERVICE_VOLUME_UP,
    SERVICE_MEDIA_PLAY, SERVICE_MEDIA_SEEK, SERVICE_MEDIA_STOP,
    SERVICE_VOLUME_SET, SERVICE_MEDIA_PAUSE, SERVICE_SHUFFLE_SET,
    SERVICE_VOLUME_DOWN, SERVICE_VOLUME_MUTE, SERVICE_MEDIA_NEXT_TRACK,
    SERVICE_MEDIA_PLAY_PAUSE, SERVICE_MEDIA_PREVIOUS_TRACK)
from homeassistant.helpers.aiohttp_client import async_get_clientsession
import homeassistant.helpers.config_validation as cv
from homeassistant.helpers.config_validation import PLATFORM_SCHEMA  # noqa
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.entity_component import EntityComponent
from homeassistant.loader import bind_hass
from homeassistant.components import websocket_api


try:
    from av_receiver import arcam_class
except ImportError:
    from .av_receiver import arcam_class

global class_obj
class_obj = arcam_class()

_LOGGER = logging.getLogger(__name__)
_RND = SystemRandom()

DOMAIN = 'arcam'
DEPENDENCIES = ['http']
SCAN_INTERVAL = timedelta(seconds=10)

ENTITY_ID_FORMAT = DOMAIN + '.{}'

ENTITY_IMAGE_URL = '/api/media_player_proxy/{0}?token={1}&cache={2}'
CACHE_IMAGES = 'images'
CACHE_MAXSIZE = 'maxsize'
CACHE_LOCK = 'lock'
CACHE_URL = 'url'
CACHE_CONTENT = 'content'
ENTITY_IMAGE_CACHE = {
    CACHE_IMAGES: collections.OrderedDict(),
    CACHE_MAXSIZE: 16
}

# SERVICE_PLAY_MEDIA = 'play_media'
# SERVICE_SELECT_SOURCE = 'select_source'
# SERVICE_SELECT_SOUND_MODE = 'select_sound_mode'
# SERVICE_CLEAR_PLAYLIST = 'clear_playlist'
#
# ATTR_MEDIA_VOLUME_LEVEL = 'volume_level'
# ATTR_MEDIA_VOLUME_MUTED = 'is_volume_muted'
# ATTR_MEDIA_SEEK_POSITION = 'seek_position'
# ATTR_MEDIA_CONTENT_ID = 'media_content_id'
# ATTR_MEDIA_CONTENT_TYPE = 'media_content_type'
# ATTR_MEDIA_DURATION = 'media_duration'
# ATTR_MEDIA_POSITION = 'media_position'
# ATTR_MEDIA_POSITION_UPDATED_AT = 'media_position_updated_at'
# ATTR_MEDIA_TITLE = 'media_title'
# ATTR_MEDIA_ARTIST = 'media_artist'
# ATTR_MEDIA_ALBUM_NAME = 'media_album_name'
# ATTR_MEDIA_ALBUM_ARTIST = 'media_album_artist'
# ATTR_MEDIA_TRACK = 'media_track'
# ATTR_MEDIA_SERIES_TITLE = 'media_series_title'
# ATTR_MEDIA_SEASON = 'media_season'
# ATTR_MEDIA_EPISODE = 'media_episode'
# ATTR_MEDIA_CHANNEL = 'media_channel'
# ATTR_MEDIA_PLAYLIST = 'media_playlist'
# ATTR_APP_ID = 'app_id'
# ATTR_APP_NAME = 'app_name'
# ATTR_INPUT_SOURCE = 'source'
# ATTR_INPUT_SOURCE_LIST = 'source_list'
# ATTR_SOUND_MODE = 'sound_mode'
# ATTR_SOUND_MODE_LIST = 'sound_mode_list'
# ATTR_MEDIA_ENQUEUE = 'enqueue'
# ATTR_MEDIA_SHUFFLE = 'shuffle'
#
# MEDIA_TYPE_MUSIC = 'music'
# MEDIA_TYPE_TVSHOW = 'tvshow'
# MEDIA_TYPE_MOVIE = 'movie'
# MEDIA_TYPE_VIDEO = 'video'
# MEDIA_TYPE_EPISODE = 'episode'
# MEDIA_TYPE_CHANNEL = 'channel'
# MEDIA_TYPE_PLAYLIST = 'playlist'
# MEDIA_TYPE_URL = 'url'
#
# SUPPORT_PAUSE = 1
# SUPPORT_SEEK = 2
# SUPPORT_VOLUME_SET = 4
# SUPPORT_VOLUME_MUTE = 8
# SUPPORT_PREVIOUS_TRACK = 16
# SUPPORT_NEXT_TRACK = 32
#
# SUPPORT_TURN_ON = 128
# SUPPORT_TURN_OFF = 256
# SUPPORT_PLAY_MEDIA = 512
# SUPPORT_VOLUME_STEP = 1024
# SUPPORT_SELECT_SOURCE = 2048
# SUPPORT_STOP = 4096
# SUPPORT_CLEAR_PLAYLIST = 8192
# SUPPORT_PLAY = 16384
# SUPPORT_SHUFFLE_SET = 32768
# SUPPORT_SELECT_SOUND_MODE = 65536
#
# # Service call validation schemas
# MEDIA_PLAYER_SCHEMA = vol.Schema({
#     ATTR_ENTITY_ID: cv.entity_ids,
# })
#
# MEDIA_PLAYER_SET_VOLUME_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_MEDIA_VOLUME_LEVEL): cv.small_float,
# })
#
# MEDIA_PLAYER_MUTE_VOLUME_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_MEDIA_VOLUME_MUTED): cv.boolean,
# })
#
# MEDIA_PLAYER_MEDIA_SEEK_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_MEDIA_SEEK_POSITION):
#         vol.All(vol.Coerce(float), vol.Range(min=0)),
# })
#
# MEDIA_PLAYER_SELECT_SOURCE_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_INPUT_SOURCE): cv.string,
# })
#
# MEDIA_PLAYER_SELECT_SOUND_MODE_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_SOUND_MODE): cv.string,
# })
#
# MEDIA_PLAYER_PLAY_MEDIA_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_MEDIA_CONTENT_TYPE): cv.string,
#     vol.Required(ATTR_MEDIA_CONTENT_ID): cv.string,
#     vol.Optional(ATTR_MEDIA_ENQUEUE): cv.boolean,
# })
#
# MEDIA_PLAYER_SET_SHUFFLE_SCHEMA = MEDIA_PLAYER_SCHEMA.extend({
#     vol.Required(ATTR_MEDIA_SHUFFLE): cv.boolean,
# })
#
#
# ATTR_TO_PROPERTY = [
#     ATTR_MEDIA_VOLUME_LEVEL,
#     ATTR_MEDIA_VOLUME_MUTED,
#     ATTR_MEDIA_CONTENT_ID,
#     ATTR_MEDIA_CONTENT_TYPE,
#     ATTR_MEDIA_DURATION,
#     ATTR_MEDIA_POSITION,
#     ATTR_MEDIA_POSITION_UPDATED_AT,
#     ATTR_MEDIA_TITLE,
#     ATTR_MEDIA_ARTIST,
#     ATTR_MEDIA_ALBUM_NAME,
#     ATTR_MEDIA_ALBUM_ARTIST,
#     ATTR_MEDIA_TRACK,
#     ATTR_MEDIA_SERIES_TITLE,
#     ATTR_MEDIA_SEASON,
#     ATTR_MEDIA_EPISODE,
#     ATTR_MEDIA_CHANNEL,
#     ATTR_MEDIA_PLAYLIST,
#     ATTR_APP_ID,
#     ATTR_APP_NAME,
#     ATTR_INPUT_SOURCE,
#     ATTR_INPUT_SOURCE_LIST,
#     ATTR_SOUND_MODE,
#     ATTR_SOUND_MODE_LIST,
#     ATTR_MEDIA_SHUFFLE,
# ]



async def async_setup(hass, config):
    """Track states and offer events for media_players."""
    component = hass.data[DOMAIN] = EntityComponent(
        logging.getLogger(__name__), DOMAIN, hass, SCAN_INTERVAL)

    print("Execution moves to av_receiver.py")
    await component.async_setup(config)
    print("Execution Returned to init.py")

    def turn_off(service):
        entity_id = service.data.get('entity_id')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.turn_off(device_id)

    def turn_on(service):
        entity_id = service.data.get('entity_id')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.turn_on(device_id)

    def select_source(service):
        entity_id = service.data.get('entity_id')
        source = service.data.get('source')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.select_source(device_id, source)

    def select_mode(service):
        entity_id = service.data.get('entity_id')
        mode = service.data.get('mode')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.select_mode(device_id, mode)

    def mute_volume(service):
        entity_id = service.data.get('entity_id')
        mute = service.data.get('mute')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.mute_volume(device_id, mute)

    def zone_2_mute_volume(service):
        entity_id = service.data.get('entity_id')
        zone_mute = service.data.get('zone_mute')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.zone_2_mute_volume(device_id, zone_mute)

    def select_navigation(service):
        entity_id = service.data.get('entity_id')
        navigation = service.data.get('navigation')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.select_navigation(device_id, navigation)

    def zone_two(service):
        entity_id = service.data.get('entity_id')
        zone2 = service.data.get('zone2')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.zone_two(device_id, zone2)

    def zone_two_mute(service):
        entity_id = service.data.get('entity_id')
        zone2_mute = service.data.get('zone2_mute')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.zone_two_mute(device_id, zone2_mute)

    def set_volume_level(service):
        entity_id = service.data.get('entity_id')
        volume = service.data.get('volume')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.set_volume_level(device_id, volume)

    def update_db(service):

        data = service.data.get('data')
        device_id = service.data.get('device_id')
        class_obj.update(hass, data, device_id)
        # print(data)

    service_dict = {
                        'turn_on': turn_on,
                        'turn_off': turn_off,
                        'select_source': select_source,
                        'select_mode': select_mode,
                        'select_navigation': select_navigation,
                        'zone_two': zone_two,
                        'zone_two_mute' : zone_two_mute,
                        'mute_volume': mute_volume,
                        'set_volume_level': set_volume_level,
                        'update_db': update_db
                    }
    for service in service_dict:
        hass.services.async_register(DOMAIN, service, service_dict[service])

    return True

