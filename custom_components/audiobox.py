import paramiko
import logging

username = "okas"
password = "mannan"

_LOGGER = logging.getLogger(__name__)
DOMAIN = 'audiobox'

def ssh_connect(host, user, password):
    try: 
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname=host, username=user,password=password)
        return ssh
    except Exception as error:
        _LOGGER.error('Connection Failed')
        _LOGGER.error(error)
    return False

def setup(hass, config):

    def execute_command(client, command):
        ssh_client = ssh_connect(client, username, password)
        if ssh_client is False:
            return False
        ssh_client.invoke_shell()
        stdin, stdout, stderr = ssh_client.exec_command(command)
        stdin.write(password + "\n")
        volume = stdout.read().decode('utf-8')
        _LOGGER.info(volume)
        _LOGGER.error(stderr.read().decode('utf-8'))
        return volume

    def set_volume(call):
        command = "amixer -D pulse sset Master " + call.data.get('volume')+ "%"
        client =  call.data.get('client')
        execute_command(client, command)
        hass.states.set('audiobox.volume',call.data.get('volume'))

    def get_volume(call):
        command = "amixer -D pulse sget Master | grep Left: | awk -F '[][]' '{print $2}'"
        client = call.data.get('client')
        volume = execute_command(client, command)
        vol = int(volume.replace("%", ""))
        hass.states.set('audiobox.volume',vol)

    hass.services.register(DOMAIN, 'set_volume', set_volume)
    hass.services.register(DOMAIN, 'get_volume', get_volume)
    return True
