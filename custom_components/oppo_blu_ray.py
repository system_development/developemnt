#!/usr/bin/env python

import paramiko

class oppo_bluray():
    """Class to encapsulate the projector.
    """

    def __init__(self,slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


#Based on the room parameter(configuration.yaml), we have to find out which sub-okas it is.Se configuration.yaml
        ip = self.slave_ip
        port= self.slave_port
        username= self.slave_user
        password= self.slave_pwd
        try:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(ip,port,username,password)
            print("Trying to establish connection through ssh")

        except Exception as error:
            print('Connection Failed')
            print(error)
            return

    def _command_handler(self, command_string):
        print("Invoking command handler")
        print("Command string is below")
        print(command_string)
        stdin,stdout,stderr = self.ssh.exec_command(command_string)
        print(stdout.readline())
        for line in iter(stdout.readline, ""):
            print(line, end="")
            print('finished.')
        return stdout




# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'oppo_blu_ray'

ATTR_NAME = 'entity_id'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('oppo_blu_ray')['serial_port']
    baudrate = config.get('oppo_blu_ray')['baudrate']
    device_id = config.get('oppo_blu_ray')['device_id']
    entity_id = ('oppo_blu_ray.' + str(device_id))
    slave_ip = config.get('oppo_blu_ray')['slave_ip']
    slave_port = config.get('oppo_blu_ray')['slave_port']
    slave_user = config.get('oppo_blu_ray')['slave_username']
    slave_pwd = config.get('oppo_blu_ray')['slave_password']

    def turn_on(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PON'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def turn_off(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'POF'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'off')

    def volume_up(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'VUP'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def volume_down(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'VDN'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def mute(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'MUT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def source(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'SRC'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def eject(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'EJT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def goto(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'GOT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def menu(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'MNU'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def home(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'HOM'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def up(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'NUP'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def down(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'NDN'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def left(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'NLT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def right(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'NRT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def back(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'RET'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def setup(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'SET'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def stop(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'STP'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def play(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PLY'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def pause(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PAU'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def previous(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'PRE'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def reverse(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'REV'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def forward(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'FWD'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def next(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'NXT'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def set_volume(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'SVL'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def top_menu(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'TTL'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def red(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'RED'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def yellow(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'YLW'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def green(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'GRN'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    def blue(call):
        name = call.data.get(ATTR_NAME, entity_id)
        oppo_blurayobj = oppo_bluray(slave_ip, slave_port, slave_user, slave_pwd)
        control_command = 'BLU'
        command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
        print("sending command")
        response_list = oppo_blurayobj._command_handler(command_string)
        if response_list:
            hass.states.set(entity_id,'on')

    hass.services.register(DOMAIN, 'volume_up', volume_up)
    hass.services.register(DOMAIN, 'volume_down', volume_down)
    hass.services.register(DOMAIN, 'mute', mute)
    hass.services.register(DOMAIN, 'goto', goto)
    hass.services.register(DOMAIN, 'source', source)
    hass.services.register(DOMAIN, 'eject', eject)
    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)
    hass.services.register(DOMAIN, 'red', red)
    hass.services.register(DOMAIN, 'yellow', yellow)
    hass.services.register(DOMAIN, 'home', home)
    hass.services.register(DOMAIN, 'up', up)
    hass.services.register(DOMAIN, 'down', down)
    hass.services.register(DOMAIN, 'left', left)
    hass.services.register(DOMAIN, 'right', right)
    hass.services.register(DOMAIN, 'previous', previous)
    hass.services.register(DOMAIN, 'pause', pause)
    hass.services.register(DOMAIN, 'reverse', reverse)
    hass.services.register(DOMAIN, 'forward', forward)
    hass.services.register(DOMAIN, 'next', next)
    hass.services.register(DOMAIN, 'set_volume', set_volume)
    hass.services.register(DOMAIN, 'back', back)
    hass.services.register(DOMAIN, 'setup', setup)
    hass.services.register(DOMAIN, 'stop', stop)
    hass.services.register(DOMAIN, 'play', play)
    hass.services.register(DOMAIN, 'menu', menu)
    hass.services.register(DOMAIN, 'green', green)
    hass.services.register(DOMAIN, 'blue', blue)
    hass.services.register(DOMAIN, 'top_menu', top_menu)



    # Return boolean to indicate that initialization was successfully.
    return True

