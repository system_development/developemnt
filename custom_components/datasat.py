#!/usr/bin/env python
import socket
import time
import requests
import json
import telnetlib

global devices_objects_list
devices_objects_list = []

class datasat_class(object):
    """Class to encapsulate the projector.
    """

    def create_connection(self, ip, port):
        # Connecting with the device on its IP and port.
        try:
            telnetObj = telnetlib.Telnet(ip, port)
            print("connection established")
            print(devices_objects_list)
            return telnetObj
        except Exception as error:
            print ('Failed to connect to client. Error message: ' + str(error))



    def transmit(self, command, object):
        try:
            print("command sent")
            object.write(command)

        except Exception as error:
            print ('Failed to transmit command. Error message: ' + str(error))
            return

    def receive(self,command, object):
        try:
            print("response received")
            response = object.read_until(command, timeout=0.5)
            return response
        except Exception as error:
            print ('Failed to receive command. Error message: ' + str(error))


    def close_connection(self, object):
        try:
            object.close()
            print("connection closed")
        except Exception as error:
            print ('Failed to close connection. Error message: ' + str(error))
            return


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'datasat'
ATTR_NAME = 'entity_id'
def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""

    global ip, port,device_id
    datasat_configs = config.get('datasat')
    print('Datasat configs')
    print(datasat_configs)
    class_obj = datasat_class()
    for config in datasat_configs:
        ip = config.get('datasat')['ip']
        port = config.get('datasat')['port']
        device_id = config.get('datasat')['device_id']
        entity_id = ('datasat.' + str(device_id))
        print(entity_id)
        print(ip)
        print(port)
        print(device_id)
        devices_objects_list.append({device_id: {'ip': ip, 'port': port}})
    print("Datasat AVP Device List Details")
    print(devices_objects_list)

        # class_obj.create_connection(device_id, ip, port)


    #If we want to use create_connection at the beginning
    # def power_on(call):
    #     control_command = '@POWER 1\r'
    #     name = call.data.get('entity_id')
    #     device_id = (str(entity_id).split("."))[1]
    #     for items in devices_objects_list:
    #         item_key = items.keys()
    #         if device_id in item_key:
    #             object = items[device_id]['object']
    #             response_list = class_obj.transmit(control_command, object,device_id)
    #             if 'POWER 1' in response_list:
    #                 state_object = hass.states.get(entity_id)
    #                 attributes_dict = {}
    #                 try:
    #                     attributes = hass.states.get(entity_id).attributes
    #                     attributes_dict.update(dict(attributes))
    #                     hass.states.set(entity_id, "on", attributes_dict)
    #                 except Exception as error:
    #                     print(error)
    #                     hass.states.set(entity_id, "on", attributes_dict)

    #If we want to make connection in each API and close it there itself.
    def turn_on(call):
        control_command = '@POWER 1\r'
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip,port)
                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if 'POWER 1' in response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        hass.states.set(entity_id, "on", attributes_dict)
                    except Exception as error:
                        print(error)
                        hass.states.set(entity_id, "on", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)

    def turn_off(call):
        control_command = '@POWER 0\r'
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip, port)
                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if 'POWER 1' in response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        hass.states.set(entity_id, "off", attributes_dict)
                    except Exception as error:
                        print(error)
                        hass.states.set(entity_id, "off", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)






    def set_volume_level(call):
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        volume = call.data.get('volume')
        volume_to_send = (100-volume)*7
        control_command = '@VOLUME '+str(volume_to_send)+'\r'
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip, port)
                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        attributes_dict['volume'] = volume
                        hass.states.set(entity_id, "on", attributes_dict)
                    except Exception as error:
                        print(error)
                        attributes_dict['volume'] = volume
                        hass.states.set(entity_id, "on", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)

    def mute_volume(call):
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        mute = call.data.get('mute')
        mute_dict = {'on': '@MUTED 1\r', 'off': '@MUTED 1\r'}
        control_command = mute_dict[mute]
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip, port)
                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        attributes_dict['mute'] = mute
                        hass.states.set(entity_id, "on", attributes_dict)
                    except Exception as error:
                        print(error)
                        attributes_dict['mute'] = mute
                        hass.states.set(entity_id, "on", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)


    def select_source(call):
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        source = call.data.get('source')
        #Source/INPUT NAMES to be obtained first and then replaced in the command

        # source_dict = {'CBL/SAT': '@INPUT Digital Cinema 1\r',
        #                'Media Player': '@INPUT Digital Cinema 2\r',
        #                'Blu-ray':'@INPUT Digital Cinema 3\r',
        #                'DVD':'@INPUT Digital Cinema 4\r',
        #                'CD':'@INPUT Digital Cinema 5\r',
        #                'AUX1':'@INPUT Digital Cinema 6\r',
        #                'AUX2':'@INPUT Digital Cinema 7\r',
        #                'TV Audio':'@INPUT Digital Cinema 8\r'}

        input_check_command = '@INPUTNAMES\r'
        input_list = []
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip, port)
                input_names_list = datasat_class_obj.receive(input_check_command, telnet_object)
                for items in input_names_list:
                    input_list.append(items)

                source_dict = {'hdmi1': '@INPUT'+input_list[0]+'\r',
                               'hdmi2': '@INPUT'+input_list[1]+'\r',
                               'hdmi3': '@INPUT'+input_list[2]+'\r',
                               'hdmi4': '@INPUT'+input_list[3]+'\r',
                               'hdmi5': '@INPUT'+input_list[4]+'\r',
                               'hdmi6': '@INPUT'+input_list[5]+'\r',
                               'hdmi7': '@INPUT'+input_list[6]+'\r',
                               'hdmi8': '@INPUT'+input_list[7]+'\r'}
                control_command = source_dict[source]

                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        attributes_dict['source'] = source
                        hass.states.set(entity_id, "on", attributes_dict)
                    except Exception as error:
                        print(error)
                        attributes_dict['source'] = source
                        hass.states.set(entity_id, "on", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)

    def select_mode(call):
        entity_id = call.data.get('entity_id')
        device_id = (str(entity_id).split("."))[1]
        mode = call.data.get('mode')

        # Mode Commands http://celadon.com/infrared-receiver-rs232-device-tables/DataSat-RS20i-RS232-Output-V3.1.htm
        mode_dict = {'DTS': '@DECODERPOST 3\r',
                    'DOLBYX': '@DECODERPOST 4\r',
                    'DOLBYZ': '@DECODERPOST 5\r',
                    'Neo Cinema':'@NEOXMODE 0\r',
                    'Neo Music':'@NEOXMODE 1\r',
                    'Neo Game':'@NEOXMODE 2\r',
                    'Pro Logic':'@DPL2MODE 0\r',
                    'Pro Music':'@DPL2MODE 1\r',
                    'Pro Movie':'@DPL2MODE 2\r'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                ip = items[device_id]['ip']
                port = items[device_id]['port']
                datasat_class_obj = datasat_class()
                telnet_object = datasat_class_obj.create_connection(ip, port)
                control_command = mode_dict[mode]

                datasat_class_obj.transmit(control_command, telnet_object)
                response_list = datasat_class_obj.receive(control_command, telnet_object)

                if response_list:
                    attributes_dict = {}
                    try:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        attributes_dict['mode'] = mode
                        hass.states.set(entity_id, "on", attributes_dict)
                    except Exception as error:
                        print(error)
                        attributes_dict['mode'] = mode
                        hass.states.set(entity_id, "on", attributes_dict)
                    datasat_class_obj.close_connection(telnet_object)




    service_dict = {'turn_on' : turn_on, 'turn_off': turn_off, 'mute_volume':mute_volume,'set_volume_level':set_volume_level,'select_source': select_source,'select_mode': select_mode }
    for service in service_dict:
        hass.services.register(DOMAIN, service, service_dict[service])




    # Return boolean to indicate that initialization was successfully.
    return True
