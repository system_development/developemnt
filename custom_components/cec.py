import paramiko

class cec_class():
	"""Class to encapsulate the projector.
	"""

	def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
		"""Set up serial connection to projector."""

		try:
			self.ssh = paramiko.SSHClient()
			self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
			self.ssh.connect(slave_ip,slave_port,slave_user,slave_pwd)
			print("Trying to establish connection through ssh")

		except Exception as error:
			print('Connection Failed')
			print(error)
			return

	def _command_handler(self, command_string):
		print("Invoking command handler")
		print("Command string is below")
		print(command_string)
		stdin,stdout,stderr = self.ssh.exec_command(command_string)
		print(stdout.readline())
		for line in iter(stdout.readline, ""):
			print(line, end="")
			print('finished.')
		return stdout


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'cec'
ATTR_NAME = 'entity_id'

def setup(hass, config):
	"""Set up is called when Home Assistant is loading our component."""


	def power_on(call):
		slave_ip = call.data.get('slave_ip')
		slave_port = call.data.get('slave_port')
		slave_user = call.data.get('slave_user')
		slave_pwd = call.data.get('slave_pwd')
		serial_port = call.data.get('serial_port')
		class_obj = cec_class(slave_ip, slave_port, slave_user, slave_pwd)
		print("sending command")
		command = 'echo "on 0" | cec-client -s '+str(serial_port)
		class_obj._command_handler(command)

	def power_off(call):
		slave_ip = call.data.get('slave_ip')
		slave_port = call.data.get('slave_port')
		slave_user = call.data.get('slave_user')
		slave_pwd = call.data.get('slave_pwd')
		serial_port = call.data.get('serial_port')
		class_obj = cec_class(slave_ip, slave_port, slave_user, slave_pwd)
		print("sending command")
		command = 'echo "standby 0" | cec-client -s '+str(serial_port)
		class_obj._command_handler(command)

	def select_source(call):
		slave_ip = call.data.get('slave_ip')
		slave_port = call.data.get('slave_port')
		slave_user = call.data.get('slave_user')
		slave_pwd = call.data.get('slave_pwd')
		source = call.data.get('source')
		serial_port = call.data.get('serial_port')
		class_obj = cec_class(slave_ip, slave_port, slave_user, slave_pwd)
		print("sending command")
		if source == 'HDMI1':
			command = 'echo "tx 1F:82:10:10" | cec-client -s '+str(serial_port)
		elif source == 'HDMI2':
			command = 'echo "tx 1F:82:20:10" | cec-client -s '+str(serial_port)
		class_obj._command_handler(command)

	hass.services.register(DOMAIN, 'power_on', power_on)
	hass.services.register(DOMAIN, 'power_off', power_off)
	hass.services.register(DOMAIN, 'select_source', select_source)

	# Return boolean to indicate that initialization was successfully.
	return True


