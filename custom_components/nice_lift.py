#!/usr/bin/env python

import time
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

_LOGGER = logging.getLogger(__name__)


class screenRemote():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self._timeout = 0.5
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'nice_lift'
ATTR_NAME = 'entity_id'
CERT_PATH=home+'/slave_certificates/'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('nice_lift')['serial_port']
    baudrate = config.get('nice_lift')['baudrate']
    device_id = config.get('nice_lift')['device_id']
    entity_id = ('nice_lift.' + str(device_id))
    slave_ip = config.get('nice_lift')['slave_ip']
    slave_port = config.get('nice_lift')['slave_port']
    slave_user = config.get('nice_lift')['slave_username']
    slave_pwd = config.get('nice_lift')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name



    def stop(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = b'CMD 02 04 03\r\n'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        if response.text:
            hass.states.set(entity_id , "stop")

    def deviceOut(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = b'CMD 02 04 05\r\n'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        if 'RSP 2 4 5' in response.text:
            hass.states.set(entity_id ,"out" )

    def deviceIn(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = b'CMD 02 04 04\r\n'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        if 'RSP 2 4 4' in response.text:
            hass.states.set(entity_id ,"in" )


    hass.services.register(DOMAIN, 'stop', stop)
    hass.services.register(DOMAIN, 'deviceOut', deviceOut)
    hass.services.register(DOMAIN, 'deviceIn', deviceIn)


    return True
