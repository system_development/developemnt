
import time, math, socket, json
from pathlib import Path

class remote:
	commands = {"power": 0, "select": 1, "backup": 2, "dismiss": 2, "channelup": 6, "channeldown": 7, "interactive": 8,
				"sidebar": 8, "help": 9, "services": 10, "search": 10, "tvguide": 11, "home": 11, "i": 14, "text": 15,
				"up": 16, "down": 17, "left": 18, "right": 19, "red": 32, "green": 33, "yellow": 34, "blue": 35, 0: 48,
				1: 49, 2: 50, 3: 51, 4: 52, 5: 53, 6: 54, 7: 55, 8: 56, 9: 57, "play": 64, "pause": 65, "stop": 66,
				"record": 67, "tv":240,"fastforward": 69, "rewind": 71, "boxoffice": 240, "sky": 241 , "plan": 242}

	connectTimeout = 1000;

	def showCommands(self):
		for command, value in self.commands.iteritems():
			print (str(command)+ " : "+str(value))

	def getCommand(self, code):
		try:
			print("command code received as")
			print(self.commands[code])
			return self.commands[code]
		except:
			print ("Error: command '"+code+"' is not valid")
			return False

	def press (self,sequence, ip, port):
		if isinstance(sequence, list):
			for item in sequence:
				toSend=self.getCommand(item)
				if toSend >= 0:
					self.sendCommand(toSend, ip, port)
					time.sleep(0.1)

		else:
			toSend=self.getCommand(sequence)
			if toSend >= 0:
				self.sendCommand(toSend, ip, port)


	def sendCommand(self,code, ip, port):
		print("sendCommand")
		commandBytes1= bytearray([4,1,0,0,0,0, int(math.floor(224 + (code/16))), code % 16])
		commandBytes2= bytearray([4,0,0,0,0,0, int(math.floor(224 + (code/16))), code % 16])
		print(commandBytes1)
		print(commandBytes2)

		try:
			client=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			print(client)
		except:
			print ('Failed to create socket. Error code: ' + str(msg[0]) + ' , Error message : ' + msg[1])
			return

		try:
			client.connect((ip, port))
		except:
			print ("Failed to connect to client")
			return

		l=12
		timeout=time.time()+self.connectTimeout

		while 1:
			data=client.recv(24)
			print("response received")
			print(data)
			data=data
			count =0
			while(count < 3):
				count = count+1

			if len(data)<24:
				client.sendall(data[0:l])
				l=1
			else:
				client.sendall(commandBytes1)
				client.sendall(commandBytes2)
				commandBytes1[1]=0
				commandBytes2[1]=0
				client.sendall(commandBytes1)
				client.sendall(commandBytes2)
				client.close()
				break

			if time.time() > timeout:
				print ("timeout error")
				client.close()
				break

	
# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'tatasky'

def setup(hass, config):
	"""Set up is called when Home Assistant is loading our component."""

	global ip, port,device_id, dict_ip, dict_telnet, class_obj, entity_id, channel_json
	tatasky_configs = config.get('tatasky')
	dict_telnet = {}
	class_obj = remote()
	home = str(Path.home())
	with open(home + '/channel_list.json', encoding='utf-8') as data_file:
		channel_json = json.loads(data_file.read())

	def power_on(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "tv"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on")

	def power_off(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "power"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "off")

	def up(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "up"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id , "on", {"volume":"up"})

	def down(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "down"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"volume":"down"})

	def guide(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "tvguide"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"guide"})

	def select(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "select"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"select"})

	def backup(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "backup"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"backup"})

	def dismiss(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "dismiss"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"dismiss"})

	def interactive(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "interactive"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"interactive"})

	def sidebar(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "sidebar"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"sidebar"})

	def help(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "help"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"help"})

	def services(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "services"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"services"})

	def search(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "search"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"search"})

	def home(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "home"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"home"})

	def i(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "i"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"i"})

	def text(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "text"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"text"})

	def channelup(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "channelup"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"channel":"up"})

	def channeldown(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "channeldown"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"channel":"down"})

	def channel_set(call):
		entity_id = call.data.get("entity_id")
		channel_no = call.data.get('channel_no')
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = channel_no
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)

	def set_channel_name(call):
		entity_id = call.data.get("entity_id")
		channel_name = call.data.get('channel_name')

		if isinstance(entity_id, str):
			print("entity is string")
			device_id = (str(entity_id).split("."))[1]
			print(device_id)
		else:
			print("entity is list")
			device_id = (str(entity_id[0]).split("."))[1]
			print(device_id)
			entity_id = entity_id[0]

		class_obj = remote()
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']

		channel_number = []
		for genres, channel_list in channel_json.items():
			for channels in range(len(channel_list)):
				if channel_list[channels]['channel_name'] == channel_name:
					channel_number = channel_list[channels]['channel_Array']

		for channel in range(len(channel_number)):
			channel_value = int(channel_number[channel])
			class_obj.press(channel_value,ip, port)
			time.sleep(0.15)


	def left(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "left"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"left"})

	def right(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "right"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"right"})

	def red(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "red"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"red"})

	def green(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "green"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"green"})

	def yellow(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "yellow"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"yellow"})

	def blue(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "blue"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"blue"})

	def play(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "play"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"play"})

	def pause(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "pause"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"pause"})

	def stop(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "stop"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"stop"})

	def record(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "record"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"record"})

	def fastforward(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "fastforward"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"fastforward"})

	def rewind(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "rewind"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"rewind"})

	def plan(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "plan"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"plan"})
	def tv(call):
		entity_id = call.data.get("entity_id")
		if isinstance(entity_id, str):
			device_id = (str(entity_id).split("."))[1]
		else:
			device_id = (str(entity_id[0]).split("."))[1]
			entity_id = entity_id[0]
		class_obj = remote()
		typed = "tv"
		for config in tatasky_configs:
			if config['device_id'] == device_id:
				ip = config['ip']
				port = config['port']
		class_obj.press(typed,ip, port)
		hass.states.set(entity_id, "on", {"operation":"tv"})

	hass.services.register(DOMAIN, 'power_on', power_on)
	hass.services.register(DOMAIN, 'power_off', power_off)
	hass.services.register(DOMAIN, 'down', down)
	hass.services.register(DOMAIN, 'tvguide', guide)
	hass.services.register(DOMAIN, 'up', up)
	hass.services.register(DOMAIN, 'select', select)
	hass.services.register(DOMAIN, 'backup', backup)
	hass.services.register(DOMAIN, 'dismiss', dismiss)
	hass.services.register(DOMAIN, 'interactive', interactive)
	hass.services.register(DOMAIN, 'sidebar', sidebar)
	hass.services.register(DOMAIN, 'help', help)
	hass.services.register(DOMAIN, 'services', services)
	hass.services.register(DOMAIN, 'search', search)
	hass.services.register(DOMAIN, 'home', home)
	hass.services.register(DOMAIN, 'i', i)
	hass.services.register(DOMAIN, 'text', text)
	hass.services.register(DOMAIN, 'channelup', channelup)
	hass.services.register(DOMAIN, 'channeldown', channeldown)
	hass.services.register(DOMAIN, 'left', left)
	hass.services.register(DOMAIN, 'right', right)
	hass.services.register(DOMAIN, 'red', red)
	hass.services.register(DOMAIN, 'green', green)
	hass.services.register(DOMAIN, 'yellow', yellow)
	hass.services.register(DOMAIN, 'blue', blue)
	hass.services.register(DOMAIN, 'channel_set', channel_set)
	hass.services.register(DOMAIN, 'set_channel_name', set_channel_name)
	hass.services.register(DOMAIN, 'play', play)
	hass.services.register(DOMAIN, 'pause', pause)
	hass.services.register(DOMAIN, 'stop', stop)
	hass.services.register(DOMAIN, 'record', record)
	hass.services.register(DOMAIN, 'fastforward', fastforward)
	hass.services.register(DOMAIN, 'rewind', rewind)
	hass.services.register(DOMAIN, 'plan', plan)
	hass.services.register(DOMAIN, 'tv', tv)

	# Return boolean to indicate that initialization was successfully.
	return True


