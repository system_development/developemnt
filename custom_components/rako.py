
#!/usr/bin/env python
import socket
import time
import requests
import json

class rako(object):
    """Class to encapsulate the projector.
    """

    def __init__(self):
        self.ip=ip
        self.port=port

        # Creating socket connection.
        try:
            self.client=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            print("socket connection created")
        except Exception as error:
            print ('Failed to create socket. Error message: ' + str(error))

    def create_connection(self):
        # Connecting with the device on its IP and port.
        try:
            self.client.connect((self.ip, self.port))
            print("connection established")
        except Exception as error:
            print ('Failed to connect to client. Error message: ' + str(error))
            return

    def transmit(self, command):
        try:
            print("command sent")
            self.client.sendall(command.encode())
            print(command.encode())
        except Exception as error:
            print ('Failed to transmit command. Error message: ' + str(error))
            return

    def receive(self):
        try:
            print("response received")
            response = self.client.recv(4096)
            print(response.decode())
            return response
        except Exception as error:
            print ('Failed to receive command. Error message: ' + str(error))


    def close_connection(self):
        try:
            self.client.close()
            print("connection closed")
        except Exception as error:
            print ('Failed to close connection. Error message: ' + str(error))
            return


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'rako'
ATTR_NAME = 'entity_id'
def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""

    global ip, port,device_id
    ip = config.get('rako')['ip']
    port = config.get('rako')['port']
    device_id = config.get('rako')['device_id']
    entity_id = ('rako.' + str(device_id))
    print(entity_id)
    print(ip)
    print(port)

    def scene_on(call):
        name = call.data.get(ATTR_NAME, entity_id)
        rakoObj = rako()
        rakoObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&sc:"+"1"+"+\r\n"
        rakoObj.transmit(command_string)
        response = rakoObj.receive()
        print(response)
        hass.states.set(entity_id, "\nOK", {"operation":"scene_on"})
        rakoObj.close_connection()

    def scene_off(call):
        name = call.data.get(ATTR_NAME, entity_id)
        rakoObj = rako()
        rakoObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&sc:"+"0"+"+\r\n"
        rakoObj.transmit(command_string)
        response = rakoObj.receive()
        print(response)
        hass.states.set(entity_id, "\nOK", {"operation":"scene_off"})
        rakoObj.close_connection()

    def brightness_level1(call):
        name = call.data.get(ATTR_NAME, entity_id)
        rakoObj = rako()
        rakoObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&lev:"+"150"+"+\r\n"
        rakoObj.transmit(command_string)
        response = rakoObj.receive()
        print(response)
        hass.states.set(entity_id, "\nOK", {"operation":"brightness_level1"})
        rakoObj.close_connection()

    def brightness_level2(call):
        name = call.data.get(ATTR_NAME, entity_id)
        rakoObj = rako()
        rakoObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&lev:"+"100"+"+\r\n"
        rakoObj.transmit(command_string)
        response = rakoObj.receive()
        print(response)
        hass.states.set(entity_id, "\nOK", {"operation":"brightness_level2"})
        rakoObj.close_connection()

    def brightness_level3(call):
        name = call.data.get(ATTR_NAME, entity_id)
        rakoObj = rako()
        rakoObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&lev:"+"50"+"+\r\n"
        rakoObj.transmit(command_string)
        response = rakoObj.receive()
        print(response)
        hass.states.set(entity_id, "\nOK", {"operation":"brightness_level3"})
        rakoObj.close_connection()

    def brightness_level4(call):
        name = call.data.get(ATTR_NAME, entity_id)

        trinovObj = trinov()
        trinovObj.create_connection()
        command_string = "ro:"+"9"+"&ch:"+"3"+"&lev:"+"250"+"+\r\n"
        trinovObj.transmit(command_string)
        response = rakoObj.receive()
        hass.states.set(entity_id, "\nOK", {"operation":"brightness_level4"})
        rakoObj.close_connection()





    hass.services.register(DOMAIN, 'scene_on', scene_on)
    hass.services.register(DOMAIN, 'scene_off', scene_off)
    hass.services.register(DOMAIN, 'brightness_level1', brightness_level1)
    hass.services.register(DOMAIN, 'brightness_level2', brightness_level2)
    hass.services.register(DOMAIN, 'brightness_level3', brightness_level3)
    hass.services.register(DOMAIN, 'brightness_level4', brightness_level4)


    # Return boolean to indicate that initialization was successfully.
    return True
