import time, math, socket, struct, time, threading, select
import telnetlib
import logging
import telnetlib
import asyncio
import threading

import voluptuous as vol
import simplejson as json
from xml.etree import ElementTree as ET
from pathlib import Path

from homeassistant.helpers.entity import Entity

import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)
home = str(Path.home())
global devices_objects_list, master_box_ip
devices_objects_list = []
try:
    with open(home+'/si_data.json') as data_file:
        data = json.loads(data_file.read())
    initial_dict = dict(data)

    okas_boxes = initial_dict['okasBox']
    for boxes in okas_boxes:
        box_type = boxes['okasboxtype']
        if box_type == 'Master Controller':
            master_box_ip = boxes['okasboxip']
            _LOGGER.info("\n\nMaster Box IP is retrieved and  it is:")
            _LOGGER.info(master_box_ip)
except Exception as e:
    _LOGGER.info(e)



XMC_PING = '<?xml version="1.0" encoding="utf-8"?><emotivaPing />'.encode('utf-8')
XMC_COMMAND = '<?xml version="1.0" encoding="utf-8"?><emotivaControl><commandName value="commandValue" ack="yes" /></emotivaControl>'.encode('utf-8')
XMC_SUBSCRIBE='<?xml version="1.0" encoding="utf-8"?><emotivaSubscription><power /><volume /><source /><zone2_power /><zone2_volume /><zone2_input /><mode /></emotivaSubscription>'.encode('utf-8')
XMC_UPDATE='<?xml version="1.0" encoding="utf-8"?><emotivaUpdate><power /><zone2_power /><volume /><zone2_volume /><source /><zone2_input /><mode /></emotivaUpdate>'.encode('utf-8')


#IPs should be taken from configuration and PORTs should be taken from the TRANSPONDER RESPONSE PACKET
XMC_IPADDRESS = "192.168.0.70"
XMC_TRANSPONDER_PORT = 7000
MASTER_TRANSPONDER_PORT = 7001
XMC_CONTROL_PORT = 7002
XMC_NOTIFY_PORT = 7003

MESSAGE_PING = [XMC_PING, XMC_IPADDRESS, XMC_TRANSPONDER_PORT]
MESSAGE_SUB = [XMC_SUBSCRIBE, XMC_IPADDRESS, XMC_CONTROL_PORT]
MESSAGE_UPDATE = [XMC_UPDATE, XMC_IPADDRESS, XMC_CONTROL_PORT]
MESSAGE_COMMAND  = [XMC_COMMAND, XMC_IPADDRESS, XMC_CONTROL_PORT]





def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up the Emotiva platform."""
    global ip, port, device_id, class_obj, entity_domain,master_box_ip, notify_port


    entity_domain = 'emotiva'
    emotiva_configs = dict(config)
    _LOGGER.info("Printing emotiva config below")
    _LOGGER.info(emotiva_configs)
    ip = emotiva_configs['ip']
    port = emotiva_configs['port']
    device_id = emotiva_configs['device_id']
    class_obj =  emotiva_class()
    class_obj.create_connection(device_id, ip, port)
    _LOGGER.info("About to call ping")
    class_obj.ping(ip, port, device_id)
    time.sleep(5)
    _LOGGER.info("About to call subscribe")

    class_obj.subscribe(ip, port, device_id)

    _LOGGER.info("Before call to update function")
    # hass.loop.create_task(class_obj.update(hass))
    # hass.async_add_job(class_obj.update(hass))
    # class_obj.update(hass)
    update_thread = threading.Thread(target=class_obj.update, args=(hass,))
    update_thread.start()

    def ping_api(call):
        message = MESSAGE_PING[0]
        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sUDP.sendto(message, (ip, XMC_TRANSPONDER_PORT))
        listenUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        listenUDP.bind((master_box_ip, MASTER_TRANSPONDER_PORT))
        listen = listenUDP.recv(4096)
        _LOGGER.info("\n\nListen from 7001 on master\n\n")
        xml_data = ET.fromstring(listen)
        data_to_find = xml_data.find(".//name")
        name_XMC = data_to_find.text
        data_to_find = xml_data.find(".//controlPort")
        control_port = data_to_find.text
        data_to_find = xml_data.find(".//notifyPort")
        notify_port = data_to_find.text
        _LOGGER.info(listen)
        _LOGGER.info("\n\nPrinting the control port and notify port\n\n")
        _LOGGER.info(name_XMC, control_port, notify_port)
        listenUDP.close()
        sUDP.close()

    def subscribe_api(call):
        _LOGGER.info("\n\nCall made to subscribe function\n\n")
        message = MESSAGE_SUB[0]
        _LOGGER.info(message)
        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _LOGGER.info(sUDP)
        sUDP.sendto(message, (ip, XMC_CONTROL_PORT))
        listenUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _LOGGER.info(listenUDP)
        _LOGGER.info(master_box_ip)
        listenUDP.bind((master_box_ip, XMC_CONTROL_PORT))
        listen = listenUDP.recv(4096)
        _LOGGER.info("\n\nListen from 7001 on master subscribe\n\n")
        # xml_data = ET.fromstring(listen)
        _LOGGER.info(listen)

        listenUDP.close()

        sUDP.close()

    hass.services.register('emotiva', 'ping_api', {}, False)
    hass.services.register('emotiva', 'subscribe_api', {}, False)


class emotiva_class():

    def create_connection(self, device_id, ip, port):

        try:
            sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            devices_objects_list.append({device_id: {'ip': ip, 'port': port, 'object': sUDP}})
            _LOGGER.info(devices_objects_list)

        except Exception as error:
            _LOGGER.error('Failed to create connection. Error message: ' + str(error))

    def ping(self, ip, port, device_id):
        message = MESSAGE_PING[0]
        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sUDP.sendto(message, (ip, XMC_TRANSPONDER_PORT))
        listenUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        listenUDP.bind((master_box_ip, MASTER_TRANSPONDER_PORT))
        listen = listenUDP.recv(4096)
        _LOGGER.info("\n\nListen from 7001 on master\n\n")
        xml_data = ET.fromstring(listen)
        data_to_find = xml_data.find(".//name")
        name_XMC = data_to_find.text
        data_to_find = xml_data.find(".//controlPort")
        control_port = data_to_find.text
        data_to_find = xml_data.find(".//notifyPort")
        notify_port = data_to_find.text
        _LOGGER.info(listen)
        _LOGGER.info("\n\nPrinting the control port and notify port\n\n")
        _LOGGER.info(name_XMC, control_port, notify_port)
        listenUDP.close()
        sUDP.close()



    def subscribe(self, ip, port, device_id):
        _LOGGER.info("\n\nCall made to subscribe function\n\n")
        message = MESSAGE_SUB[0]
        _LOGGER.info(message)
        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _LOGGER.info(sUDP)
        sUDP.sendto(message, (ip, XMC_CONTROL_PORT))
        listenUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        _LOGGER.info(listenUDP)
        _LOGGER.info(master_box_ip)
        listenUDP.bind((master_box_ip, XMC_CONTROL_PORT))
        listen = listenUDP.recv(4096)
        _LOGGER.info("\n\nListen from 7001 on master subscribe\n\n")
        # xml_data = ET.fromstring(listen)
        _LOGGER.info(listen)

        listenUDP.close()

        sUDP.close()




    def transmit(self, hass, command_list, object, device_id):
        try:
            for command in command_list:
                _LOGGER.info("command sent")
                object.write(command)
                _LOGGER.info(command)
                time.sleep(0.5)

        except Exception as error:
            _LOGGER.error('Failed to transmit command. Error message: ' + str(error))
            _LOGGER.info("connection re-establishing")
            time.sleep(1)
            new_device_dict = {}
            for items in devices_objects_list:
                item_key = items.keys()
                if device_id in item_key:
                    ip = items[device_id]['ip']
                    port = items[device_id]['port']
                    telnetObj = telnetlib.Telnet(ip, port, timeout=0.5)
                    new_device_dict.update({'ip': ip, 'port': port, 'object': telnetObj})
                    items[device_id] = new_device_dict
                    for command in command_list:
                        telnetObj.write(command)
                        time.sleep(0.5)
                    self.update(hass)
                    break
            _LOGGER.info(devices_objects_list)


    def update(self, hass):
        _LOGGER.info("Call made to update function")
        while 1:

            _LOGGER.info("Inside the while loop")
            for devices in devices_objects_list:
                for key, value in devices.items():
                    _LOGGER.info("Inside while loop")
                    _LOGGER.info(key,value)
                    # device_object = value['object']
                    entity_id = entity_domain + '.' + key
                    response_UDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    response_UDP.bind((master_box_ip, 7003))

                    response_realtime = response_UDP.recvfrom(4096)
                    _LOGGER.info("Listening")
                    _LOGGER.info(response_realtime)
                    actual_response = response_realtime[0]
                    _LOGGER.info("actual response")
                    _LOGGER.info(actual_response)
                    sender_device_ip = response_realtime[1]
                    root = ET.fromstring(actual_response)
                    _LOGGER.info("actual response--- root")
                    _LOGGER.info(root)

                    attributes_dict = {}
                    try:

                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))
                        if actual_response:
                            _LOGGER.info("there is a response in try")
                            _LOGGER.info(actual_response)

                            for child in root:
                                _LOGGER.info("finding children in try")
                                _LOGGER.info(child)
                                _LOGGER.info(child.tag)


                                if child.tag == 'mode':
                                    _LOGGER.info("mode tag found")
                                    feedback_dict = {'All Stereo': 'all_stereo',
                                                     'Stereo': 'stereo',
                                                     'Auto': 'auto',
                                                     'Direct': 'direct',
                                                     'Dolby Surround': 'dolby',
                                                     'Reference Stereo':'reference_stereo',
                                                     'Preset1': 'preset1',
                                                     'Preset2': 'preset2'}
                                    received_value = child.attrib['value']
                                    _LOGGER.info(received_value)
                                    if received_value in feedback_dict.keys():
                                        _LOGGER.info("received value in feedback dict")
                                        attributes_dict['mode'] = feedback_dict[received_value]
                                        _LOGGER.info("Setting Mode")
                                        main_zone_state = hass.states.get(entity_id).state
                                        hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if child.tag == 'source':
                                    _LOGGER.info("\n\nsource tag found\n\n")
                                    feedback_dict = {'HDMI 1': 'hdmi1',
                                                   'HDMI 2': 'hdmi2',
                                                   'HDMI 3': 'hdmi3',
                                                   'HDMI 4': 'hdmi4',
                                                   'HDMI 5': 'hdmi5',
                                                   'HDMI 6': 'hdmi6',
                                                   'HDMI 7': 'hdmi7',
                                                   'HDMI 8': 'hdmi8',
                                                   'Analog 1': 'analog1',
                                                   'Analog 2': 'analog2',
                                                   'Analog 3': 'analog3',
                                                   'Coax 1':'coax1',
                                                   'Coax 2':'coax2',
                                                   'Coax 3':'coax3',
                                                   'Coax 4':'coax4',
                                                   'Optical 1':'optical1',
                                                   'Optical 2':'optical2',
                                                   'Optical 3':'optical3',
                                                   'Optical 4':'optical4'
                                                   }
                                    received_value = child.attrib['value']
                                    _LOGGER.info(received_value)
                                    if received_value in feedback_dict.keys():
                                        _LOGGER.info("received value in feedback dict")
                                        attributes_dict['source'] = feedback_dict[received_value]
                                        _LOGGER.info("Setting Source")
                                        main_zone_state = hass.states.get(entity_id).state
                                        hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if child.tag == 'volume':
                                    _LOGGER.info("\n\nvolume tag found\n\n")
                                    received_value = child.attrib['value']
                                    if received_value == 'Mute':
                                        attributes_dict['mute'] = 'on'
                                    elif received_value == '-':
                                        attributes_dict['mute'] = 'off'
                                    else:
                                        if received_value[1].isspace():
                                            device_volume = received_value[0]+received_value[2:]
                                            device_volume = int(device_volume.split(".")[0])
                                        else:
                                            device_volume = int(received_value.split(".")[0])
                                        _LOGGER.info("Volume received from device is",device_volume, type(device_volume))
                                        if device_volume > -90:
                                            _LOGGER.info("Inside volume")
                                            attributes_dict['volume'] = device_volume + 89
                                            attributes_dict['mute'] = 'off'
                                        elif device_volume <= -90 and device_volume > -97:
                                            _LOGGER.info("Inside volume")
                                            attributes_dict['volume'] = 0
                                            attributes_dict['mute'] = 'off'
                                            main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state,attributes_dict)

                                if child.tag == 'power':
                                    _LOGGER.info("\n\npower tag found\n\n")
                                    received_value = child.attrib['value']
                                    _LOGGER.info("\n\nReceived value for power\n\n")
                                    _LOGGER.info(received_value)
                                    if received_value == 'On':
                                        _LOGGER.info("Received Call for power is On")
                                        hass.states.set(entity_id, "on", attributes_dict)
                                    elif received_value == 'Off':
                                        attributes_dict['source'] = None
                                        _LOGGER.info("Received Call for power is Off")
                                        hass.states.set(entity_id, "off", attributes_dict)








                    except Exception as e:
                        _LOGGER.info(e)
                        if actual_response:
                            _LOGGER.info("there is a response in except")

                            for child in root:
                                _LOGGER.info("finding children in except")
                                _LOGGER.info(child)
                                _LOGGER.info(child.tag)


                                if child.tag == 'mode':
                                    _LOGGER.info("mode tag found")
                                    feedback_dict = {'All Stereo': 'all_stereo',
                                                     'Stereo': 'stereo',
                                                     'Auto': 'auto',
                                                     'Direct': 'direct',
                                                     'Dolby Surround': 'dolby',
                                                     'Reference Stereo': 'reference_stereo',
                                                     'Preset1': 'preset1',
                                                     'Preset2': 'preset2'}
                                    received_value = child.attrib['value']
                                    _LOGGER.info(received_value)
                                    if received_value in feedback_dict.keys():
                                        _LOGGER.info("received value in feedback dict")
                                        attributes_dict['mode'] = feedback_dict[received_value]
                                        _LOGGER.info("Setting Mode")
                                        # main_zone_state = hass.states.get(entity_id).state
                                        hass.states.set(entity_id, "on", attributes_dict)
                                if child.tag == 'source':
                                    _LOGGER.info("\n\nsource tag found\n\n")
                                    feedback_dict = {'HDMI 1': 'hdmi1',
                                                   'HDMI 2': 'hdmi2',
                                                   'HDMI 3': 'hdmi3',
                                                   'HDMI 4': 'hdmi4',
                                                   'HDMI 5': 'hdmi5',
                                                   'HDMI 6': 'hdmi6',
                                                   'HDMI 7': 'hdmi7',
                                                   'HDMI 8': 'hdmi8',
                                                   'Analog 1': 'analog1',
                                                   'Analog 2': 'analog2',
                                                   'Analog 3': 'analog3',
                                                   'Coax 1':'coax1',
                                                   'Coax 2':'coax2',
                                                   'Coax 3':'coax3',
                                                   'Coax 4':'coax4',
                                                   'Optical 1':'optical1',
                                                   'Optical 2':'optical2',
                                                   'Optical 3':'optical3',
                                                   'Optical 4':'optical4'
                                                   }
                                    received_value = child.attrib['value']
                                    _LOGGER.info(received_value)
                                    if received_value in feedback_dict.keys():
                                        _LOGGER.info("received value in feedback dict")
                                        attributes_dict['source'] = feedback_dict[received_value]
                                        _LOGGER.info("Setting Source")
                                        # main_zone_state = hass.states.get(entity_id).state
                                        hass.states.set(entity_id, "on", attributes_dict)


                                if child.tag == 'volume':
                                    _LOGGER.info("\n\nvolume tag found\n\n")
                                    received_value = child.attrib['value']
                                    if received_value == 'Mute':
                                        attributes_dict['mute'] = 'on'
                                    elif received_value == '-':
                                        attributes_dict['mute'] = 'off'
                                    else:
                                        if received_value[1].isspace():
                                            device_volume = received_value[0] + received_value[2:]
                                            device_volume = int(device_volume.split(".")[0])
                                        else:
                                            device_volume = int(received_value.split(".")[0])
                                        _LOGGER.info("Volume received from device is", device_volume, type(device_volume))
                                        if device_volume > -90:
                                            _LOGGER.info("Inside volume")
                                            attributes_dict['volume'] = device_volume + 89
                                            attributes_dict['mute'] = 'off'
                                        elif device_volume <= -90 and device_volume > -97:
                                            _LOGGER.info("Inside volume")
                                            attributes_dict['volume'] = 0
                                            attributes_dict['mute'] = 'off'
                                    hass.states.set(entity_id, 'on', attributes_dict)

                                if child.tag == 'power':
                                    _LOGGER.info("\n\npower tag found\n\n")
                                    received_value = child.attrib['value']
                                    _LOGGER.info("Received Call for power", received_value)
                                    if received_value == 'On':
                                        hass.states.set(entity_id, "on", attributes_dict)
                                    elif received_value == 'Off':
                                        attributes_dict['source'] = None
                                        hass.states.set(entity_id, "off", attributes_dict)

    def turn_off(self, hass, device_id, zone):
        class_obj = emotiva_class()
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                device_port = items[device_id]['ip']
                class_obj.ping(device_ip, device_port, device_id)
                class_obj.subscribe(device_ip, device_port, device_id)
                if zone is None:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), 'power_off'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                    # sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    # sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                    # _LOGGER.info("Power Off command send to Emotiva")
                    # sUDP.close()
                elif zone == 2:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), 'zone2_power_off'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                _LOGGER.info("Power Off command send to Emotiva")
                sUDP.close()
        class_obj.ping(device_ip, device_port, device_id)
        class_obj.subscribe(device_ip, device_port, device_id)

    def turn_on(self, hass, device_id, zone):
        class_obj = emotiva_class()
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                device_port = items[device_id]['ip']
                class_obj.ping(device_ip, device_port, device_id)
                class_obj.subscribe(device_ip, device_port, device_id)
                if zone is None:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), 'power_on'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                    sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    # sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                    # _LOGGER.info("Power On command send to Emotiva")
                    # sUDP.close()
                elif zone == 2:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'),'zone2_power_on'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                _LOGGER.info("Power On command send to Emotiva")
                sUDP.close()

    def select_source(self, hass, device_id, source, zone):
        source_dict = {'hdmi1': 'hdmi1',
                       'hdmi2': 'hdmi2',
                       'hdmi3': 'hdmi3',
                       'hdmi4': 'hdmi4',
                       'hdmi5': 'hdmi5',
                       'hdmi6': 'hdmi6',
                       'hdmi7': 'hdmi7',
                       'hdmi8': 'hdmi8',
                       'analog1': 'analog1',
                       'analog2': 'analog2',
                       'analog3': 'analog3',
                       'coax1':'coax1',
                       'coax2':'coax2',
                       'coax3':'coax3',
                       'coax4':'coax4',
                       'optical1':'optical1',
                       'optical2':'optical2',
                       'optical3':'optical3',
                       'optical4':'optical4'
                       }

        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                if zone is None:
                    if source not in source_dict.keys():
                        _LOGGER.info(source, " is not a valid source for main zone\n")
                    else:
                        message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), source_dict[source].encode('utf-8'))
                        message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                        sUDP.close()
                elif zone == 2:
                    zone2_source_dict = {
                                   'analog1': 'zone2_analog1',
                                   'analog2': 'zone2_analog2',
                                   'analog3': 'zone2_analog3',
                                   'analog4': 'zone2_analog4',
                                   'analog5': 'zone2_analog5',
                                   'analog7': 'zone2_analog71',
                                   'analog8': 'zone2_analog8',
                                   'coax1': 'zone2_coax1',
                                   'coax2': 'zone2_coax2',
                                   'coax3': 'zone2_coax3',
                                   'coax4': 'zone2_coax4',
                                   'optical1': 'zone2_optical1',
                                   'optical2': 'zone2_optical2',
                                   'optical3': 'zone2_optical3',
                                   'optical4': 'zone2_optical4'
                                   }
                    if source not in zone2_source_dict.keys():
                        _LOGGER.info(source,"is not a valid source for zone2\n")
                    else:
                        zone2_source = zone2_source_dict[source]
                        message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), zone2_source.encode('utf-8'))
                        message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                        sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                        sUDP.close()



    def select_mode(self, hass, device_id, mode):
        mode_dict = {'all_stereo': 'all_stereo',
                     'stereo': 'stereo',
                     'auto': 'auto',
                     'direct': 'direct',
                     'dolby': 'dolby',
                     'reference_stereo':'reference_stereo',
                     'preset1': 'preset1',
                     'preset2': 'preset2'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), mode_dict[mode].encode('utf-8'))
                message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                sUDP.close()

    def mute_volume(self, hass, device_id, mute, zone):
        mute_dict = {'on': 'mute_on', 'off': 'mute_off'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                if zone is None:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), mute_dict[mute].encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                elif zone == 2:
                    zone2_mute = 'zone2_'+str(mute_dict[mute])
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), zone2_mute.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
        sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
        sUDP.close()



    def select_navigation(self, hass, device_id, navigation):
        navigation_dict = {'up': 'up', 'down': 'down', 'left': 'left', 'right': 'right',
                           'enter': 'enter', 'menu_on': 'menu'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), navigation_dict[navigation].encode('utf-8'))
                message = message.replace('commandValue'.encode('utf-8'), '0'.encode('utf-8'))
                sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                sUDP.close()


    def set_volume_level(self, hass, device_id, volume, zone):
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                device_ip = items[device_id]['ip']
                #Range of volume on device is from +11--100 to -96--0, we consider +11 to -89
                volume_to_send = int(volume)-89
                if zone is None:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), 'set_volume'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), str(volume_to_send).encode('utf-8'))
                elif zone == 2:
                    message = MESSAGE_COMMAND[0].replace('commandName'.encode('utf-8'), 'zone2_set_volume'.encode('utf-8'))
                    message = message.replace('commandValue'.encode('utf-8'), str(volume_to_send).encode('utf-8'))
                sUDP = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sUDP.sendto(message, (device_ip, MESSAGE_COMMAND[2]))
                sUDP.close()





