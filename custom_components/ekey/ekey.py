import time, math, socket, struct, threading, select
import logging
import socket
import binascii
import codecs


DOMAIN = 'ekey'
_LOGGER = logging.getLogger(__name__)

def setup_platform(hass, config, add_devices, discovery_info=None):

    global devices, sock1, class_obj,device_unit_dict,fingerID,userID,user_ID,finger_ID

    ekey_configs = dict(config)
    print("\n\nEkey Config \n\n")
    print(ekey_configs)
    class_obj = ekey_class()
    devices = ekey_configs['devices']
    print("\n\ndevices\n\n")
    print(devices)
    device_unit_dict = {}

    for user in devices:
        print(user)
        # userID = user['userID']
        # fingerID=user['fingerID']
        # device_unit_dict[userID] = fingerID
        # print("userID of ekey:",userID)
        # print("fingerID of ekey:",fingerID)

        user_ID=user['userID']
        finger_ID=user['fingerID']
        print("\n\nuser id and finger id\n\n")
        print(user_ID, finger_ID)
        print(type(user_ID),type(finger_ID))
        # print("user_ID and finger_ID" , user_ID , finger_ID)
        device_unit_dict.update({user_ID:finger_ID})
        print("device_unit_dict" , device_unit_dict)


    sock1 = class_obj.create_connection(ekey_configs['ip'], ekey_configs['port'])

    class_obj.update(hass, sock1)

class ekey_class():

    def create_connection(self, ip, port):
        try:
            print("###### Connection is created ######")
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            print("Ip and port for device ekey:" ,ip ,port)
            sock.bind((ip, port))
            print("connection created")
            return sock

        except Exception as error:
            print(error)
            print('Failed to create connection. Error message: ')

    def serialID(self,serial):
        if isinstance(serial, int):
            serial = str(serial)
        elif isinstance(serial, str):
            serial = serial
        week = serial[6:8]
        year = serial[8:10]
        seq = serial[10:]
        cal = int(year) * 53 + int(week)
        cal = cal * 65536
        cal = cal + int(seq)
        cal = cal + 1879048192
        cal = hex(cal)
        device_serial = cal[2:]
        return device_serial

#update function

    def update(self, hass,sock):

        while True:

            print("listening the feedback from device ")
            print("object value" ,  sock)
            data= sock.recv(4096)

            print("data in its raw")
            print(data)
            # print(type(data))
            # dev_code = int(data[0].encode('hex'), 16)
            # print("dev_code: ", dev_code)
            # print(type(data[0]))

            output_data=binascii.hexlify(bytearray(data))
            temp = binascii.hexlify(data)
            print(temp)
            print(type(temp))
            print("data in binascii hexlify")
            print(output_data)
            print(type(output_data))
            decoded_data=output_data.decode()
            print("=======:",decoded_data)
            print(type(decoded_data))



            #Extracting device id
            parse=decoded_data[16:24]
            serial_ekey =parse[6]+parse[7]+parse[4]+parse[5]+parse[2]+parse[3]+parse[0]+parse[1]


            serial = 80207911180576
            obj = ekey_class()
            extracted_serial = obj.serialID(serial)
            if extracted_serial == serial_ekey:
                print("Serial Id of Ekey is authenticated")
            else:
                print("Serial Id of Ekey cannot be authenticated")

            another = bytes.fromhex(decoded_data)
            print("gulati")
            print(another)
            print(type(another))
            user_id=decoded_data[57:58]
            print("user_id",user_id)
            validation_value=decoded_data[8:10]
            print("validdation_value",validation_value)
            finger_id=decoded_data[65:66]
            print("finger_id",finger_id)
            serial_number=decoded_data[17:25]
            print(serial_number)
            print("serial_number",serial_number)
            print("User details type" ,type(user_id) ,type(finger_id) ,type(serial_number))
            print("user details are given as",user_id , finger_id, serial_number)
            print(validation_value)
            print(type(validation_value))
            if validation_value=='88':
                print(validation_value)
                print("User is Authenticated is valid")

                if user_id in device_unit_dict.keys():
                    print("user_id inside if loop:" , user_id)
                    user_finger = device_unit_dict[user_ID]
                    print("user finger_id inside if loop:" ,user_finger)
                    if finger_id in user_finger:
                        print("finger id {} is valid for user {} here".format(finger_id, user_id))
                        # print("finger_ID inside second if loop",finger_ID)
                        # user_finger = device_unit_dict[finger_ID]
                        # print("finger_id inside if loop:" , user_finger)
                        # print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                        # hass.services.call(,,,False)
                    else:
                        print("finger {} not ok for user {}".format(finger_id,user_id))
                else:
                    print("user {} not ok".format(user_id))

            else:
                print("User Authentication is Invalid")
