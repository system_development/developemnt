#!/usr/bin/env python

from homeassistant.components.sensor import serial_pm
from sqlalchemy.sql.functions import session_user
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

_LOGGER = logging.getLogger(__name__)

class benq_projector_class():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'benq_projector'
CERT_PATH=home+'/slave_certificates/'

ATTR_NAME = 'entity_id'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate, device_id, slave_ip, username, password
    _LOGGER.info("\n\n\nBenq Projector Setup Function\n\n\n")
    serial_port = config.get('benq_projector')['serial_port']
    baudrate = config.get('benq_projector')['baudrate']
    device_id = config.get('benq_projector')['device_id']
    entity_id = ('benq_projector.' + str(device_id))
    slave_ip = config.get('benq_projector')['slave_ip']
    slave_port = config.get('benq_projector')['slave_port']
    slave_user = config.get('benq_projector')['slave_username']
    slave_pwd = config.get('benq_projector')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    print(slave_cert_path)

#the command which runs at the shell is : from benq_projector import benq_projector_class; benq_projector_class( '/dev/ttyS0' ).get_power_status()
#Command which runs at the shell is: python3 -c 'from benq_projector import benq_projector_class; benq_projector_class( "/dev/ttyS0" , "115200" , b'\r*pow=?#\r' ).get_power_status()'


    def turn_on(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "\r*pow=on#\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        if 'POW=ON' in response.text:
            _LOGGER.info("Response Setting to ON")
            hass.states.set(entity_id, 'on')


    def turn_off(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "\r*pow=off#\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)

        if 'POW=OFF' in response.text:
            _LOGGER.info("Response Setting to OFF")
            hass.states.set(entity_id, 'off')

    def get_power_status(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "\r*pow=?#\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)

        if response.text:
            _LOGGER.info("Response Setting ")
            hass.states.set(entity_id, 'on', {"operation": "get_power_state"})


    def select_source(call):
        name = call.data.get(ATTR_NAME, entity_id)
        source_dict = {"hdmi1":"\r*sour=hdmi#\r", "hdmi2":"\r*sour=hdmi2#\r"}
        # source_dict_response = {"hdmi1":"SOUR=HDMI#","hdmi2":"SOUR=HDMI2#"}
        source = call.data.get('source')
        control_command = source_dict[source]
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        attributes_dict = {}
        if "HDMI#" in response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['operation'] = source
                else:
                    attributes_dict['operation'] = source
            except Exception as error:
                print(error)
                attributes_dict['operation'] = source
            hass.states.set(entity_id, 'on', attributes_dict)
        elif "HDMI2#" in response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['operation'] = source
                else:
                    attributes_dict['operation'] = source
            except Exception as error:
                print(error)
                attributes_dict['operation'] = source
            hass.states.set(entity_id, 'on', attributes_dict)




    # def select_source_HDMI1(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "\r*sour=hdmi#\r"
    #     data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
    #     data = urllib.parse.urlencode(data)
    #     url = 'http://' + str(slave_ip) + ':5000/projector?' + data
    #     _LOGGER.info(url)
    #     response = requests.get(url)
    #     _LOGGER.info(response.text)
    #     if 'SOUR=HDMI#' in response.text:
    #         _LOGGER.info("Response Setting to HDMI1")
    #         hass.states.set(entity_id, 'on', {"operation": "hdmi1"})
    #
    #
    # def select_source_HDMI2(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "\r*sour=hdmi2#\r"
    #     data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
    #     data = urllib.parse.urlencode(data)
    #     url = 'http://' + str(slave_ip) + ':5000/projector?' + data
    #     _LOGGER.info(url)
    #     response = requests.get(url)
    #     _LOGGER.info(response.text)
    #     if 'SOUR=HDMI2#' in response.text:
    #         _LOGGER.info("Response Setting to HDMI2")
    #         hass.states.set(entity_id, 'on', {"operation": "hdmi2"})

    def get_source_status(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "\r*sour=?#\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        if response.text:
            _LOGGER.info("Response Setting to be decided")
            hass.states.set(entity_id, 'on', {"operation": "get_source_set"})
    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)
    hass.services.register(DOMAIN, 'get_power_status', get_power_status)
    hass.services.register(DOMAIN, 'select_source', select_source)
    hass.services.register(DOMAIN, 'get_source_status', get_source_status)

    # Return boolean to indicate that initialization was successfully.
    return True
