import time, math, socket, struct, threading, select
import logging
import socket
import asyncio
import voluptuous as vol
from homeassistant.helpers.entity import Entity
import homeassistant.helpers.config_validation as cv

from homeassistant.core import callback
import threading

DOMAIN = 'custom_light_cover'
_LOGGER = logging.getLogger(__name__)

def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up the Marantz platform."""

    global devices, event_session_obj, command_session_obj, device_bus_dict , button_device_dict , press_device_dict
    device_bus_dict={}
    button_device_dict={}
    press_device_dict={}
    bticino_configs = dict(config)
    class_obj =  bticino_class()
    devices = bticino_configs['devices']
    for dev_bus in devices:
        print(dev_bus)
        if 'bus_val' in dev_bus.keys():
            bus_device_id = dev_bus['device_id']
            bus_value = dev_bus['bus_val']
            print(bus_value , bus_device_id)
            device_bus_dict.update({bus_device_id:bus_value})
            print("dictionary=====" , device_bus_dict)
        if 'button_no' in dev_bus.keys():
            button_device_id = dev_bus['device_id']
            button_no = dev_bus['button_no']
            print("button number==============================" , button_no)
            print("button no" , button_no , button_device_id)
            button_device_dict.update({button_device_id:button_no})
            print("dictionary=====" , device_bus_dict)
        if 'press' in dev_bus.keys():

            press_device_id = dev_bus['device_id']
            press = dev_bus['press']
            print("press========================================" , press)
            print("press",press , press_device_id)
            press_device_dict.update({press_device_id:press})
            print("dictionary=====" , device_bus_dict)


        event_session_obj = class_obj.create_connection(bticino_configs['ip'], bticino_configs['port'])
        command_session_obj = class_obj.create_connection(bticino_configs['ip'], bticino_configs['port'])


    # Registering services
    def turn_off(service):

        entity_id = service.data.get('entity_id')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        if device_id in device_bus_dict.keys():
            bus_val = device_bus_dict[device_id]
            print("==================",bus_val)

            if bus_val :
                class_obj.turn_off(device_id , bus_val)
            else:
                print("inside else" , bus_val)
                class_obj.turn_off(device_id ,bus_val = None )
        else:
            class_obj.turn_off(device_id , bus_val = None)

    def turn_on(service):
        entity_id = service.data.get('entity_id')
        brightness = service.data.get('brightness')
        print("brightness" , brightness)
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]

        #Only enter here when bus_val is present
        if device_id in device_bus_dict.keys():
            bus_val = device_bus_dict[device_id]
            print("bussss" , bus_val)

            if (brightness in range(2,10)) and bus_val :
                print("entering in brightness and bus")
                class_obj.turn_on(device_id , bus_val , brightness)
            elif bus_val  and (brightness == None):
                print("entering in bus, not brightness")
                class_obj.turn_on(device_id ,bus_val)
            elif (brightness in range(2,10)) and (bus_val == None):
                print("entering brigntness and no bus")
                print("printing attributes" , brightness , bus_val)
                class_obj.turn_on(device_id  , bus_val = None)
        #When no bus_value is present
        elif brightness is not None:
            print("bright" , brightness)
            class_obj.turn_on(device_id ,None , brightness )
        else:
            class_obj.turn_on(device_id , bus_val= None)
            # print("error")

    def select_scene(service):
        entity_id = service.data.get('entity_id')

        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        print("printing outside if condition of select scene")
        #Only enter when press and button is present
        if device_id in press_device_dict.keys() and device_id in device_bus_dict.keys():
            print("device_id of devices for scene " , device_id)
            press = press_device_dict[device_id]
            button_no = button_device_dict[device_id]
            bus_value = device_bus_dict[device_id]
            print("printing all parameter of device list ========" , press , button_no , bus_value)
            ########multiple value ###############
            if button_no and press and bus_value:
                print("bus value inside === " , bus_value)
                class_obj.select_scene(device_id , button_no, press  , bus_value)
            else:
                print("nothing found")

        elif device_id in press_device_dict.keys():
            print("device_id of devices for scene " , device_id)
            press = press_device_dict[device_id]
            button_no = button_device_dict[device_id]
            if button_no and press:
                class_obj.select_scene(device_id , button_no,press , None )
            else:
                print("nothing found")
        # Only enter when press and button and bus value  is present

        # Only for button and bus value
        elif device_id in button_device_dict.keys() and device_id in device_bus_dict.keys():
            print("device_id of devices for scene " , device_id)
            button_no = button_device_dict[device_id]
            bus_value = device_bus_dict[device_id]
            if button_no and bus_value:
                class_obj.select_scene(device_id , button_no, None  , bus_value)
            else:
                print("nothing found")

        #Only enter when only button is present
        elif device_id in button_device_dict.keys():
            button_no = button_device_dict[device_id]
            print("button no in select scene" , button_no)
            class_obj.select_scene(device_id , button_no ,None , None)

    def open_cover(service):
        entity_id = service.data.get('entity_id')
        # bus = service.data.get('bus')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        if bus_value:
            class_obj.open_cover(device_id , bus_value)
        else:
            class_obj.open_cover((device_id ))


    def close_cover(service):
        entity_id = service.data.get('entity_id')
        # bus = service.data.get('bus')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        if bus_value:
            class_obj.close_cover(device_id , bus_value)
        else:
            class_obj.close_cover((device_id ))


    def stop_cover(service):
        entity_id = service.data.get('entity_id')
        # bus = service.data.get('bus')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        if bus_value:
            class_obj.stop_cover(device_id , bus_value)
        else:
            class_obj.stop_cover((device_id ))



    service_dict = {
                        'turn_on': turn_on,
                        'turn_off': turn_off,
                        'open_cover' : open_cover,
                        'close_cover' : close_cover,
                        'stop_cover' : stop_cover,
                        'select_scene': select_scene
                    }
    for service in service_dict:
        hass.services.async_register(DOMAIN, service, service_dict[service])

    # Updating feedback
    print("**************\n\n")
    print(bticino_configs['devices'])
    update_thread = threading.Thread(target=class_obj.update, args=(hass, bticino_configs['devices'],))
    update_thread.start()

class bticino_class():

    def create_connection(self, ip, port):
        try:
            telnetObj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            telnetObj.connect((ip, port))
            print(telnetObj)
            return telnetObj

        except Exception as error:
            _LOGGER.error('Failed to create connection. Error message: ' + str(error))


    def transmit(self, command):
        try:
            print("command sent")
            command_session_obj.send(command.encode())
            print(command.encode())
        except Exception as error:
            _LOGGER.error('Failed to transmit command. Error message: ' + str(error))


    def update(self, hass, devices):

        #Start event session
        self.initial_time = int(round(time.time()))
        event_session_command = "*99*1##"
        print('start event session')
        event_session_obj.send(event_session_command.encode())
        print(event_session_command.encode())

        # start command session
        print('start command session')
        self.transmit("*99*9##")

        while 1:
            self.current_time = int(round(time.time()))
            time_diff = self.current_time - self.initial_time

            if time_diff >= 5:
                # re-start command session
                print('re-start command session')
                self.transmit("*99*9##")
                self.initial_time = self.current_time
                time.sleep(.05)
            try:
                response_realtime = event_session_obj.recv(20)
                if response_realtime:
                    print('===response received===')
                    print(response_realtime)
                
                    for bticino_config in devices:
                        #print(bticino_config)
                        temp_dict = dict(bticino_config)
                        #print(bticino_config)

                        APL_val= int(str(bticino_config['A']) + str(bticino_config['PL']))


                        if ('*1*1*'+str(APL_val)).encode() in response_realtime:
                            #if A
                            hass.states.set(DOMAIN + '.'+temp_dict['device_id'] , 'on', {})
                        if ('*1*0*'+str(APL_val)).encode() in response_realtime:
                            hass.states.set(DOMAIN + '.' + temp_dict['device_id'], 'off', {})

            except Exception as error:
                    _LOGGER.error('Failed to receive real time feedback'+str(error))

    def turn_on(self,  device_id,bus_val,*brightness ):
            # bus_value=kwargs.get('bus_value')
            print("printing bus value======", bus_val)
            for device in devices:

                if device_id == device['device_id']:

                    APL_val = str(device['A'])+str(device['PL'])

                    if brightness and bus_val:

                        self.transmit("*1*"+str(*brightness)+"*"+APL_val+"#4#0"+str(bus_val)+"##")
                    elif brightness:

                        self.transmit("*1*"+str(*brightness)+"*"+APL_val+"##")
                    elif bus_val:

                        self.transmit("*1*1*"+APL_val+"#4#0"+str(bus_val)+"##")
                    else:

                        self.transmit("*1*1*"+APL_val+"##")

    def turn_off(self, device_id , bus_val):

        print("checking bus_val inside turn_off" , bus_val)
        # bus_value=kwargs.get('bus_value')
        for device in devices:
            if device_id == device['device_id']:
                APL_val = str(device['A'])+str(device['PL'])

                if bus_val :
                    self.transmit("*1*0*"+APL_val+"#4#0"+str(bus_val)+"##")
                else:
                    self.transmit("*1*0*"+APL_val+"##")

    def open_cover(self, device_id,bus_val):
        for device in devices:
            if device_id == device['device_id']:
                APL_val = str(device['A'])+str(device['PL'])

                if bus_val :
                    self.transmit("*2*1*"+APL_val+"#4#0"+str(bus_val)+"##")
                else:
                    self.transmit("*2*1*"+APL_val+"##")

    def close_cover(self, device_id,bus_val):
        for device in devices:
            if device_id == device['device_id']:
                APL_val = str(device['A'])+str(device['PL'])

                if bus_val :
                    self.transmit("*2*2*"+APL_val+"#4#0"+str(bus_val)+"##")
                else:
                    self.transmit("*2*2*"+APL_val+"##")

    def stop_cover(self, device_id,bus_val):
        for device in devices:
            if device_id == device['device_id']:
                APL_val = str(device['A'])+str(device['PL'])

                if bus_val :
                    self.transmit("*2*0*"+APL_val+"#4#0"+str(bus_val)+"##")
                else:
                    self.transmit("*2*0*"+APL_val+"##")

    def select_scene(self, device_id,button_no,press , bus_value):
        print("getting button number and press number" , button_no, press )
        for device in devices:
            if device_id == device['device_id']:
                APL_val = str(device['A'])+str(device['PL'])
                print("bus value" , bus_value)

                if  press and  button_no and bus_value:
                    print("bus value==" , bus_value)
                    self.transmit("*15*"+str(button_no)+"#"+str(press)+"*"+str(APL_val)+"#4#0"+str(bus_value)+"##")
                elif press and button_no:
                    self.transmit("*15*"+str(button_no)+"#"+str(press)+"*"+str(APL_val)+"##")
                elif button_no and  bus_value:
                    self.transmit("*15*"+str(button_no)+"*"+str(APL_val)+"#4#0"+str(bus_value)+"##")
                else:
                    self.transmit("*15*"+str(button_no)+"*"+str(APL_val)+"##")

