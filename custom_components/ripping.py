#!/usr/bin/env python

import paramiko
from homeassistant.components.sensor import serial_pm
from sqlalchemy.sql.functions import session_user


class ripping_class():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd

#Based on the room parameter(configuration.yaml), we have to find out which sub-okas it is.Se configuration.yaml
        ip = self.slave_ip
        port= self.slave_port
        username= self.slave_user
        password= self.slave_pwd
        try:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(ip,port,username,password)
            print("Trying to establish connection through ssh")

        except Exception as error:
            print('Connection Failed')
            print(error)
            return

    def _command_handler(self, command_string):
        print("Invoking command handler")
        print("Command string is below")
        print(command_string)
        stdin,stdout,stderr = self.ssh.exec_command(command_string)
        print("Command sent through command handler")
        print(stdout.readline())
        for line in iter(stdout.readline, ""):
            print(line, end="")
            print('finished.')
        return stdout



# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'ripping'

ATTR_NAME = 'entity_id'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate, room, slave_ip, username, password
    baudrate = config.get('ripping')['baudrate']
    device_id = config.get('ripping')['device_id']
    entity_id = ('ripping.' + str(device_id))
    slave_ip = config.get('ripping')['slave_ip']
    slave_port = config.get('ripping')['slave_port']
    slave_user = config.get('ripping')['slave_username']
    slave_pwd = config.get('ripping')['slave_password']

#the command which runs at the shell is : from benq_projector import benq_projector_class; benq_projector_class( '/dev/ttyS0' ).get_power_status()
#Command which runs at the shell is: python3 -c 'from benq_projector import benq_projector_class; benq_projector_class( "/dev/ttyS0" , "115200" , b'\r*pow=?#\r' ).get_power_status()'


    def make_mkv(call):
        print("Inside makemkv function")
        paswd= 'oct@2018'
        name = call.data.get(ATTR_NAME, entity_id)
        ripping_classObj = ripping_class(slave_ip, slave_port, slave_user, slave_pwd)
        # control_command = "\r*pow=on#\r"
        command_string = 'echo {}|sudo -S makemkvcon --minlength=3600 -r --decrypt --directio=true mkv disc:0 all /mnt/nas; eject -r'.format(paswd)
        print("sending command")
        response_list = ripping_classObj._command_handler(command_string)
        if response_list:
            print("Inside response list")
            hass.states.set(entity_id,'on')


    hass.services.register(DOMAIN, 'make_mkv', make_mkv)


    # Return boolean to indicate that initialization was successfully.
    return True

