#!/usr/bin/env python

from homeassistant.components.sensor import serial_pm
from sqlalchemy.sql.functions import session_user
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

_LOGGER = logging.getLogger(__name__)


class optoma_projector():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'optoma_projector'

ATTR_NAME = 'entity_id'
#entity_id = 'optoma_projector1'
CERT_PATH=home+'/slave_certificates/'
def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('optoma_projector')['serial_port']
    baudrate = config.get('optoma_projector')['baudrate']
    device_id = config.get('optoma_projector')['device_id']
    entity_id = ('optoma_projector.' + str(device_id))
    slave_ip = config.get('optoma_projector')['slave_ip']
    slave_port = config.get('optoma_projector')['slave_port']
    slave_user = config.get('optoma_projector')['slave_username']
    slave_pwd = config.get('optoma_projector')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    _LOGGER.info(slave_cert_path)

    def turn_on(call):
        get_power_status(call)
        _LOGGER.info("get power status is called on turn off")
        entity_id = call.data.get('entity_id')
        control_command = "~0000 1\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url, verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'INFO1' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

    def turn_off(call):
        get_power_status(call)
        _LOGGER.info("get power status is called on turn off")
        entity_id = call.data.get('entity_id')
        control_command = "~0000 2\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'INFO2' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

    def select_source(call):
        entity_id = call.data.get('entity_id')
        source = call.data.get('source')
        source_dict = {"hdmi1":"~0039 1\r\n","hdmi2":"~0039 7\r\n","vga":"~0039 5\r\n"}
        control_command = source_dict[source]
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        #Check specific source results to map with exact response
        if str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['operation'] = source
                else:
                    attributes_dict['operation'] = source
            except Exception as error:
                print(error)
                attributes_dict['operation'] = source
            hass.states.set(entity_id,'on',attributes_dict)

    def get_source_status(call):
        entity_id = call.data.get('entity_id')
        control_command = "~00121 1\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        if str(response_to_check):
            hass.states.set(entity_id,'on')

    def get_power_status(call):
        _LOGGER.info("Get power status is called")
        entity_id = call.data.get('entity_id')
        control_command = "~00124 1\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        response_to_check = response.text
        _LOGGER.info(response.text)
        
        attributes_dict = {}
        if 'OK0' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

        elif 'OK1' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

    def getAspectRatio(call):
        entity_id = call.data.get('entity_id')
        control_command = "~00127 1\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        _LOGGER.info(control_command)
        response_to_check = response.text

        if str(response_to_check):
            hass.states.set(entity_id,'on')

    def status(call):
        entity_id = call.data.get('entity_id')
        control_command = "~00124 1\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/projector?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        response_to_check = response.text
        _LOGGER.info(response.text)

        if str(response_to_check):
            hass.states.set(entity_id,'on')

    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)
    hass.services.register(DOMAIN,'select_source',select_source)
    hass.services.register(DOMAIN, 'get_source_status', get_source_status)
    hass.services.register(DOMAIN, 'get_power_status', get_power_status)
    hass.services.register(DOMAIN, 'getAspectRatio', getAspectRatio)
    hass.services.register(DOMAIN, 'status', status)

    # Return boolean to indicate that initialization was successfully.
    return True
