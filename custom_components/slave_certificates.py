import requests
import json
import time
from pathlib import Path
home = str(Path.home())
#import OpenSSL.crypto
import M2Crypto
import ssl
# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'slave_certificates'

def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""
    print("\n\n Slave Certificates Setupn\n\n")

    with open(home + '/si_data.json') as data_file:
        data = json.loads(data_file.read())
    si_data = dict(data)

    slave_boxes = {}
    okas_boxes = si_data['okasBox']


    for box in okas_boxes:
        if box['okasboxtype'] == 'Master Controller':
            master_box = box['okasboxip']
        else:
            slave_boxes[box['okasbox_id']]  = box['okasboxip']

    box_keys,box_values = list(slave_boxes.keys()),list(slave_boxes.values())
    print("\n\n Slave Certificates : Okas Boxes and Master\n\n")
    print(slave_boxes)



    def create_copy(call):
        print("\n\n Slave Certificates : Create Copy Called\n\n")
        print(okas_boxes)
        #Initializing the variables as None
        boxes = call.data.get("box")
        if boxes == 'all':
            print("\n\n\nAll conditions slave certificates\n\n\n")
            for items in box_values:
                slave_ip_extract = items.split('.')[3]
                cert = ssl.get_server_certificate((items, 443))
                x509 = M2Crypto.X509.load_cert_string(cert)

                cert_name = 'ca_'+str(slave_ip_extract)+'.crt'
                x509.save(home+"/slave_certificates/"+cert_name)
            return "Cert File Dumped"
        else:
            print("\n\n\nElse condition of slave certificates\n\n\n")
            for items in box_keys:
                if boxes in items:
                    slave_ip = slave_boxes[boxes]
                    slave_ip_extract = slave_ip.split('.')[3]
                    cert = ssl.get_server_certificate((items, 443))
                    x509 = M2Crypto.X509.load_cert_string(cert)

                    cert_name = 'ca_' + str(slave_ip_extract) + '.crt'
                    x509.save(home+"/slave_certificates/"+cert_name)

            return "Single cert file dumped"


    hass.services.register(DOMAIN, 'create_copy', create_copy)

    return True





