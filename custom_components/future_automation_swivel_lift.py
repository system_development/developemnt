#!/usr/bin/env python


import time
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())


_LOGGER = logging.getLogger(__name__)


class futureAutomationRemote():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self._timeout = 0.5
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'future_automation_swivel_lift'
ATTR_NAME = 'entity_id'
CERT_PATH=home+'/slave_certificates/'
def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('future_automation_swivel_lift')['serial_port']
    baudrate = config.get('future_automation_swivel_lift')['baudrate']
    device_id = config.get('future_automation_swivel_lift')['device_id']
    entity_id = ('future_automation_swivel_lift.' + str(device_id))
    slave_ip = config.get('future_automation_swivel_lift')['slave_ip']
    slave_port = config.get('future_automation_swivel_lift')['slave_port']
    slave_user = config.get('future_automation_swivel_lift')['slave_username']
    slave_pwd = config.get('future_automation_swivel_lift')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    print(slave_cert_path)


    def deviceIn(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_in\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if 'fa:ack' in str(response_to_check):
            hass.states.set(entity_id , "in")

    def deviceOut(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_out\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if 'fa:ack' in str(response_to_check):
            hass.states.set(entity_id , "out")

    def deviceOutRightLimit(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_right\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if 'fa:ack' in str(response_to_check):
            hass.states.set(entity_id ,"out" ,{"operation":"right"})

    def deviceOutLeftLimit(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_left\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if 'fa:ack' in str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"left"})

    def deviceOutHome(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_home\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check =  response.text
        if 'fa:ack' in str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"home"})

    def presetPosition(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_preset\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"preset"})

    def aPosition(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_a\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"position_A"})

    def bPosition(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_b\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"position_B"})

    def deviceStop(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_stop\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/swivellift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"stop"})

    hass.services.register(DOMAIN, 'deviceIn', deviceIn)
    hass.services.register(DOMAIN, 'deviceOut', deviceOut)
    hass.services.register(DOMAIN, 'deviceOutRightLimit', deviceOutRightLimit)
    hass.services.register(DOMAIN, 'deviceOutLeftLimit', deviceOutLeftLimit)
    hass.services.register(DOMAIN, 'deviceOutHome', deviceOutHome)
    hass.services.register(DOMAIN, 'presetPosition', presetPosition)
    hass.services.register(DOMAIN, 'aPosition', aPosition)
    hass.services.register(DOMAIN, 'bPosition', bPosition)
    hass.services.register(DOMAIN, 'deviceStop', deviceStop)

    return True


