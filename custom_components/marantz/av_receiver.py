import time, math, socket, struct, time, threading, select
import telnetlib
import logging
import telnetlib
import asyncio

import voluptuous as vol

from homeassistant.helpers.entity import Entity

import homeassistant.helpers.config_validation as cv

_LOGGER = logging.getLogger(__name__)

global devices_objects_list
devices_objects_list = []

def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up the Marantz platform."""
    global ip, port, device_id, class_obj, entity_domain
    entity_domain = 'marantz'
    marantz_configs = dict(config)
    ip = marantz_configs['ip']
    port = marantz_configs['port']
    device_id = marantz_configs['device_id']
    class_obj =  marantz_class()
    class_obj.create_connection(device_id, ip, port)

    # Starting thread for real time feedback
    update_thread = threading.Thread(target=class_obj.update, args=(hass,))
    update_thread.start()

class marantz_class():

    def create_connection(self, device_id, ip, port):

        try:
            telnetObj = telnetlib.Telnet(ip, port, timeout=0.5)
            devices_objects_list.append({device_id: {'ip': ip, 'port': port, 'object': telnetObj}})
            _LOGGER.info(devices_objects_list)

        except Exception as error:
            _LOGGER.error('Failed to create connection. Error message: ' + str(error))

    def transmit(self, hass, command_list, object, device_id):
        try:
            for command in command_list:
                _LOGGER.info("command sent")
                object.write(command)
                _LOGGER.info(command)
                time.sleep(0.5)

        except Exception as error:
            _LOGGER.error('Failed to transmit command. Error message: ' + str(error))
            _LOGGER.info("connection re-establishing")
            time.sleep(1)
            new_device_dict = {}
            for items in devices_objects_list:
                item_key = items.keys()
                if device_id in item_key:
                    ip = items[device_id]['ip']
                    port = items[device_id]['port']
                    telnetObj = telnetlib.Telnet(ip, port, timeout=0.5)
                    new_device_dict.update({'ip': ip, 'port': port, 'object': telnetObj})
                    items[device_id] = new_device_dict
                    for command in command_list:
                        telnetObj.write(command)
                        time.sleep(0.5)
                    self.update(hass)
                    break
            _LOGGER.info(devices_objects_list)

    def update(self, hass):
        while 1:
            for devices in devices_objects_list:
                for key, value in devices.items():
                    device_object = value['object']
                    entity_id = entity_domain + '.' + key
                    response_realtime = device_object.read_until(b'\r', timeout=0.5)

                    if response_realtime:
                        _LOGGER.info(response_realtime)

                        attributes_dict = {}
                        try:
                            attributes = hass.states.get(entity_id).attributes

                            attributes_dict.update(dict(attributes))
                            if b'MU' in response_realtime:
                                feedback_dict = {
                                                    b'MUON\r':'on',
                                                    b'MUOFF\r':'off'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['mute'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'SI' in response_realtime:
                                feedback_dict = {
                                                    b'SIDVD\r':'DVD',
                                                    b'SISAT/CBL\r':'CBL/SAT',
                                                    b'SIMPLAY\r':'Media Player' ,
                                                    b'SIBD\r':'Blu-ray',
                                                    b'SIGAME\r':'Game',
                                                    b'SICD\r':'CD',
                                                    b'SIAUX1\r':'AUX1',
                                                    b'SIAUX2\r':'AUX2',
                                                    b'SITV\r':'TV Audio'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['source'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'PW' in response_realtime:
                                if b'PWON\r' in response_realtime:
                                    state = 'on'
                                    hass.states.set(entity_id, state, attributes_dict)
                                elif b'PWSTANDBY\r' in response_realtime:
                                    state = 'off'
                                    attributes_dict['source'] = None
                                    attributes_dict['zone2_state'] = 'off'
                                    attributes_dict['zone2_source'] = None
                                    hass.states.set(entity_id, state, attributes_dict)

                            if b'Z' in response_realtime:
                                if b'Z2' in response_realtime:
                                    feedback_dict = {
                                        b'Z2DVD\r': 'DVD',
                                        b'Z2SAT/CBL\r': 'CBL/SAT',
                                        b'Z2MPLAY\r': 'Media Player',
                                        b'Z2BD\r': 'Blu-ray',
                                        b'Z2GAME\r': 'Game',
                                        b'Z2CD\r': 'CD',
                                        b'Z2AUX1\r': 'AUX1',
                                        b'Z2AUX2\r': 'AUX2',
                                        b'Z2TV\r': 'TV Audio'
                                    }
                                    feedback_keys = feedback_dict.keys()
                                    for command in feedback_keys:
                                        if command in response_realtime:
                                            # attributes_dict['zone2'] = {'source': feedback_dict[command],'state':'on'}
                                            attributes_dict['zone2_source'] = feedback_dict[command]
                                            attributes_dict['zone2_state'] = "on"
                                            main_zone_state = hass.states.get(entity_id).state
                                            hass.states.set(entity_id, main_zone_state, attributes_dict)
                                if b'Z2MUON' in response_realtime:
                                    attributes_dict['zone2_mute'] = 'on'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2MUOFF' in response_realtime:
                                    attributes_dict['zone2_mute'] = 'off'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)


                                if b'Z2ON\r' in response_realtime:
                                    attributes_dict['zone2_state'] = 'on'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2OFF\r' in response_realtime:
                                    attributes_dict['zone2_state'] = 'off'
                                    attributes_dict['zone2_source'] = None
                                   # hass.states.set(entity_id, "off", attributes_dict)
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z20' in response_realtime or b'Z21' in response_realtime or b'Z22' in response_realtime or b'Z23' in response_realtime or b'Z24' in response_realtime or b'Z25' in response_realtime or b'Z26' in response_realtime or b'Z27' in response_realtime or b'Z28' in response_realtime or b'Z29' in response_realtime and b'Z2MAX' not in response_realtime:
                                    volume_extract = response_realtime.decode()
                                    volume_digit = ''.join(x for x in volume_extract if x.isdigit())[1:]
                                    if len(volume_digit) == 1:
                                        volume = volume_digit
                                    elif len(volume_digit) == 2:
                                        volume = volume_digit
                                    elif len(volume_digit) == 3:
                                        volume = int(volume_digit) / 10
                                    attributes_dict['zone2_volume'] = str(volume)
                                    attributes_dict['zone2_mute'] = 'off'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)
                                if b'ZMON\r' in response_realtime:
                                    # attributes_dict['zone_main'] = 'on'
                                    hass.states.set(entity_id, 'on', attributes_dict)

                                if b'ZMOFF\r' in response_realtime:
                                    state = 'off'
                                    # attributes_dict['zone_main'] = 'off'
                                    attributes_dict['source'] = None
                                    hass.states.set(entity_id, state, attributes_dict)



                            if b'MN' in response_realtime:
                                feedback_dict = {
                                                    b'MNCUP\r':'up',
                                                    b'MNCDOWN\r':'down',
                                                    b'MNCLT\r':'left' ,
                                                    b'MNCRT\r':'right',
                                                    b'MNENT\r':'enter',
                                                    b'MNMEN ON\r':'menu_on',
                                                    b'MNMEN OFF\r':'menu_off'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['navigation'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'MS' in response_realtime:
                                feedback_dict = {
                                                     b'MSMOVIE\r':'surround_movie',
                                                     b'MSMUSIC\r':'surround_music',
                                                     b'MSGAME\r':'surround_game' ,
                                                     b'MSAUTO\r':'auto',
                                                     b'MSSTEREO\r':'stereo',
                                                     b'MSDOLBY DIGITAL\r':'dolby_digi',
                                                     b'MSMCH STEREO\r':'multi_ch_stereo',
                                                     b'MSDTS SURROUND\r':'dts_surround',
                                                     b'MSDIRECT\r':'direct',
                                                     b'MSPURE DIRECT\r':'pure_direct'
                                                 }

                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['mode'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'MV' in response_realtime and b'MVMAX' not in response_realtime:
                                volume_extract = response_realtime.decode()
                                volume_digit = ''.join(x for x in volume_extract if x.isdigit())
                                if len(volume_digit) == 1:
                                    volume = volume_digit
                                elif len(volume_digit) == 2:
                                    volume = volume_digit
                                elif len(volume_digit) == 3:
                                    volume = int(volume_digit) / 10
                                attributes_dict['volume'] = str(volume)
                                attributes_dict['mute'] = 'off'
                                hass.states.set(entity_id, "on", attributes_dict)

                        except Exception as error:
                            _LOGGER.error(error)
                            if b'MU' in response_realtime:
                                feedback_dict = {
                                                    b'MUON\r':'on',
                                                    b'MUOFF\r':'off'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['mute'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'SI' in response_realtime:
                                feedback_dict = {
                                                    b'SIDVD\r':'DVD',
                                                    b'SISAT/CBL\r':'CBL/SAT',
                                                    b'SIMPLAY\r':'Media Player' ,
                                                    b'SIBD\r':'Blu-ray',
                                                    b'SIGAME\r':'Game',
                                                    b'SICD\r':'CD',
                                                    b'SIAUX1\r':'AUX1',
                                                    b'SIAUX2\r':'AUX2',
                                                    b'SITV\r':'TV Audio'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['source'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'PW' in response_realtime:
                                if b'PWON\r' in response_realtime:
                                    state = 'on'
                                    hass.states.set(entity_id, state, attributes_dict)
                                elif b'PWSTANDBY\r' in response_realtime:
                                    state = 'off'
                                    attributes_dict['source'] = None
                                    attributes_dict['zone2_state'] = 'off'
                                    attributes_dict['zone2_source'] = None
                                    hass.states.set(entity_id, state, attributes_dict)

                            if b'Z' in response_realtime:
                                if b'Z2' in response_realtime:
                                    feedback_dict = {
                                        b'Z2DVD\r': 'DVD',
                                        b'Z2SAT/CBL\r': 'CBL/SAT',
                                        b'Z2MPLAY\r': 'Media Player',
                                        b'Z2BD\r': 'Blu-ray',
                                        b'Z2GAME\r': 'Game',
                                        b'Z2CD\r': 'CD',
                                        b'Z2AUX1\r': 'AUX1',
                                        b'Z2AUX2\r': 'AUX2',
                                        b'Z2TV\r': 'TV Audio'
                                    }
                                    feedback_keys = feedback_dict.keys()
                                    for command in feedback_keys:
                                        if command in response_realtime:
                                            # attributes_dict['zone2'] = {'source': feedback_dict[command],'state':'on'}
                                            attributes_dict['zone2_source'] = feedback_dict[command]
                                            attributes_dict['zone2_state'] = "on"
                                            main_zone_state = hass.states.get(entity_id).state
                                            hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2MUON' in response_realtime:
                                    attributes_dict['zone2_mute'] = 'on'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2MUOFF' in response_realtime:
                                    attributes_dict['zone2_mute'] = 'off'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2ON\r' in response_realtime:
                                    attributes_dict['zone2_state'] = 'on'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z2OFF\r' in response_realtime:
                                    attributes_dict['zone2_state'] = 'off'
                                    attributes_dict['zone2_source'] = None
                                    # hass.states.set(entity_id, "off", attributes_dict)
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'Z21' in response_realtime or b'Z21' in response_realtime or b'Z22' in response_realtime or b'Z23' in response_realtime or b'Z24' in response_realtime or b'Z25' in response_realtime or b'Z26' in response_realtime or b'Z27' in response_realtime or b'Z28' in response_realtime or b'Z29' in response_realtime and b'Z2MAX' not in response_realtime:
                                    volume_extract = response_realtime.decode()
                                    volume_digit = ''.join(x for x in volume_extract if x.isdigit())[1:]
                                    if len(volume_digit) == 1:
                                        volume = volume_digit
                                    elif len(volume_digit) == 2:
                                        volume = volume_digit
                                    elif len(volume_digit) == 3:
                                        volume = int(volume_digit) / 10
                                    attributes_dict['zone2_volume'] = str(volume)
                                    attributes_dict['zone2_mute'] = 'off'
                                    main_zone_state = hass.states.get(entity_id).state
                                    hass.states.set(entity_id, main_zone_state, attributes_dict)

                                if b'ZMON\r' in response_realtime:
                                    # attributes_dict['zone_main'] = 'on'
                                    hass.states.set(entity_id, 'on', attributes_dict)

                                if b'ZMOFF\r' in response_realtime:
                                    state = 'off'
                                    # attributes_dict['zone_main'] = 'off'
                                    attributes_dict['source'] = None
                                    hass.states.set(entity_id, state, attributes_dict)

                            if b'MN' in response_realtime:
                                feedback_dict = {
                                                    b'MNCUP\r':'up',
                                                    b'MNCDOWN\r':'down',
                                                    b'MNCLT\r':'left' ,
                                                    b'MNCRT\r':'right',
                                                    b'MNENT\r':'enter',
                                                    b'MNMEN ON\r':'menu_on',
                                                    b'MNMEN OFF\r':'menu_off'
                                                }
                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['navigation'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'MS' in response_realtime:
                                feedback_dict = {
                                                     b'MSMOVIE\r':'surround_movie',
                                                     b'MSMUSIC\r':'surround_music',
                                                     b'MSGAME\r':'surround_game' ,
                                                     b'MSAUTO\r':'auto',
                                                     b'MSSTEREO\r':'stereo',
                                                     b'MSDOLBY DIGITAL\r':'dolby_digi',
                                                     b'MSMCH STEREO\r':'multi_ch_stereo',
                                                     b'MSDTS SURROUND\r':'dts_surround',
                                                     b'MSDIRECT\r':'direct',
                                                     b'MSPURE DIRECT\r':'pure_direct'
                                                 }

                                feedback_keys = feedback_dict.keys()
                                for command in feedback_keys:
                                    if command in response_realtime:
                                        attributes_dict['mode'] = feedback_dict[command]
                                        hass.states.set(entity_id, "on", attributes_dict)

                            if b'MV' in response_realtime and b'MVMAX' not in response_realtime:
                                volume_extract = response_realtime.decode()
                                volume_digit = ''.join(x for x in volume_extract if x.isdigit())
                                if len(volume_digit) == 1:
                                    volume = volume_digit
                                elif len(volume_digit) == 2:
                                    volume = volume_digit
                                elif len(volume_digit) == 3:
                                    volume = int(volume_digit) / 10
                                attributes_dict['volume'] = str(volume)
                                attributes_dict['mute'] = 'off'
                                hass.states.set(entity_id, "on", attributes_dict)

    def turn_off(self, hass, device_id, zone):
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                if zone is None:
                    class_obj.transmit(hass, [b'PWSTANDBY\r'], object, device_id)
                elif zone == 1:
                    class_obj.transmit(hass, [b'ZMOFF\r'], object, device_id)
                elif zone == 2:
                    class_obj.transmit(hass, [b'Z2OFF\r'], object, device_id)

    def turn_on(self, hass, device_id, zone):
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                if zone is None:
                    class_obj.transmit(hass, [b'ZMON\r'], object, device_id)
                elif zone == 2:
                    class_obj.transmit(hass, [b'Z2ON\r'], object, device_id)


    def select_source(self, hass, device_id, source, zone):
        source_dict = {'CBL/SAT': b'SISAT/CBL\r', 'Media Player': b'SIMPLAY\r', 'Blu-ray': b'SIBD\r', 'DVD': b'SIDVD\r',
                           'Game': b'SIGAME\r', 'CD': b'SICD\r', 'AUX1': b'SIAUX1\r', 'AUX2': b'SIAUX2\r',
                           'TV Audio': b'SITV\r'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                if zone is None:
                    class_obj.transmit(hass, [b'SI?\r', source_dict[source]], object, device_id)
                elif zone == 2:
                    source_dict = {'CBL/SAT': b'Z2SAT/CBL\r', 'Media Player': b'Z2MPLAY\r', 'Blu-ray': b'Z2BD\r',
                                   'DVD': b'Z2DVD\r',
                                   'Game': b'Z2GAME\r', 'CD': b'Z2CD\r', 'AUX1': b'Z2AUX1\r', 'AUX2': b'Z2AUX2\r',
                                   'TV Audio': b'Z2TV\r'}
                    class_obj.transmit(hass, [b'Z2?\r', source_dict[source]], object, device_id)




    def select_mode(self, hass, device_id, mode):
        mode_dict = {'surround_movie': b'MSMOVIE\r', 'surround_music': b'MSMUSIC\r', 'surround_game': b'MSGAME\r',
                     'auto': b'MSAUTO\r', 'stereo': b'MSSTEREO\r', 'dolby_digi': b'MSDOLBY DIGITAL\r',
                     'multi_ch_stereo': b'MSMCH STEREO\r', 'dts_surround': b'MSDTS SURROUND\r', 'direct': b'MSDIRECT\r',
                     'pure_direct': b'MSPURE DIRECT\r'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                class_obj.transmit(hass, [mode_dict[mode]], object, device_id)

    def mute_volume(self, hass, device_id, mute, zone):
        mute_dict = {'on': b'MUON\r', 'off': b'MUOFF\r'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                if zone is None:
                    class_obj.transmit(hass, [mute_dict[mute]], object, device_id)
                elif zone == 2:
                    mute_dict = {'on': b'Z2MUON\r', 'off': b'Z2MUOFF\r'}
                    class_obj.transmit(hass, [mute_dict[mute]], object, device_id)



    def select_navigation(self, hass, device_id, navigation):
        navigation_dict = {'up': b'MNCUP\r', 'down': b'MNCDN\r', 'left': b'MNCLT\r', 'right': b'MNCRT\r',
                           'enter': b'MNENT\r', 'menu_on': b'MNMEN ON\r', 'menu_off': b'MNMEN OFF\r'}
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                class_obj.transmit(hass, [navigation_dict[navigation]], object, device_id)

    def set_volume_level(self, hass, device_id, volume, zone):
        for items in devices_objects_list:
            item_key = items.keys()
            if device_id in item_key:
                object = items[device_id]['object']
                if zone is None:
                    if volume < 10:
                        command = "MV0"+str(volume)+"\r"
                    elif volume > 98:
                        command = "MV98\r"
                    else:
                        command = "MV"+str(volume)+"\r"
                    class_obj.transmit(hass, [command.encode()], object, device_id)
                elif zone == 2:
                    if volume < 10:
                        command = "Z20"+str(volume)+"\r"
                    elif volume > 98:
                        command = "Z298\r"
                    else:
                        command = "Z2"+str(volume)+"\r"
                    class_obj.transmit(hass, [command.encode()], object, device_id)

