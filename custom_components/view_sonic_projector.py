#!/usr/bin/env python

import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

_LOGGER = logging.getLogger(__name__)


class view_sonic_Projector():
    """Class to encapsulate the projector.
    """

    def __init__(self,slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'view_sonic_projector'
CERT_PATH=home+'/slave_certificates/'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('view_sonic_projector')['serial_port']
    baudrate = config.get('view_sonic_projector')['baudrate']
    device_id = config.get('view_sonic_projector')['device_id']
    slave_ip = config.get('view_sonic_projector')['slave_ip']
    slave_port = config.get('view_sonic_projector')['slave_port']
    slave_user = config.get('view_sonic_projector')['slave_username']
    slave_pwd = config.get('view_sonic_projector')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    _LOGGER.info(slave_cert_path)

    #We have used /loewetv endpoint since it takes two iterations to get response.
    def turn_on(call):
        get_power_status(call)
        _LOGGER.info("Get Power Status Completed, Turn On Called ")
        entity_id = call.data.get('entity_id')
        control_command = '\x06\x14\x00\x04\x00\x34\x11\x00\x00\x5D'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x03' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

    #We have used /loewetv endpoint since it takes two iterations to get response.
    def turn_off(call):
        get_power_status(call)
        _LOGGER.info("Get Power Status Completed, Turn Off Called ")
        entity_id = call.data.get('entity_id')
        control_command = '\x06\x14\x00\x04\x00\x34\x11\x01\x00\x5E'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x03' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

    #We have used /loewetv endpoint since it takes two iterations to get response.
    def get_power_status(call):
        _LOGGER.info("Get Power Status is called")
        entity_id = call.data.get('entity_id')
        control_command = '\x07\x14\x00\x05\x00\x34\x00\x00\x11\x00\x5E'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x01\\\\x18'in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

        elif 'x00\\\\x18' in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

    def select_source(call):
        entity_id = call.data.get('entity_id')
        source = call.data.get('source')
        source_dict = {'hdmi1':'\x06\x14\x00\x04\x00\x34\x13\x01\x07\x67','hdmi2':'\x06\x14\x00\x04\x00\x34\x13\x01\x03\x63'}
        control_command = source_dict[source]
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        response_to_check = response.text

        attributes_dict = {}
        if str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['operation'] = source
                else:
                    attributes_dict['operation'] = source
            except Exception as error:
                print(error)
                attributes_dict['operation'] = source
            hass.states.set(entity_id,'on',attributes_dict)

    def get_source_status(call):
        entity_id = call.data.get('entity_id')
        control_command = '\x07\x14\x00\x05\x00\x34\x00\x00\x13\x01\x61'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        if str(response_to_check):
            hass.states.set(entity_id,'on',{"operation":"get_source_set"})

    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)
    hass.services.register(DOMAIN, 'get_power_status', get_power_status)
    hass.services.register(DOMAIN,'select_source',select_source)
    hass.services.register(DOMAIN, 'get_source_status', get_source_status)

    # Return boolean to indicate that initialization was successfully.
    return True

