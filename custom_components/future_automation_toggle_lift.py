#!/usr/bin/env python

import paramiko
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

_LOGGER = logging.getLogger(__name__)


class futureAutomationRemote():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self._timeout = 0.5
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'future_automation_toggle_lift'
ATTR_NAME = 'entity_id'
CERT_PATH=home+'/slave_certificates/'
def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('future_automation_toggle_lift')['serial_port']
    baudrate = config.get('future_automation_toggle_lift')['baudrate']
    device_id = config.get('future_automation_toggle_lift')['device_id']
    entity_id = ('future_automation_toggle_lift.' + str(device_id))
    slave_ip = config.get('future_automation_toggle_lift')['slave_ip']
    slave_port = config.get('future_automation_toggle_lift')['slave_port']
    slave_user = config.get('future_automation_toggle_lift')['slave_username']
    slave_pwd = config.get('future_automation_toggle_lift')['slave_password']
    print(serial_port)
    print(baudrate)
    print(device_id)
    print(entity_id)
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    print(slave_cert_path)


    
    def deviceIn(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_in\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/togglelift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id , "in")

    def deviceOut(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_out\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/togglelift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if 'devisout' in str(response_to_check):
            hass.states.set(entity_id , "out")

    def deviceStop(call):
        name = call.data.get(ATTR_NAME, entity_id)
        control_command = "fa_stop\r"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/togglelift?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)
        response_to_check = response.text
        if str(response_to_check):
            hass.states.set(entity_id ,"out" , {"operation":"stop"})

    hass.services.register(DOMAIN, 'deviceIn', deviceIn)
    hass.services.register(DOMAIN, 'deviceOut', deviceOut)
    hass.services.register(DOMAIN, 'deviceStop', deviceStop)

    return True


