import time, math, socket, struct, threading, select
import logging
import socket


DOMAIN = 'coolmasternet'
_LOGGER = logging.getLogger(__name__)

def setup_platform(hass, config, add_devices, discovery_info=None):

    global devices, event_session_obj, command_session_obj,class_obj,device_unit_dict
    coolmasternet_configs = dict(config)
    class_obj =  coolmasternet_class()
    devices = coolmasternet_configs['devices']
    print("Type Devices")
    print(devices)
    print(type(devices))
    device_unit_dict = {}
    for items in devices:
        unit_id = items['unit_id']
        unit_id_digit = unit_id.split(".")[1]
        device_id = items['device_id']
        print("success")
        print(unit_id)
        print(device_id)
        device_unit_dict[unit_id_digit] = device_id
    print(device_unit_dict)
    event_session_obj = class_obj.create_connection(coolmasternet_configs['ip'], coolmasternet_configs['port'])
    # command_session_obj = class_obj.create_connection(coolmasternet_configs['ip'], coolmasternet_configs['port'])

    # Registering services
    def turn_off(service):
        entity_id = service.data.get('entity_id')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.turn_off(device_id)

    def turn_on(service):
        entity_id = service.data.get('entity_id')
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
        class_obj.turn_on(device_id)

    service_dict = {
                        'turn_on': turn_on,
                        'turn_off': turn_off,
                        'cool':cool,
                        'heat':heat,
                        'tempUP':tempUP,
                        'tempDOWN':tempDOWN,
                        'tempSET':tempSET,
                        'fspeedLOW':fspeedLOW,
                        'fspeedMEDIUM':fspeedMEDIUM,
                        'fspeedLOW':fspeedLOW,
                        'fan':fan
                    }
    for service in service_dict:
        hass.services.async_register(DOMAIN, service, service_dict[service])

    # Updating feedback
    class_obj.update(hass, coolmasternet_configs['devices'])

class coolmasternet_class():

    def create_connection(self, ip, port):
        try:
            telnetObj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            telnetObj.connect((ip, port))
            # print(telnetObj)
            print("connection created")
            return telnetObj

        except:
            print('Failed to create connection. Error message: ')


    def transmit(self, command):
        try:
            print("command sent")
            event_session_obj.send(command.encode())
            print(command.encode())
        except:
            print('Failed to transmit command. Error message: ')


    def update(self, hass, devices):

        while 1:

                print('re-start command session')
                self.transmit('ls \r')
                time.sleep(45)
                response = event_session_obj.recv(120)

                response_d = response.decode()
                print("response single single" , response_d)

                print(type(response_d))
                print(len(response_d))
                res1 = response_d.split(" - ")

                print("printing - - - - - ")
                print(len(res1))
                print(res1)
                for items in res1:
                    print("inside for loop, showing one unit id response one time")
                    new1 = items.strip(">")
                    print(new1)
                    new2 = new1.strip("0\r\n")
                    print(new2)
                    new3 = new2.strip("\r\n>")
                    print(new3)
                    new4 = new3.strip("OK\r\n>")
                    new = new4.strip(" ")


                    print("Printing each element")
                    print(new)

                    each  = new.split(" ")
                    for items in each:
                        if items == '':
                            each.pop(each.index(''))

                    print(each)
                    if len(each) > 3:
                        temp_unit_id = each[0].split(".")[1]
                        device_id = device_unit_dict[temp_unit_id]
                        attributes_dict = {'unit_id': temp_unit_id, "set_temp":each[2],"curr_temp":each[3],"speed":each[4],"mode":each[5]}
                        print(attributes_dict)
                        hass.states.set(DOMAIN+"."+device_id, each[1], attributes_dict)
                        print("Saved state for device_id")
                        print(device_id)







    def turn_on(self,  device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('on '+str(unit_id)+'\r'))

    def turn_off(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('off '+str(unit_id)+'\r'))

    def cool(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('cool '+str(unit_id)+'\r'))

    def heat(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('heat '+str(unit_id)+'\r'))

    def fspeedLOW(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('fspeed '+str(unit_id)+' l\r'))

    def fspeedMEDIUM(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('fspeed '+str(unit_id)+' m\r'))

    def fspeedHIGH(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                print(self.transmit('fspeed '+str(unit_id)+' h\r'))

    def tempSET(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                temperature = call.data.get('temperature')
                print(self.transmit('temp '+str(unit_id)+' '+str(temperature)+'\r')

    def fan(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                temperature = call.data.get('temperature')
                print(self.transmit('fan '+str(unit_id)+' \r')

    def tempUP(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                temperature = call.data.get('temperature')
                print(self.transmit('temp '+str(unit_id)+' +1\r')

    def tempDOWN(self, device_id):
        for device in devices:
            if device['device_id'] == device_id:
                unit_id= device['unit_id']
                temperature = call.data.get('temperature')
                print(self.transmit('temp '+str(unit_id)+' -1\r')

    

                