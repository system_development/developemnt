#!/usr/bin/env python


import struct
import logging
import requests
import urllib
import time
from pathlib import Path
home = str(Path.home())
_LOGGER = logging.getLogger(__name__)

class sim_two_projector():
    """Class to encapsulate the projector.
    """

    def __init__(self, slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'sim_two_projector'
ATTR_NAME = 'entity_id'
CERT_PATH=home+'/slave_certificates/'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('sim_two_projector')['serial_port']
    baudrate = config.get('sim_two_projector')['baudrate']
    device_id = config.get('sim_two_projector')['device_id']
    entity_id = ('sim_two_projector.' + str(device_id))
    slave_ip = config.get('sim_two_projector')['slave_ip']
    slave_port = config.get('sim_two_projector')['slave_port']
    slave_user = config.get('sim_two_projector')['slave_username']
    slave_pwd = config.get('sim_two_projector')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    _LOGGER.info(slave_cert_path)

    def turn_on(call):
        entity_id = call.data.get('entity_id')
        control_command = 'BEEF02060080E5490100000000'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/sim?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x06' in str(response_to_check):
            _LOGGER.info("Valid Response Matched")
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

    #applied endpoint loewe tv to restrict to two iteratons.check and make it proper.
    def turn_off(call):
        entity_id = call.data.get('entity_id')
        control_command = 'BEEF020600B3E54A0100000000'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/sim?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info("Power off:1st command")
        _LOGGER.info(response.text)
        response_one_check = response.text

        attributes_dict = {}
        if 'x06' in str(response_one_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

        elif 'x15' in str(response_one_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'off',attributes_dict)

    def get_power_status(call):
        _LOGGER.info("Get power status is called")
        entity_id = call.data.get('entity_id')
        control_command = 'BEEF100A0034B701010001011501000002'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/sim?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x15' not in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['operation'] = None
            except Exception as error:
                print(error)
                attributes_dict['operation'] = None
            hass.states.set(entity_id,'on',attributes_dict)

    def select_source(call):
        time.sleep(15)
        entity_id = call.data.get('entity_id')
        source = call.data.get('source')
        source_dict = {'hdmi1':'BEEF020600D5E54C0100000000','hdmi2':'BEEF02060004E44D0100000000','video':'BEEF02060080E5490100000000', 'component':'BEEF020600B3E54A0100000000', 'graphics':'BEEF02060062E44B0100000000'}
        control_command = source_dict[source]
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/sim?' + data
        _LOGGER.info(url)
        _LOGGER.info(control_command)
        response = requests.get(url,verify=slave_cert_path)
        _LOGGER.info(response.text)
        response_to_check = response.text

        attributes_dict = {}
        if 'x15' not in str(response_to_check):
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['operation'] = source
                else:
                    attributes_dict['operation'] = source
            except Exception as error:
                print(error)
                attributes_dict['operation'] = source
            hass.states.set(entity_id,'on',attributes_dict)

    hass.services.register(DOMAIN, 'turn_on', turn_on)
    hass.services.register(DOMAIN, 'turn_off', turn_off)
    hass.services.register(DOMAIN, 'get_power_status', get_power_status)
    hass.services.register(DOMAIN, 'select_source', select_source)

    # Return boolean to indicate that initialization was successfully.
    return True

