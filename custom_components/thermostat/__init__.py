
"""
Component to interface with various media players.

For more details about this component, please refer to the documentation at
https://home-assistant.io/components/media_player/
"""

from datetime import timedelta
import logging
from homeassistant.helpers.entity_component import EntityComponent

try:
    from cool_master import cool_master_class
except ImportError:
    from .cool_master import cool_master_class

_LOGGER = logging.getLogger(__name__)
DOMAIN = 'thermostat'
SCAN_INTERVAL = timedelta(seconds=10)

async def async_setup(hass, config):
    """Track states and offer events for media_players."""

    component = hass.data[DOMAIN] = EntityComponent(
        logging.getLogger(__name__), DOMAIN, hass, SCAN_INTERVAL)
    await component.async_setup(config)
    return True

