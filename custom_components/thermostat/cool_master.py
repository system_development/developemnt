
import threading, serial, logging, time
_LOGGER = logging.getLogger(__name__)
DOMAIN = 'thermostat'

def setup_platform(hass, config, add_devices, discovery_info=None):
    """Set up the cool_master platform."""

    global cool_master_configs, conn_obj, serial_port, baudrate
    cool_master_configs = dict(config)
    serial_port = cool_master_configs['serial_port']
    baudrate = cool_master_configs['baudrate']
    class_obj =  cool_master_class()
    conn_obj = class_obj.create_connection(serial_port, baudrate)

    # Defining services
    def on(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'on '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def off(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'off '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def cool(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'cool '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def heat(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'heat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def tempSET(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break
        temperature = service.data.get('temperature')

        command_string = 'temp '+str(unit_id)+' '+str(temperature)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def fspeedLOW(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'fspeed '+str(unit_id)+' l\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def fspeedMEDIUM(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'fspeed '+str(unit_id)+' m\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def fspeedHIGH(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'fspeed '+str(unit_id)+' h\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def fan(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'fan '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def tempUP(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'temp '+str(unit_id)+' +1\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def tempDOWN(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'temp '+str(unit_id)+' -1\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def allon(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'allon\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    def alloff(service):
        entity_id = service.data.get('entity_id')
        device_id = entity_id.split('.')[1]
        for config in cool_master_configs['devices']:
            if config['device_id'] == device_id:
                unit_id = config['unit_id']
                break

        command_string = 'alloff '+str(unit_id)+'\r'
        class_obj.transmit(command_string)
        command_string = 'stat '+str(unit_id)+'\r'
        class_obj.transmit(command_string)

    # Registering services
    service_dict = {
                    'on': on,
                    'off':off,
                    'cool': cool,
                    'heat':heat,
                    'tempSET':tempSET,
                    'fspeedLOW':fspeedLOW,
                    'fspeedMEDIUM':fspeedMEDIUM,
                    'fspeedHIGH':fspeedHIGH,
                    'fan':fan,
                    'tempUP':tempUP,
                    'tempDOWN':tempDOWN,
                    'allon':allon,
                    'alloff':alloff
                    }

    for service in service_dict:
        hass.services.async_register(DOMAIN, service, service_dict[service])

    # Starting thread for real time feedback
    update_thread = threading.Thread(target=class_obj.update, args=(hass,))
    update_thread.start()

class cool_master_class():

    # Create connection with the device
    def create_connection(self, serial_port, baudrate):

        try:
            conn_obj = serial.Serial(serial_port, baudrate, timeout=1)
            return conn_obj

        except Exception as error:
            _LOGGER.error('Failed to create connection. Error message: ' + str(error))

    # Transmit command to the device
    def transmit(self, command_string):

        try:
            _LOGGER.info("command sent")
            command = command_string.encode()
            conn_obj.write(command)
            _LOGGER.info(command)

        except Exception as error:
            _LOGGER.error('Failed to transmit command. Error message: ' + str(error))
            _LOGGER.info("connection re-establishing")
            self.create_connection(serial_port, baudrate)

    # Receive real time feedback from the device
    def update(self, hass):
        initial_time = round(time.time())
        while 1:
            try:
                current_time = round(time.time())
                if current_time-initial_time >= 45:
                    self.transmit('stat\r')
                    initial_time = current_time

                response = conn_obj.readline()
                if response:
                    attributes_dict = {}
                    entity_id = DOMAIN+'.device_'+str(response)[2]+str(response)[3]+str(response)[4]
                    state_object = hass.states.get(entity_id)

                    if state_object:
                        attributes = hass.states.get(entity_id).attributes
                        attributes_dict.update(dict(attributes))

                    if 'ON' in str(response):
                        state = 'on'

                    if 'OFF' in str(response):
                        state = 'off'

                    if 'Cool' in str(response):
                        attributes_dict['mode'] = 'cool'

                    if 'Heat' in str(response):
                        attributes_dict['mode'] = 'heat'

                    if 'High' in str(response):
                        attributes_dict['fan_speed'] = 'high'

                    if 'Med' in str(response):
                        attributes_dict['fan_speed'] = 'med'

                    if 'Low' in str(response):
                        attributes_dict['fan_speed'] = 'low'

                    if str(response)[10] and str(response)[11]:
                        attributes_dict['temperature'] = str(response)[10] + str(response)[11]

                    hass.states.set(entity_id , state, attributes_dict)
            except Exception as error:
                _LOGGER.error(error)
