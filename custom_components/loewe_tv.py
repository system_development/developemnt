#!/usr/bin/env python

import paramiko
import time
import logging
import requests
import urllib
from pathlib import Path
home = str(Path.home())

class loeweTv(object):

    def __init__(self,  slave_ip, slave_port, slave_user, slave_pwd):
        """Set up serial connection to projector."""

        self._serial_port = serial_port
        self._baudrate = baudrate
        self.slave_ip = slave_ip
        self.slave_port = slave_port
        self.slave_user = slave_user
        self.slave_pwd = slave_pwd


        #Based on the room parameter(configuration.yaml), we have to find out which sub-okas it is.Se configuration.yaml
        ip = self.slave_ip
        port= self.slave_port
        username= self.slave_user
        password= self.slave_pwd
        try:
            self.ssh = paramiko.SSHClient()
            self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.ssh.connect(ip,port,username,password)
            print("Trying to establish connection through ssh")

        except Exception as error:
            print('Connection Failed')
            print(error)
            return

    def _command_handler(self, command_string):
        print("Command string")
        print(command_string)
        self.ssh.exec_command(command_string)
        return True



# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'loewe_tv'
ATTR_NAME = 'entity_id'
CERT_PATH=home+'/slave_certificates/'
def setup(hass, config):

    #"""Set up is called when Home Assistant is loading our component."""
    global serial_port, baudrate,device_id
    serial_port = config.get('loewe_tv')['serial_port']
    baudrate = config.get('loewe_tv')['baudrate']
    device_id = config.get('loewe_tv')['device_id']
    entity_id = ('loewe_tv.' + str(device_id))
    slave_ip = config.get('loewe_tv')['slave_ip']
    slave_port = config.get('loewe_tv')['slave_port']
    slave_user = config.get('loewe_tv')['slave_username']
    slave_pwd = config.get('loewe_tv')['slave_password']
    slave_cert_domain = slave_ip.split('.')[3]
    slave_cert_name = 'ca_'+str(slave_cert_domain)+'.crt'
    slave_cert_path = CERT_PATH+slave_cert_name
    print(slave_cert_path)

    def power_on(call):
        entity_id = call.data.get('entity_id')
        control_command = "ir 0 0 22 3\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)

        attributes_dict = {}

        if response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                else:
                    attributes_dict['source'] = None
                    attributes_dict['volume'] = None
                    attributes_dict['mute'] = None
            except Exception as error:
                print(error)
                attributes_dict['source'] = None
                attributes_dict['volume'] = None
                attributes_dict['mute'] = None

            hass.states.set(entity_id, "on", attributes_dict)

    def power_off(call):
        entity_id = call.data.get('entity_id')
        control_command = "ir 0 0 25 3\r\n"
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)

        attributes_dict = {}

        if response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['source'] = None
                    attributes_dict['mute'] = 'off'
                else:
                    attributes_dict['source'] = None
                    attributes_dict['volume'] = None
                    attributes_dict['mute'] = 'off'

            except Exception as error:
                print(error)
                attributes_dict['source'] = None
                attributes_dict['volume'] = None
                attributes_dict['mute'] = 'off'
            hass.states.set(entity_id, "off", attributes_dict)

    def mute_volume(call):
        entity_id = call.data.get('entity_id')
        mute_dict={'on':"data mute 1\r\n",'off':"data mute 0\r\n"}
        mute = call.data.get("mute")
        print("mute status")
        print(mute)
        command = mute_dict[mute]

        data = {'port': serial_port, 'baud': baudrate, 'command': command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)

        attributes_dict = {}

        if response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['mute'] = 'off'
                else:
                    attributes_dict['source'] = None
                    attributes_dict['volume'] = None
                    attributes_dict['mute'] = 'off'
            except Exception as error:
                print(error)
                attributes_dict['source'] = None
                attributes_dict['volume'] = None
                attributes_dict['mute'] = 'off'
            hass.states.set(entity_id, 'on', attributes_dict)
        elif 'data mute 1' in response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['mute'] = 'on'
                else:
                    attributes_dict['source'] = None
                    attributes_dict['volume'] = None
                    attributes_dict['mute'] = 'on'
            except Exception as error:
                print(error)
                attributes_dict['source'] = None
                attributes_dict['volume'] = None
                attributes_dict['mute'] = 'off'
            hass.states.set(entity_id, 'on', attributes_dict)

    def set_volume_level(call):
        entity_id = call.data.get('entity_id')
        volume = call.data.get("volume")
        control_command = 'data volume '+str(volume)+'\r\n'
        data = {'port': serial_port, 'baud': baudrate, 'command': control_command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)

        attributes_dict = {}

        if response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['volume'] = volume
                else:
                    attributes_dict['source'] = None
                    attributes_dict['volume'] = volume
                    attributes_dict['mute'] = None
            except Exception as error:
                print(error)
                attributes_dict['source'] = None
                attributes_dict['volume'] = volume
                attributes_dict['mute'] = None
            hass.states.set(entity_id, 'on', attributes_dict)

    def select_source(call):
        entity_id = call.data.get('entity_id')
        loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
        source_dict={'HDMI1':"prog -6\r\n",'HDMI2':"prog -7\r\n"}
        source = call.data.get("source")
        print("the source is=")
        print(source)
        command = source_dict[source]
        data = {'port': serial_port, 'baud': baudrate, 'command': command}
        data = urllib.parse.urlencode(data)
        url = 'https://' + str(slave_ip) + '/loewetv?' + data
        print(data)
        print(url)
        response = requests.get(url,verify=slave_cert_path)
        print(response.text)

        attributes_dict = {}

        if response.text:
            state_object = hass.states.get(entity_id)
            try:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict.update(dict(attributes))
                    attributes_dict['source'] = source
                else:
                    attributes_dict['source'] = source
                    attributes_dict['volume'] = None
                    attributes_dict['mute'] = None
            except Exception as error:
                print(error)
                attributes_dict['source'] = source
                attributes_dict['volume'] = None
                attributes_dict['mute'] = None
            hass.states.set(entity_id, 'on', attributes_dict)
            

    # def key0(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "prog 0\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key1(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "prog 1\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key2(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 2 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key3(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 3 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key4(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 4 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key5(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 5 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key6(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 6 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key7(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 7 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key8(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 8 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def key9(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 9 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def menu(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 11 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def normal(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 14 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def cursorRight(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 16 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def cursorLeft(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 17 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def programDown(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 23 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def programUp(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 24 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def green(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 26 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def red(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 27 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def VCR1(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 28 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def DVD(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 30 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def cursorUp(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 32 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def cursorDown(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 33 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on')
    #
    # def picture(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 35 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'picture'})
    #
    # def SAT(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 36 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'SAT'})
    #
    # def STB(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 37 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'STB'})
    #
    # def OKKey(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 38 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'OKKey'})
    #
    # def blueKey(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 40 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'blueKey'})
    #
    # def yellowKey(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     control_command = "ir 0 0 43 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'yellowKey'})
    #
    # def assistMenu(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 49 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'assistMenu'})
    #
    # def radioMode(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 53 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'radioMode'})
    #
    # def endKey(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 63 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'endKey'})
    #
    # def TVModeOn(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 72 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'TVModeOn'})
    #
    # def radioModeOn(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "power radio\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'radioModeOn'})
    #
    # def infoKey(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 79 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'infoKey'})
    #
    # def AV1Input(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 114 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'AV1Input'})
    #
    # def AV2Input(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 115 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'AV2Input'})
    #
    # def AV3Input(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 116 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'AV3Input'})
    #
    # def AVSInput(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 117 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'AVSInput'})
    #
    # def VGAInput(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 118 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'VGAInput'})
    #
    # def componentInput(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 0 120 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'componetInput'})
    #
    # def videoInput(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "prog -8\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'videoInput'})
    #
    # def HDRPause(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 41 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRPause'})
    #
    # def HDRRewind(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 50 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRRewind'})
    #
    # def HDRFastForward(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 52 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRFastForward'})
    #
    # def HDRPlay(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 53 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRPlay'})
    #
    # def HDRStop(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 54 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRStop'})
    #
    # def HDRRecord(call):
    #     name = call.data.get(ATTR_NAME, entity_id)
    #     loeweTvObj = loeweTv(slave_ip, slave_port, slave_user, slave_pwd)
    #     control_command = "ir 0 6 55 3\r\n"
    #     command_string = 'python3 -c "from serial_devices_slave import serial_devices_slave_class; serial_devices_slave_class( \'{}\' , \'{}\' ).tx_rx( {} )"'.format(serial_port, baudrate, control_command.encode())
    #     response = loeweTvObj._command_handler(command_string)
    #     if response:
    #         hass.states.set(entity_id , 'on',{'operation':'HDRRecord'})

    hass.services.register(DOMAIN, 'power_on', power_on)
    hass.services.register(DOMAIN, 'power_off', power_off)
    hass.services.register(DOMAIN, 'mute_volume', mute_volume)
    hass.services.register(DOMAIN, 'set_volume_level', set_volume_level)
    hass.services.register(DOMAIN, 'select_source', select_source)

    # hass.services.register(DOMAIN, 'key0', key0)
    # hass.services.register(DOMAIN, 'key1', key1)
    # hass.services.register(DOMAIN, 'key2', key2)
    # hass.services.register(DOMAIN, 'key3', key3)
    # hass.services.register(DOMAIN, 'key4', key4)
    # hass.services.register(DOMAIN, 'key4', key5)
    # hass.services.register(DOMAIN, 'key5', key6)
    # hass.services.register(DOMAIN, 'key7', key7)
    # hass.services.register(DOMAIN, 'key8', key8)
    # hass.services.register(DOMAIN, 'key9', key9)
    # hass.services.register(DOMAIN, 'menu', menu)
    # hass.services.register(DOMAIN, 'normal', normal)
    # hass.services.register(DOMAIN, 'cursorRight', cursorRight)
    # hass.services.register(DOMAIN, 'cursorLeft', cursorLeft)
    # hass.services.register(DOMAIN, 'programDown', programDown)
    # hass.services.register(DOMAIN, 'programUp', programUp)
    # hass.services.register(DOMAIN, 'green', green)
    # hass.services.register(DOMAIN, 'red', red)
    # hass.services.register(DOMAIN, 'VCR1', VCR1)
    # hass.services.register(DOMAIN, 'DVD', DVD)
    # hass.services.register(DOMAIN, 'cursorUp', cursorUp)
    # hass.services.register(DOMAIN, 'cursorDown', cursorDown)
    # hass.services.register(DOMAIN, 'picture', picture)
    # hass.services.register(DOMAIN, 'SAT', SAT)
    # hass.services.register(DOMAIN, 'STB', STB)
    # hass.services.register(DOMAIN, 'OKKey', OKKey)
    # hass.services.register(DOMAIN, 'blueKey', blueKey)
    # hass.services.register(DOMAIN, 'yellowKey', yellowKey)
    # hass.services.register(DOMAIN, 'assistMenu', assistMenu)
    # hass.services.register(DOMAIN, 'radioMode', radioMode)
    # hass.services.register(DOMAIN, 'infoKey', infoKey)
    # hass.services.register(DOMAIN, 'AV1Input', AV1Input)
    # hass.services.register(DOMAIN, 'AV2Input', AV2Input)
    # hass.services.register(DOMAIN, 'AV3Input', AV3Input)
    # hass.services.register(DOMAIN, 'AVSInput', AVSInput)
    # hass.services.register(DOMAIN, 'VGAInput', VGAInput)
    # hass.services.register(DOMAIN, 'componentInput', componentInput)
    # hass.services.register(DOMAIN, 'videoInput', videoInput)
    # hass.services.register(DOMAIN, 'HDRPause', HDRPause)
    # hass.services.register(DOMAIN, 'HDRRewind', HDRRewind)
    # hass.services.register(DOMAIN, 'HDRFastForward', HDRFastForward)
    # hass.services.register(DOMAIN, 'HDRPlay', HDRPlay)
    # hass.services.register(DOMAIN, 'HDRStop', HDRStop)
    # hass.services.register(DOMAIN, 'HDRRecord', HDRRecord)
    # hass.services.register(DOMAIN, 'endKey', endKey)
    # hass.services.register(DOMAIN, 'yellowKey', yellowKey)
    # hass.services.register(DOMAIN, 'assistMenu', assistMenu)
    # hass.services.register(DOMAIN, 'TVModeOn', TVModeOn)
    # hass.services.register(DOMAIN, 'radioModeOn', radioModeOn)

    # Return boolean to indicate that initialization was successfully.
    return True
