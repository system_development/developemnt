import requests, json, time, logging,traceback
import os
import sys
from pathlib import Path
from homeassistant.core import valid_entity_id, split_entity_id
from voluptuous import *
import socket
hostname = socket.gethostname()
IPAddr = socket.gethostbyname(hostname)
home = str(Path.home())
_LOGGER = logging.getLogger(__name__)

class SequenceClass():
    def __init__(self):
        self.schema_room_id = None
        self.si_data = None
        self.entities_dict = None
        self.okas_mplayer_dict = None
        self.okas_slave_ip_dict = None
        self.hass = None
        self.room_id = None
        self.multiroom_id = None
        self.device_name = None
        self.channel_name = None
        self.songid = None
        self.movieid = None
        self.musicvideoid = None
        self.playlistid = None
        self.position = None
        self.resume = None
        self.speed = None
        self.udid = None
        self.song_type = None
        self.action = None
        self.zone = None
        self.is_multiroom = None
        self.response_dict = None

    def api_params(self,
                    schema_room_id,
                    si_data,
                    entities_dict,
                    okas_mplayer_dict,
                    okas_slave_ip_dict,
                    hass,
                    room_id,
                    multiroom_id,
                    device_name,
                    channel_name,
                    songid ,
                    movieid,
                    musicvideoid,
                    playlistid,
                    position,
                    resume,
                    speed,
                    udid,
                    song_type,
                    action,
                    zone,
                    is_multiroom):

        self.schema_room_id = schema_room_id
        self.si_data = si_data
        self.entities_dict = entities_dict
        self.okas_mplayer_dict = okas_mplayer_dict
        self.okas_slave_ip_dict = okas_slave_ip_dict
        self.hass = hass
        self.room_id = room_id
        self.multiroom_id = multiroom_id
        self.device_name = device_name
        self.channel_name = channel_name
        self.songid = songid
        self.movieid = movieid
        self.musicvideoid = musicvideoid
        self.playlistid = playlistid
        self.position = position
        self.resume = resume
        self.speed = speed
        self.udid = udid
        self.song_type = song_type
        self.action = action
        self.zone = zone
        self.is_multiroom = is_multiroom

    def single_room_response_dict(self, okasbox_dict):
        url = 'https://'+str(okasbox_dict[self.room_id])+'/kodi?song_type='+str(self.song_type)+'&songid='+str(self.songid)
        response = requests.post(url=url, verify=False)
        self.response_dict = response.json()

    def multiroom_response_dict(self, boxip_list,okas_boxes):
        print("mplayer_boxid",okas_boxes )
        url = 'http://'+str(IPAddr)+ ':3001/kodi/play_queue'
        payload = {"ip": boxip_list, "songID": self.songid,"song_type":self.song_type}
        headers = {
          'Content-Type': 'application/json'
        }
        response = requests.post( url, data=json.dumps(payload), headers=headers)
        self.response_dict = response.json()

        if 'data' in self.response_dict:
            main_list = self.response_dict['data']
            songid = main_list[main_list.index(self.songid)+1]
            #Adding next song in queue
            service_data = {"entity_id":"media_player."+ str(okas_boxes),"udid":self.udid, "method": "Playlist.Add","playlistid": 0 ,"item":{"songid":songid}}
            self.hass.services.call("media_player", "kodi_call_method", service_data, False)

    def device_macro_okas(self, macro_service_data):
        try:
            device_id = macro_service_data['device_id']
            service = macro_service_data['service']
            device_name = macro_service_data['device_name']

            # Fetch Macro conditions and actions from the Macros
            okas_mplayer_device_macro = self.si_data['Macros']
            for okas_macros in okas_mplayer_device_macro:
                if okas_macros['macro_type'] == 'deviceMacro':
                    if okas_macros['macro_param']['deviceType'] == 'okas_mplayer':

                        if okas_macros['macro_param']['roomID'] == self.room_id and self.okas_mplayer_dict[okas_macros['macro_param']['roomID']] == device_id and 'media_player' == device_name and okas_macros['macro_param']['service'] == service:
                            for okas_macro_actions in okas_macros['macro_actions']:
                                okas_macro_action_room_id = okas_macro_actions['roomID']
                                okas_macro_action_service = okas_macro_actions['service']
                                okas_macro_action_device_type = okas_macro_actions['deviceType']
                                okas_room_id = 'room_' + str(okas_macro_action_room_id)

                                if 'mood' in str(okas_macro_action_device_type):
                                    MACRO_DOMAIN = "custom_scene"
                                    MACRO_SERVICE = 'select_scene'
                                    mood_id = "mood_" + str(okas_macro_action_service)
                                    entity_id = MACRO_DOMAIN + "." + str(okas_room_id)
                                    scene_name = "scene." + str(mood_id)
                                    service_data = {"entity_id": entity_id, "scene_name": scene_name}
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                                    _LOGGER.info('Executing mood action on okas mplayer condition in device macro')

                                elif 'configure' in str(okas_macro_action_device_type):
                                    MACRO_DOMAIN = "cover"
                                    MACRO_SERVICE = okas_macro_action_service
                                    entity_id = MACRO_DOMAIN + "." + str(okas_macro_action_device_type)
                                    service_data = {"entity_id": entity_id}
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                                    _LOGGER.info('Executing cover action on okas mplayer condition in device macro')
                                else:
                                    okas_macro_action_device_id = 'device_' + str(okas_macro_action_device_type)
                                    MACRO_DOMAIN = self.entities_dict[okas_macro_action_device_id]
                                    MACRO_SERVICE = okas_macro_action_service
                                    entity_id = MACRO_DOMAIN + "." + str(okas_macro_action_device_id)
                                    service_data = {"entity_id": entity_id}
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                                    _LOGGER.info('Executing a device action on okas mplayer condition in device macro')

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})


    def device_macro(self, macro_service_data):
        try:
            _LOGGER.info('Executing a device macro')
            device_id = macro_service_data['device_id']
            service = macro_service_data['service']
            device = macro_service_data['device']
            device_name = macro_service_data['device_name']
            devicemacro = macro_service_data['devicemacro']

            for macro_actions in devicemacro['macro_actions']:
                action_room_id = macro_actions['roomID']
                macro_action_room_id = 'room_' + str(action_room_id)
                macro_action_service = macro_actions['service']
                action_device_number = macro_actions['deviceType']
                kodi_device_id = self.okas_mplayer_dict[action_room_id]

                if  devicemacro['macro_param']['roomID'] == self.room_id and 'device_' + str(devicemacro['macro_param']['deviceType']) == device_id and devicemacro['macro_param']['service'] == service and device['device_name'] == device_name:
                    _LOGGER.info('entered condition in device macro')
                    if 'mood' in str(action_device_number):
                        MACRO_DOMAIN = "custom_scene"
                        MACRO_SERVICE = 'select_scene'
                        mood_id = "mood_"+ str(macro_action_service)
                        entity_id = MACRO_DOMAIN + "." + str(macro_action_room_id)
                        scene_name = "scene." + str(mood_id)
                        service_data = {"entity_id": entity_id, "scene_name": scene_name}
                        self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                        _LOGGER.info('Executing mood action in device macro')

                    elif 'okas_mplayer' in str(action_device_number):
                        OKAS_DOMAIN = 'media_player'
                        OKAS_SERVICE = 'kodi_call_method'
                        okas_stop_service_data = {"entity_id": "media_player." + str(kodi_device_id),"udid":'',
                                                  "method": "Player." + str(macro_action_service),
                                                  "playerid": playerid}
                        self.hass.services.call(OKAS_DOMAIN, OKAS_SERVICE, okas_stop_service_data,False)
                        _LOGGER.info('Executing okas_mplayer movie action in device macro')

                    elif 'configure' in str(action_device_number):
                        MACRO_DOMAIN = "cover"
                        MACRO_SERVICE = str(macro_action_service)
                        entity_id = MACRO_DOMAIN + "." + str(action_device_number)
                        service_data = {"entity_id": entity_id}
                        self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                        _LOGGER.info('Executing cover action in device macro')

                    else:
                        macro_action_device_id = 'device_'+str(action_device_number)
                        macro_action_device_name = self.entities_dict[macro_action_device_id]

                        if 'CBL/SAT' in macro_action_service or 'Media Player' in macro_action_service or 'Blu-ray' in macro_action_service:
                            MACRO_DOMAIN = self.entities_dict[macro_action_device_id]
                            entity_id = MACRO_DOMAIN + "." + macro_action_room_id
                            service_data = {"entity_id": entity_id,
                                            "source": macro_action_service}
                            MACRO_SERVICE = 'select_source'
                            if self.hass.states.get(entity_id) == None:
                                self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE,
                                                   service_data, False)
                            elif bool(self.hass.states.get(
                                    entity_id).attributes) == True and 'source' in (
                                    self.hass.states.get(entity_id).attributes).keys():
                                if (self.hass.states.get(entity_id).state != 'on') or (
                                        (self.hass.states.get(entity_id).state != 'off') and (
                                        self.hass.states.get(entity_id).attributes['source'] == None)):
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE,
                                                       service_data, False)
                                elif (self.hass.states.get(entity_id).attributes['source'] != macro_action_service):
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE,
                                                       service_data, False)
                            else:
                                self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                            _LOGGER.info('Executing a source switching action in device macro')
                        else:
                            MACRO_DOMAIN = self.entities_dict[macro_action_device_id]
                            MACRO_SERVICE = macro_action_service
                            entity_id = MACRO_DOMAIN + "." + macro_action_device_id
                            service_data = {"entity_id": entity_id}

                            #Fix for lift in device macro
                            if 'lift' in MACRO_DOMAIN:
                                if self.hass.states.get(entity_id) == None:
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data,
                                                       False)
                                elif self.hass.states.get(entity_id) != None and (
                                        self.hass.states.get(entity_id).state) != 'out':
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data,
                                                       False)
                                _LOGGER.info('Executing a lift action in device macro')

                            else:
                                if self.hass.states.get(entity_id) == None:
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data,
                                                       False)
                                elif self.hass.states.get(entity_id) != None and (
                                        self.hass.states.get(entity_id).state) != 'on':
                                    self.hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data,
                                                       False)
                                _LOGGER.info('Executing a device action in device macro')

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})
        global okas_box_type
    def multiroom(self, roomwise_device_type_dict, okasbox_dict):
        try:
            _LOGGER.info('Handle okas in multiroom')
            okas_boxes = self.si_data['okasBox']
            okas_slave_ip_dict = {}
            roomid_list = []
            boxip_list = []

            okas_boxes = self.si_data['okasBox']
            for kodi in okas_boxes:
                okas_box_mplayer_id = kodi['okasbox_id']
                okas_box_type = kodi['okasboxtype']
                okas_box_room_id = kodi['room']
                okas_box_ip = kodi['okasboxip']
                if okas_box_type != 'Master Controller':
                    self.okas_mplayer_dict[okas_box_room_id] = okas_box_mplayer_id
                    okas_slave_ip_dict[okas_box_room_id] = okas_box_ip
                    roomid_list.append( okas_box_room_id )


            if self.action == "play":
                for room in roomid_list:
                    for room2 in self.multiroom_id:
                        if room2 == room:
                            slave_ip = okas_slave_ip_dict[room2]
                            boxip_list.append(slave_ip)
                if self.song_type == 'single_song':
                    # playing single song in multiroom
                    url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                    payload= {"ip": boxip_list, "songID": self.songid}
                    headers = {'Content-Type' : 'application/json'}
                    response = requests.post(url, data=json.dumps(payload), headers=headers)
                # Playing queue
                elif self.playlistid != None and self.position != None:
                    service_data = {"entity_id": "media_player." + str(self.okas_mplayer_dict[room]), "udid":self.udid, "method": "Player." + str(kodi_method),
                    "item": {"playlistid": self.playlistid, "position": self.position}}
                    self.hass.services.call("media_player", "kodi_call_method", service_data, False)
                elif self.movieid != None and self.resume != None:
                    service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                    "item": {"movieid": self.movieid}, "options": {"resume": self.resume}}
                    self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                elif self.movieid != None:
                    service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                    "item": {"movieid": self.movieid}}
                    self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                elif self.musicvideoid != None:
                    service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                    "item": {"musicvideoid": self.musicvideoid}}
                    self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                # Playing queue
                elif self.song_type == 'all_songs':
                     self.multiroom_response_dict(boxip_list,self.okas_mplayer_dict[self.room_id])

            elif self.action == "playpause":
               url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
               payload = {}
               headers = {}
               response = requests.request('GET', url, headers = headers, data = payload, allow_redirects=False)

            elif self.action == "stop":
                 url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                 payload = {}
                 headers = {}
                 response = requests.request('GET', url, headers = headers, data = payload, allow_redirects=False)

            elif self.action == "status":
                url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                payload = {}
                headers = {}
                response = requests.request('GET',url,headers = headers,data = payload, allow_redirects= False)
                json_data =response.json()
                boxip = json_data.get("clients")
                status =json_data.get("status")
                songid = json_data.get("songid")

                okas_room_id_dict = {}
                for kodi in okas_boxes:
                    okas_box_type = kodi['okasboxtype']
                    okas_box_room_id = kodi['room']
                    okas_box_ip = kodi['okasboxip']

                    if okas_box_type != 'Master Controller':
                        okas_room_id_dict[okas_box_ip] = okas_box_room_id

                okas_box_list = []
                for box_slave_ip in boxip:
                    okas_box_id = okas_room_id_dict[box_slave_ip]
                    okas_box_list.append(okas_box_id)

                event_data = {"multiroom_id":okas_box_list ,'state':status,'songid': songid,'udid' : self.udid}
                self.hass.bus.fire( 'multiroom_status', event_data=event_data)

            elif self.action == "time":
                  url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                  payload = {}
                  headers = {}
                  response = requests.request('GET',url,headers = headers,data = payload, allow_redirects= False)
                  json_data =response.json()

            elif self.action == "add":
                url ='http://'+str(IPAddr)+':3001/kodi/status'
                payload= {}
                headers = {}
                response =  requests.get( url, data=json.dumps(payload), headers=headers)
                json_data = response.json()
                box_ip = json_data.get('clients')

                # Calling change API
                okas_room_id_dict = {}
                for kodi in okas_boxes:
                 okas_box_mplayer_id = kodi['okasbox_id']
                 okas_box_type = kodi['okasboxtype']
                 okas_box_room_id = kodi['room']
                 okas_box_ip = kodi['okasboxip']

                 if okas_box_type != 'Master Controller':
                     okas_room_id_dict[okas_box_ip] = okas_box_room_id

                okas_box_list = []
                for box_slave_ip in box_ip:
                  okas_box_id = okas_room_id_dict[box_slave_ip]
                  okas_box_list.append(okas_box_id)

                okas_room_id_dict = {}
                for kodi in okas_boxes:
                   okas_box_type = kodi['okasboxtype']
                   okas_box_room_id = kodi['room']
                   okas_box_ip = kodi['okasboxip']

                   if okas_box_type != 'Master Controller':
                       okas_room_id_dict[okas_box_room_id] = okas_box_ip

                okas_box_list.append(okas_room_id_dict[self.multiroom_id[0]])
                url = 'http://'+str(IPAddr)+':3001/kodi/change'
                payload = {"ip": okas_box_list}
                headers = {'Content-Type': 'application/json'}
                response =  requests.get( url, data=json.dumps(payload), headers=headers)
                json_data = response.json()

            elif self.action == "delete":
                url= 'http://'+str(IPAddr)+':3001/kodi/status'
                payload= {}
                headers = {}
                response =  requests.get( url, data=json.dumps(payload), headers=headers)
                json_data = response.json()
                box_ip = json_data.get('clients')

                # Calling change API
                okas_room_id_dict = {}
                for kodi in okas_boxes:
                 okas_box_type = kodi['okasboxtype']
                 okas_box_room_id = kodi['room']
                 okas_box_ip = kodi['okasboxip']

                 if okas_box_type != 'Master Controller':
                     okas_room_id_dict[okas_box_ip] = okas_box_room_id

                okas_box_list = []
                for box_slave_ip in box_ip:
                  okas_box_list.append(box_slave_ip)

                okas_room_id_dict = {}
                for kodi in okas_boxes:
                   okas_box_type = kodi['okasboxtype']
                   okas_box_room_id = kodi['room']
                   okas_box_ip = kodi['okasboxip']

                   if okas_box_type != 'Master Controller':
                       okas_room_id_dict[okas_box_room_id] = okas_box_ip

                okas_box_list.remove(okas_room_id_dict[self.multiroom_id[0]])
                url = 'http://'+str(IPAddr)+':3001/kodi/change'
                payload = {"ip": okas_box_list}
                headers = {'Content-Type': 'application/json'}
                response = requests.post( url, data=json.dumps(payload), headers=headers)

            elif self.action == "next":
                url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                payload= {}
                headers = {}
                response = requests.request('GET', url, headers = headers, data = payload, allow_redirects=False)

            elif self.action == "prev":
                url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                payload= {}
                headers = {}
                response = requests.request('GET', url, headers = headers, data = payload, allow_redirects=False)

            elif self.action == "seek":
                 url = 'http://'+str(IPAddr)+':3001/kodi/'+str(self.action)
                 payload = {"time" : self.resume}
                 headers = {'Content-Type' : 'application/json'}
                 response = requests.post( url, data=json.dumps(payload), headers=headers)

            _LOGGER.info('Play okas mplayer movie/song')
            # # Multiroom Okas Mplayer Device Macro
            # macro_service_data_okas_open = {'room_id': self.multiroom_id,
            #                 'device_id': self.okas_mplayer_dict[room],
            #                 'device_name': kodi_device_name,
            #                 'service': 'PlayPause',
            #                 'roomwise_device_type_dict': roomwise_device_type_dict,
            #                 'entities_dict': self.entities_dict,
            #                 'okas_mplayer_dict': self.okas_mplayer_dict,
            #                 'udid':self.udid
            #                 }
            #
            # self.device_macro_okas(macro_service_data_okas_open)

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})


    def input_handler(self, roomwise_device_type_dict, kodi_device_id, kaleidescape_device_id):

        try:
            global response_dict
            global udid

            _LOGGER.info('Controlling input device')
            okasbox_dict = {}
            for okasbox in self.si_data['okasBox']:
                okasbox_dict[okasbox['room']] = okasbox['okasboxip']

            for room_info in self.si_data['room']:
                if self.room_id == room_info['room_id']:
                    for device in room_info['device']:
                        if device['device_name'] == self.device_name:
                            media_player_device_id = device['device_id']
                            break

            # Handling input device tatasky, channel set, its device macro and okas pause.
            if self.device_name == 'tatasky':
                device_id = roomwise_device_type_dict['satellite']['device_id']
                DOMAIN = 'tatasky'
                SERVICE = 'power_on'
                entity_id = str(self.device_name) + "." + str(device_id)
                service_data = {'entity_id': entity_id}


                if self.hass.states.get(entity_id) == None:
                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                    time.sleep(2)
                elif self.hass.states.get(entity_id) != None and (self.hass.states.get(entity_id).state) != 'on':
                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                    time.sleep(2)
                _LOGGER.info('Turn on tatasky')
                service_data = {'entity_id':entity_id, 'channel_name':self.channel_name}
                self.hass.services.call('tatasky', 'set_channel_name', service_data, False)
                _LOGGER.info('Set channel on tatasky')

                #Call okas_mplayer_pause API
                if self.speed == 1:
                    kodi_method = 'PlayPause'
                    OKAS_PAUSE_DOMAIN = 'media_player'
                    OKAS_PAUSE_SERVICE = 'kodi_call_method'

                    okas_pause_service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid,"method": "Player."+str(kodi_method), "playerid": playerid}
                    self.hass.services.call(OKAS_PAUSE_DOMAIN, OKAS_PAUSE_SERVICE, okas_pause_service_data, False)
                    _LOGGER.info('Pause okas_mplayer')

                # Call device macro if present.
                for room_info in self.si_data['room']:
                    if room_info['room_id'] == self.room_id:
                        for device in room_info['device']:
                            for devicemacro in device['devicemacro']:
                                if len(devicemacro) > 0:
                                    macro_service_data = {
                                      'room_id': self.room_id,
                                      'device_id': device_id,
                                      'device_name': DOMAIN,
                                      'service': SERVICE,
                                      'roomwise_device_type_dict': roomwise_device_type_dict,
                                      'entities_dict' : self.entities_dict,
                                      'okas_mplayer_dict': self.okas_mplayer_dict,
                                      'device': device,
                                      'devicemacro': devicemacro
                                    }

                                    self.device_macro(macro_service_data)


           # Handling all media players its device macro and okas pause
            elif self.device_name == 'apple_tv' or self.device_name == 'oppo_blu_ray' or self.device_name == 'kaleidescape':

                # Call okas_mplayer_pause API
                if self.speed == 1:
                    OKAS_PAUSE_DOMAIN = 'media_player'
                    OKAS_PAUSE_SERVICE = 'kodi_call_method'
                    kodi_method = 'PlayPause'
                    okas_pause_service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid,"method": "Player."+str(kodi_method), "playerid": playerid}
                    self.hass.services.call(OKAS_PAUSE_DOMAIN, OKAS_PAUSE_SERVICE, okas_pause_service_data, False)
                    _LOGGER.info('Pause okas_mplayer')

                if self.action != None:
                    # Execute Play/Pause/Stop
                    _LOGGER.info({'entity_id': self.device_name + '.' + str(media_player_device_id)})
                    self.hass.services.call(self.device_name, self.action, {'entity_id': self.device_name + '.' + str(media_player_device_id)}, False)
                    _LOGGER.info('play/pause/stop media players')

                # Call device macro on play/pause/stop.
                for room_info in self.si_data['room']:
                    if room_info['room_id'] == self.room_id:
                        for device in room_info['device']:
                            for devicemacro in device['devicemacro']:
                                if len(devicemacro) > 0:
                                    macro_service_data = {'room_id': self.room_id,
                                      'device_id': media_player_device_id,
                                      'device_name': self.device_name,
                                      'service': self.action,
                                      'roomwise_device_type_dict': roomwise_device_type_dict,
                                      'entities_dict': self.entities_dict,
                                      'okas_mplayer_dict': self.okas_mplayer_dict,
                                      'device': device,
                                      'devicemacro': devicemacro
                                    }
                                    self.device_macro(macro_service_data)


            # Handling input device okas_mplayer and its device macro
            elif self.device_name == 'okas_mplayer':

                if self.is_multiroom:
                    # Handling okas in multiroom

                    service_data = {'room_id' : self.room_id,
                                    'multiroom_id' : self.multiroom_id,
                                    'device_name': self.device_name,
                                    'channel_name': self.channel_name,
                                    'songid' : self.songid,
                                    'movieid': self.movieid,
                                    'musicvideoid': self.musicvideoid,
                                    'playlistid': self.playlistid,
                                    'position': self.position,
                                    'resume': self.resume,
                                    'speed': self.speed,
                                    'kodi_device_id': kodi_device_id,
                                    'kaleidescape_device_id': kaleidescape_device_id,
                                    'roomwise_device_type_dict': roomwise_device_type_dict,
                                    'entities_dict': self.entities_dict,
                                    'okas_mplayer_dict': self.okas_mplayer_dict,
                                    'udid': self.udid,
                                    'song_type': self.song_type,
                                    'action': self.action
                                    }

                    self.multiroom(roomwise_device_type_dict, okasbox_dict)

                else:
                    # Handling okas in single room

                    if self.action != None and (self.action == 'pause' or self.action == 'play'):
                        # play/pause okas_mplayer
                        kodi_device_name = 'media_player'
                        kodi_method = 'PlayPause'

                        okas_pause_service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid,"method": "Player."+str(kodi_method), "playerid": playerid}
                        self.hass.services.call(kodi_device_name, 'kodi_call_method', okas_pause_service_data, False)
                        _LOGGER.info('play/pause okas_mplayer')

                        # Device Macro on okas_mplayer pause
                        macro_service_data_okas_open = {'room_id': self.room_id,
                                                        'device_id': kodi_device_id,
                                                        'device_name': kodi_device_name,
                                                        'service': 'PlayPause',
                                                        'roomwise_device_type_dict': roomwise_device_type_dict,
                                                        'entities_dict': self.entities_dict,
                                                        'okas_mplayer_dict': self.okas_mplayer_dict,
                                                        'udid':self.udid

                                                        }

                        self.device_macro_okas(macro_service_data_okas_open)

                    elif self.action != None and (self.action == 'stop'):
                        # Pause okas_mplayer
                        kodi_device_name = 'media_player'
                        kodi_method = 'stop'
                        service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid,"method": "Player."+str(kodi_method), "playerid": playerid}
                        self.hass.services.call(kodi_device_name, 'kodi_call_method', service_data, False)
                        _LOGGER.info('Stop okas_mplayer')

                        # Device Macro on okas_mplayer pause
                        macro_service_data_okas_open = {'room_id': self.room_id,
                                                        'device_id': kodi_device_id,
                                                        'device_name': kodi_device_name,
                                                        'service': 'PlayPause',
                                                        'roomwise_device_type_dict': roomwise_device_type_dict,
                                                        'entities_dict': self.entities_dict,
                                                        'okas_mplayer_dict': self.okas_mplayer_dict,
                                                        'udid':self.udid
                                                        }

                        self.device_macro_okas(macro_service_data_okas_open)

                    else:
                        kodi_method = 'Open'
                        kodi_device_name = 'media_player'

                        #Play okas_mplayer
                        if self.songid != None:
                            # Clear playlist queue
                            service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Playlist.Clear","playlistid": 0}
                            self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                            # Playing single song
                            if self.song_type == 'single_song':
                                service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                            "item": {"songid": self.songid}}
                                self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                            # Playing playlist
                            else:
                                # Adding current song in queue
                                service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Playlist.Add","playlistid": 0 ,"item":{"songid": self.songid}}
                                self.hass.services.call("media_player", "kodi_call_method", service_data, False)
                                time.sleep(1)

                                # Playing current song
                                service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                                 "item": {"playlistid": 0, "position": 0}}
                                self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                                # Fetching all song ids in a list
                                self.single_room_response_dict(okasbox_dict)

                            self.hass.states.set('song_type.room_'+str(self.room_id),self.song_type)

                        elif self.playlistid != None and self.position != None:
                            service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                            "item": {"playlistid": self.playlistid, "position": self.position}}
                            self.hass.services.call("media_player", "kodi_call_method", service_data, False)


                        elif self.movieid != None and self.resume != None:
                            service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                            "item": {"movieid": self.movieid}, "options": {"resume": self.resume}}
                            self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                        elif self.movieid != None:
                            service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                            "item": {"movieid": self.movieid}}
                            self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                        elif self.musicvideoid != None:
                            service_data = {"entity_id": "media_player." + str(kodi_device_id), "udid":self.udid, "method": "Player." + str(kodi_method),
                                            "item": {"musicvideoid": self.musicvideoid}}
                            self.hass.services.call("media_player", "kodi_call_method", service_data, False)

                        _LOGGER.info('Play okas mplayer movie/song')

                        # Okas Mplayer Device Macro
                        macro_service_data_okas_open = {'room_id': self.room_id,
                                                        'device_id': kodi_device_id,
                                                        'device_name': kodi_device_name,
                                                        'service': 'PlayPause',
                                                        'roomwise_device_type_dict': roomwise_device_type_dict,
                                                        'entities_dict': self.entities_dict,
                                                        'okas_mplayer_dict': self.okas_mplayer_dict,
                                                        'udid':self.udid
                                                        }

                        self.device_macro_okas(macro_service_data_okas_open)

        except Exception as error:
            print(_LOGGER.info(traceback.format_exc()))
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})

    def output_handler(self, roomwise_device_type_dict):
        try:
            _LOGGER.info('Controlling output device')
            attributes_dict=  {}

            if 'avp' in roomwise_device_type_dict.keys():
                if self.songid == None and self.playlistid == None:
                    #If output device is a projector
                    if 'projector' in roomwise_device_type_dict.keys():
                        output_device_name = roomwise_device_type_dict['projector']['device_name']
                        output_device_id = roomwise_device_type_dict['projector']['device_id']
                        entity_id = str(output_device_name)+'.'+str(output_device_id)
                        service_data = {"entity_id":entity_id}

                        DOMAIN = output_device_name
                        SERVICE = 'turn_on'

                        #Turning power on for benq_projector: Checks if device is already on or not
                        if self.hass.states.get(entity_id) == None:
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(10)
                        elif self.hass.states.get(entity_id) != None and (self.hass.states.get(entity_id).state) != 'on':
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(10)

                        # Call device macro if present.
                        for room_info in self.si_data['room']:
                            if room_info['room_id'] == self.room_id:
                                for device in room_info['device']:
                                    for devicemacro in device['devicemacro']:
                                        if len(devicemacro) > 0:
                                            macro_service_data = {'room_id': self.room_id,
                                              'device_id': output_device_id,
                                              'device_name': output_device_name,
                                              'service': SERVICE,
                                              'roomwise_device_type_dict': roomwise_device_type_dict,
                                              'entities_dict': self.entities_dict,
                                              'okas_mplayer_dict': self.okas_mplayer_dict,
                                              'device': device,
                                              'devicemacro': devicemacro
                                                  }
                                            self.device_macro(macro_service_data)


                        #Logic to switch the source in projector to HDMI1/HDMI2
                        if len(roomwise_device_type_dict['projector']['deviceparams']) > 0:
                           # DOMAIN = output_device_name
                            device_param_values = roomwise_device_type_dict['projector']['deviceparams'][0]
                            config_param_values = device_param_values['config_param']
                            output_source = config_param_values['inputPort']

                            #check the service name properly and whether output source name needs to be changed to lower case
                            SERVICE = 'select_source'
                            service_data = {"entity_id": entity_id,"source":output_source.lower()}

                            # verifying if state object is not present
                            if self.hass.states.get(entity_id) == None:
                                self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                            # verifying if state object is present and attributes are not present
                            elif bool(self.hass.states.get(entity_id).attributes) == False:
                                self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                            # verifying if state object and attributes are present and operation is different
                            elif (self.hass.states.get(entity_id).attributes['operation']) != output_source.lower():
                                self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                    #If output device is TV, power on the TV(source swithcing not handled here)
                    if 'tv' in roomwise_device_type_dict.keys():
                        output_device_name = roomwise_device_type_dict['tv']['device_name']
                        output_device_id = roomwise_device_type_dict['tv']['device_id']
                        entity_id = str(output_device_name) + '.' + str(output_device_id)
                        service_data = {"entity_id": entity_id}
                        DOMAIN = output_device_name
                        SERVICE = 'power_on'

                        # Call device macro if present.
                        for room_info in self.si_data['room']:
                            if room_info['room_id'] == self.room_id:
                                for device in room_info['device']:
                                    for devicemacro in device['devicemacro']:
                                        if len(devicemacro) > 0:
                                            macro_service_data = {'room_id': self.room_id,
                                              'device_id': output_device_id,
                                              'device_name': output_device_name,
                                              'service': SERVICE,
                                              'roomwise_device_type_dict': roomwise_device_type_dict,
                                              'entities_dict':self.entities_dict,
                                              'okas_mplayer_dict':self.okas_mplayer_dict,
                                              'device': device,
                                              'devicemacro': devicemacro
                                              }

                                            self.device_macro(macro_service_data)

                        if self.hass.states.get(entity_id) == None:
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(2)
                        elif self.hass.states.get(entity_id) != None and (
                                self.hass.states.get(entity_id).state) != 'on':
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(2)

                        if len(roomwise_device_type_dict['tv']['deviceparams']) > 0:
                                device_param_values = roomwise_device_type_dict['tv']['deviceparams'][0]
                                config_param_values = device_param_values['config_param']
                                output_source = config_param_values['inputPort']

                                SERVICE = 'select_source'

                                if bool(self.hass.states.get(entity_id).attributes) == True and (self.hass.states.get(entity_id).attributes['source']) != output_source:
                                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})

    def source_handler(self, roomwise_device_type_dict):
        try:
            _LOGGER.info('Controlling source handling device')
            attributes_dict = {}

            #In the roomwise devices dictionary if we change the keys to be the device_type instead of device-name we can check on behalf of the avp.
            #Source handling through the avp, if present
            if 'avp' in roomwise_device_type_dict.keys():
                handler_device_id = roomwise_device_type_dict['avp']['device_id']
                handler_device_name = roomwise_device_type_dict['avp']['device_name']
                deviceparams = roomwise_device_type_dict['avp']['deviceparams']
                config_params = deviceparams[0]['config_param']

                for configparams in config_params:
                    source = configparams['inputPort']
                    source_name = configparams['deviceName']

                    #Part of code to turn on the AVP when it is not marantz.
                    if handler_device_name == 'trinov':
                        entity_id = str(handler_device_name) + "." + str(handler_device_id)
                        service_data = {'entity_id': entity_id}
                        DOMAIN = handler_device_name
                        SERVICE = 'power_on'
                        if self.hass.states.get(entity_id) == None:
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(46)
                        elif self.hass.states.get(entity_id).state != 'on':
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            time.sleep(46)

                    if handler_device_name == 'emotiva':
                        entity_id = str(handler_device_name) + "." + str(handler_device_id)
                        service_data = {'entity_id': entity_id}
                        DOMAIN = handler_device_name
                        SERVICE = 'turn_on'
                        if self.hass.states.get(entity_id) == None:
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                        elif self.hass.states.get(entity_id).state != 'on':
                            self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                    #Part to call the source selection API for AVPs.
                    if source_name == self.device_name:
                        DOMAIN = handler_device_name
                        SERVICE = 'select_source'
                        entity_id = str(handler_device_name)+"."+str(handler_device_id)

                        # Select source of avr on zone 2
                        if self.zone is not None:
                           service_data = {'entity_id': entity_id, 'source': source,'zone':self.zone}

                           if self.hass.states.get(entity_id) == None:
                               self.hass.services.call(DOMAIN, SERVICE, service_data, False)


                           elif bool(self.hass.states.get(entity_id).attributes) == True and 'zone2_state' in (self.hass.states.get(entity_id).attributes).keys():
                               if (self.hass.states.get(entity_id).attributes['zone2_state'] == 'off'):
                                   self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                           elif bool(self.hass.states.get(entity_id).attributes) == True and 'zone2_source' in (self.hass.states.get(entity_id).attributes).keys():
                               if (self.hass.states.get(entity_id).attributes['zone2_source'] == None) or (self.hass.states.get(entity_id).attributes['zone2_source'] != source):
                                   self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                        else:
                           service_data = {'entity_id': entity_id, 'source': source}
                           if self.hass.states.get(entity_id) == None:
                               self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                           elif (self.hass.states.get(entity_id).state != 'on'):
                               self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                           elif bool(self.hass.states.get(entity_id).attributes) == True and 'source' in (self.hass.states.get(entity_id).attributes).keys():
                               if (self.hass.states.get(entity_id).attributes['source'] == None) or (
                                       self.hass.states.get(entity_id).attributes['source'] != source):
                                   self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                current_entity_id = str(handler_device_name) + "." + str(handler_device_id)

            # Source handling through the tv, if avp is not present in the room
            elif 'tv' in roomwise_device_type_dict.keys() and 'avp' not in roomwise_device_type_dict.keys():
            # elif 'avp' not in roomwise_device_type_dict.keys():
                handler_device_id = roomwise_device_type_dict['tv']['device_id']
                handler_device_name = roomwise_device_type_dict['tv']['device_name']
                entity_id = str(handler_device_name) + "." + str(handler_device_id)
                service_data = {'entity_id': entity_id}
                DOMAIN = handler_device_name
                SERVICE = 'power_on'

                #If we have request for movie/video/channels etc., turn on the tv
                # if songid!= None or movieid != None or resume != None or musicvideoid != None or channel_name != None:
                if self.hass.states.get(entity_id) == None:
                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                    time.sleep(2)
                elif self.hass.states.get(entity_id) != None and (
                        self.hass.states.get(entity_id).state) != 'on':
                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                    time.sleep(2)

                # Call device macro if present.
                for room_info in self.si_data['room']:
                    if room_info['room_id'] == self.room_id:
                        for device in room_info['device']:
                            for devicemacro in device['devicemacro']:
                                if len(devicemacro) > 0:
                                    macro_service_data = {'room_id': self.room_id,
                                      'device_id': handler_device_id,
                                      'device_name': handler_device_name,
                                      'service': SERVICE,
                                      'roomwise_device_type_dict': roomwise_device_type_dict,
                                      'entities_dict':self.entities_dict,
                                      'okas_mplayer_dict':self.okas_mplayer_dict,
                                      'device': device,
                                      'devicemacro': devicemacro
                                      }

                                    self.device_macro(macro_service_data)


                deviceparams = roomwise_device_type_dict['tv']['deviceparams'][0]
                config_param = deviceparams['config_param']
                for configparams in config_param:
                    source = configparams['inputPort']
                    source_name = configparams['deviceName']

                    if source_name == self.device_name:
                        # Switching sources of tv when no avp is present
                        if handler_device_name == 'panasonic_tv' or 'loewe_tv':
                            DOMAIN = handler_device_name
                            SERVICE = 'select_source'
                            #Check the service data to be sent to panasonic tv
                            service_data = {"entity_id": entity_id, "source": source}

                            # Setting source for TV, when TV is source handler
                            if self.hass.states.get(entity_id) == None:
                                self.hass.services.call(DOMAIN, SERVICE, service_data, False)
                            else:
                                if bool(self.hass.states.get(entity_id).attributes) == True and (self.hass.states.get(entity_id).attributes['source']) != source:
                                    self.hass.services.call(DOMAIN, SERVICE, service_data, False)

                current_entity_id = str(handler_device_name) + "." + str(handler_device_id)

            elif 'tv' not in roomwise_device_type_dict.keys() and 'avp' not in roomwise_device_type_dict.keys():
                handler_device_name = 'media_player'
                handler_device_id = self.okas_mplayer_dict[self.room_id]
                current_entity_id = str(handler_device_name) + "." + str(handler_device_id)

            def source_handle_event(event):
                event_entity_id = event.data.get('entity_id')
                try:
                    if bool(valid_entity_id(event_entity_id)) is not True:
                        _LOGGER.info("Entity ID failed validation")
                        return "Entity ID is not valid"
                except Exception as e:
                    _LOGGER.info(e)

                if str(current_entity_id) == str(event_entity_id):
                    new_event_attributes = event.data.get('new_state').attributes

                    for room_info in self.si_data['room']:
                        if self.room_id == room_info['room_id']:

                            media_bar_dict = {}
                            for device in room_info['device']:
                                device_type_room = device['device_type']
                                media_bar_dict[device_type_room] = {'device_id': device['device_id'],
                                                                    'device_name': device['device_name'],
                                                                    'device_type': device['device_type'],
                                                                    'okasBoxIp': device['okasBoxIp'],
                                                                    'deviceparams': device['deviceparams'],
                                                                    }

                            if 'avp' not in media_bar_dict.keys() and 'tv' not in media_bar_dict.keys():
                                deviceName = 'okas_mplayer'
                                config_params = {}

                            else:
                                if 'avp' in media_bar_dict.keys():
                                    device_params = media_bar_dict['avp']["deviceparams"]

                                    for deviceparam in device_params:
                                        config_params = deviceparam["config_param"]

                                    if 'source' in new_event_attributes.keys():
                                        source = new_event_attributes['source']

                                        if source != None:
                                            for config_param in config_params:
                                                inputPort = config_param["inputPort"]

                                                if inputPort == source:
                                                    deviceName = config_param["deviceName"]

                                        else:
                                            deviceName = None

                                    else:
                                        deviceName = None

                                elif 'avp' not in media_bar_dict.keys() and 'tv' in media_bar_dict.keys():
                                    device_params = media_bar_dict['tv']["deviceparams"]

                                    for deviceparam in device_params:
                                        config_params = deviceparam["config_param"]

                                    if 'source' in new_event_attributes.keys():
                                        source = new_event_attributes['source']

                                        if source != None:
                                            for config_param in config_params:
                                                inputPort = config_param["inputPort"]

                                                if inputPort == source:
                                                    deviceName = config_param["deviceName"]
                                        else:
                                            deviceName = None

                                    else:
                                        deviceName = None

                            attributes_dict = {}
                            state_object = self.hass.states.get('media_bar.room_'+str(self.room_id))
                            if state_object != None:
                                if bool(state_object.attributes) == True:
                                    attributes = state_object.attributes
                                    attributes_dict = dict(attributes)
                                    attributes_dict['input_device'] = deviceName
                                else:
                                    attributes_dict['udid'] = self.udid

                            if self.is_multiroom:
                                for room_id in self.multiroom_id:
                                     self.hass.states.set('media_bar.room_'+str(room_id), 'on', attributes_dict)
                            else:
                                self.hass.states.set('media_bar.room_'+str(self.room_id), 'on', attributes_dict)

            self.hass.bus.listen('state_changed',source_handle_event)
            attributes_dict = {}
            state_object = self.hass.states.get('media_bar.room_'+str(self.room_id))
            if state_object != None:
                if bool(state_object.attributes) == True:
                    attributes = state_object.attributes
                    attributes_dict = dict(attributes)
                    attributes_dict['udid'] = self.udid
                else:
                    attributes_dict['udid'] = self.udid

            if self.is_multiroom:
                for room_id in self.multiroom_id:
                     self.hass.states.set('media_bar.room_'+str(room_id), 'on', attributes_dict)
            else:
                self.hass.states.set('media_bar.room_'+str(self.room_id), 'on', attributes_dict)

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            if self.is_multiroom:
                for room_id in self.multiroom_id:
                     self.hass.states.set('sequence.room_'+str(room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})
            else:
                self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})



    def single_room_handler(self):
        try:
            _LOGGER.info('Controlling devices in a single room')
            attributes_dict = {}
            test_automation = {}
            test_automation['room_id'] = self.room_id
            test_automation['device_name'] = self.device_name

            try:
                self.schema_room_id(self.room_id)
            except Exception as e:
                print(self.room_id)
                print("is not a valid room id")
                _LOGGER.info(e)

            schema_automation = Schema({Optional('room_id'): All(int),
                                        Required('device_name'): All(str, Length(min=1, max=21))})
            try:
                if bool(schema_automation(test_automation)) == True:
                    response_code = '200'
                    response_message = response_codes[response_code]
                    attributes_dict['response_code'] = response_code
                    attributes_dict['response_message'] = response_message
                    self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)


            except Exception as e:
                _LOGGER.info(e)
                response_code = '400'
                response_message = response_codes[response_code]
                attributes_dict['response_code'] = response_code
                attributes_dict['response_message'] = str(e)
                self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

            # Checking if the slave box is up, only then the sequeunce has to be executed.
            slave_host = self.okas_slave_ip_dict[self.room_id]
            try:
                ping_response = os.system("ping -c 1 " + slave_host)
                if ping_response != 0:
                    _LOGGER.info(str(slave_host)+' is down!')
                    attributes_dict['response_code'] = '500'
                    attributes_dict['response_message'] = 'Room is not under the network.'
                    self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)
                    return

                    # sys.exit('Slave box is down,exiting execution')
            except Exception as e:
                _LOGGER.info("Caught an exception while pinging the slavebox ",+str(slave_host))
                _LOGGER.info(e)

            if test_automation['device_name'] == 'tatasky':

                schema_automation_in1 = Schema({Optional('room_id'): All(int),
                                                Required('device_name'): All(str, Length(min=1, max=21)),
                                                Required('channel_name'): All(str, Length(min=1, max=101))})
                test_automation['channel_name'] = self.channel_name

                try:
                    if bool(schema_automation_in1(test_automation)) == True:
                        response_code = '200'
                        response_message = response_codes[response_code]
                        attributes_dict['response_code'] = response_code
                        attributes_dict['response_message'] = response_message
                        self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

                except Exception as e:
                    _LOGGER.info(e)
                    response_code = '400'
                    response_message = response_codes[response_code]
                    attributes_dict['response_code'] = response_code
                    attributes_dict['response_message'] = str(e)
                    self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)


            elif test_automation['device_name'] == 'okas_mplayer':
                schema_automation_in2 = Schema({Optional('room_id'): All(int),
                                                Required('device_name'): All(str, Length(min=1, max=21)),
                                                Optional('songid'): All(int),
                                                Optional('movieid'): All(int),
                                                Optional('musicvideoid'): All(int),
                                                Optional('playlistid'): All(int),
                                                Optional('position'): All(int),
                                                Optional('resume'): All(dict),
                                                Optional('speed'): All(int),
                                                Optional('udid'): All(str, Length(min=1, max=101)),
                                                Optional('song_type'): All(str, Length(min=1, max=21))})

                if self.songid is not None:
                    test_automation['songid'] = self.songid
                if self.movieid is not None:
                    test_automation['movieid'] = self.movieid
                if self.musicvideoid is not None:
                    test_automation['musicvideoid'] = self.musicvideoid
                if self.playlistid is not None:
                    test_automation['playlistid'] = self.playlistid
                if self.position is not None:
                    test_automation['position'] = self.position
                if self.resume is not None:
                    test_automation['resume'] = self.resume
                if self.speed is not None:
                    test_automation['speed'] = self.speed
                if self.udid is not None:
                    test_automation['udid'] = self.udid
                if self.song_type is not None:
                    test_automation['song_type'] = self.song_type

                try:
                    if bool(schema_automation_in2(test_automation)) == True:
                        response_code = '200'
                        response_message = response_codes[response_code]
                        attributes_dict['response_code'] = response_code
                        attributes_dict['response_message'] = response_message
                        self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

                except Exception as e:
                    _LOGGER.info(e)
                    response_code = '400'
                    response_message = response_codes[response_code]
                    attributes_dict['response_code'] = response_code
                    attributes_dict['response_message'] = str(e)
                    self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

            elif test_automation['device_name'] == 'kaleidescape':
                schema_automation_in3 = Schema({Optional('room_id'): All(int),
                                                Required('device_name'): All(str, Length(min=1, max=21))
                                                })

                try:
                    if bool(schema_automation_in3(test_automation)) == True:
                        response_code = '200'
                        response_message = response_codes[response_code]
                        attributes_dict['response_code'] = response_code
                        attributes_dict['response_message'] = response_message
                        self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

                except Exception as e:
                    _LOGGER.info(e)
                    response_code = '400'
                    response_message = response_codes[response_code]
                    attributes_dict['response_code'] = response_code
                    attributes_dict['response_message'] = str(e)
                    self.hass.states.set('sequence.room_'+str(self.room_id), attributes_dict)

            #Roomwise device detail dictionary
            #Roomwise device type is created for managing the source_handler API
            for room_info in self.si_data['room']:
                if self.room_id == room_info['room_id']:
                    roomwise_devices_dict = {}
                    roomwise_device_type_dict = {}
                    for device in room_info['device']:
                        device_name_room = device['device_name']
                        device_type_room = device['device_type']
                        roomwise_devices_dict[device_name_room] = {'device_id': device['device_id'],
                                                              'device_name': device['device_name'],
                                                              'device_type': device['device_type'],
                                                              'okasBoxIp': device['okasBoxIp'],
                                                              'deviceparams': device['deviceparams'],
                                                              }
                        roomwise_device_type_dict[device_type_room] = {'device_id': device['device_id'],
                                                              'device_name': device['device_name'],
                                                              'device_type': device['device_type'],
                                                              'okasBoxIp': device['okasBoxIp'],
                                                              'deviceparams': device['deviceparams'],
                                                              }

                    if len(room_info['device']) > 0:
                        for device in room_info['device']:
                            kodi_device_id = self.okas_mplayer_dict[self.room_id]
                            kaleidescape_device_id = None
                    else:
                        kodi_device_id = self.okas_mplayer_dict[self.room_id]
                        kaleidescape_device_id = None

                    for device in room_info['device']:
                        if device['device_name'] == 'kaleidescape':
                            kaleidescape_device_id = device['device_id']
                            break

                    self.output_handler(roomwise_device_type_dict)
                    self.input_handler(roomwise_device_type_dict, kodi_device_id, kaleidescape_device_id)
                    self.source_handler(roomwise_device_type_dict)

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})

    def multiroom_handler(self):
        try:
            _LOGGER.info('Controlling devices in multiroom')

            if self.multiroom_id != None:
                for rooms in self.multiroom_id:
                    for room_info in self.si_data['room']:

                        if rooms == room_info['room_id']:
                            roomwise_devices_dict = {}
                            roomwise_device_type_dict = {}
                            for device in room_info['device']:
                                device_name_room = device['device_name']
                                device_type_room = device['device_type']
                                roomwise_devices_dict[device_name_room] = {'device_id': device['device_id'],
                                                                      'device_name': device['device_name'],
                                                                      'device_type': device['device_type'],
                                                                      'okasBoxIp': device['okasBoxIp'],
                                                                      'deviceparams': device['deviceparams'],
                                                                      }
                                roomwise_device_type_dict[device_type_room] = {'device_id': device['device_id'],
                                                                      'device_name': device['device_name'],
                                                                      'device_type': device['device_type'],
                                                                      'okasBoxIp': device['okasBoxIp'],
                                                                      'deviceparams': device['deviceparams'],

                                                                  }

                            if len( room_info['device'] ) > 0 :
                                for device in room_info['device'] :
                                    kodi_device_id = self.okas_mplayer_dict[rooms]
                                    kaleidescape_device_id = None
                            else :
                                kodi_device_id = self.okas_mplayer_dict[rooms]
                                kaleidescape_device_id = None

                    _LOGGER.info("Calling source handler for both rooms of multiroom")
                    _LOGGER.info(rooms)
                    self.source_handler(roomwise_device_type_dict)

                self.input_handler(roomwise_device_type_dict, kodi_device_id, kaleidescape_device_id)
            else:
                self.input_handler({}, None, None)

        except Exception as error:
            _LOGGER.info(traceback.format_exc())
            self.hass.states.set('sequence.room_'+str(self.room_id), {'response_code':417, 'response_message':'Expectation Failed ERROR: '+str(error)})

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'sequence'

def setup(hass,config):
    """Set up is called when Home Assistant is loading our component."""

    class_obj = SequenceClass()

    def single_handle_event(event):
        global playerid

        event_entity_id = event.data.get('entity_id')

        if "media_player." in event_entity_id:
            new_state = event.data.get('new_state').state
            new_event_attributes = event.data.get('new_state').attributes

            if 'players' in new_event_attributes:
                if len(new_event_attributes['players']) > 0:
                    playerid = new_event_attributes['players'][0]['playerid']

            if 'item' in new_event_attributes:
                if 'id' in new_event_attributes['item']:
                    new_song_id = new_event_attributes['item']['id']

                    if event.data.get('old_state') != None:
                        old_event_attributes = event.data.get('old_state').attributes

                        if 'item' in old_event_attributes:
                            if 'id' in old_event_attributes['item']:
                                old_song_id = old_event_attributes['item']['id']
                            else:
                                old_song_id = None
                        else:
                            old_song_id = None
                    else:
                        old_song_id = None

                    if (new_state == 'playing') and (new_song_id != old_song_id):

                        if class_obj.response_dict:

                            current_song_id = new_event_attributes['item']['id']
                            if 'data' in class_obj.response_dict:
                                main_list = class_obj.response_dict['data']
                                songid = main_list[main_list.index(current_song_id)+1]
                            else:
                                songid = class_obj.response_dict[class_obj.response_dict.index(current_song_id)+1]

                            _LOGGER.info(songid)

                            # Adding next song in queue
                            service_data = {"entity_id": event_entity_id, "udid":class_obj.udid, "method": "Playlist.Add","playlistid": 0 ,"item":{"songid":songid}}
                            hass.services.call("media_player", "kodi_call_method", service_data, False)

    hass.bus.listen('state_changed',single_handle_event)

    with open(home + '/si_data.json') as data_file:
        data = json.loads(data_file.read())

    si_data = dict(data)
    okasbox_dict = {}
    for okasbox in si_data['okasBox']:
        okasbox_dict[okasbox['room']] = okasbox['okasboxip']

    global response_codes
    response_codes = {'200': 'Success', '204': 'No Content in Request', '400': 'Bad Request',
                      '404': 'Request Not Found', '500': 'Internal Server Error'}
    schema_device_name = Schema(str, Length(min=1, max=21))
    schema_device_id = Schema(str, Length(min=1, max=21))
    schema_room_id = Schema(int)

    schema_service_data = Schema({Optional('room_id'): All(int),
                                  Required('device_name'): All(str, Length(min=1, max=21)),
                                  Optional('channel_name'): All(str, Length(min=1, max=101)),
                                  Optional('songid'): All(int),
                                  Optional('movieid'): All(int),
                                  Optional('musicvideoid'): All(int),
                                  Optional('playlistid'): All(int),
                                  Optional('position'): All(int),
                                  Optional('resume'): All(dict),
                                  Optional('speed'): All(int),
                                  Optional('kodi_device_id'): All(str, Length(min=1, max=21)),
                                  Optional('kaleidescape_device_id'): All(str, Length(min=1, max=21)),
                                  Optional('roomwise_device_type_dict'): All(dict),
                                  Optional('entities_dict'): All(dict),
                                  Optional('okas_mplayer_dict'): All(dict),
                                  Optional('udid'): All(str, Length(min=1, max=101)),
                                  Optional('song_type'): All(str, Length(min=1, max=21))
                                  })

    entities_dict = {}
    room_dict = {}
    for room_info in si_data['room']:
        room_id = room_info['room_id']
        room_name = room_info['room_name']
        room_dict[room_id] = room_name
        for device in room_info['device']:
            device_name = device['device_name']
            device_id = device['device_id']
            try:
                schema_device_name(device_name)

            except Exception as e:
                print(device_name + "is not a valid device name")
                _LOGGER.info(e)

            try:
                schema_device_id(device_id)
            except Exception as e:
                print(device_id + "is not a valid device id", )
                _LOGGER.info(e)

            if bool(schema_device_name(device_name)) == True and bool(schema_device_id(device_id)) == True:
                entities_dict[device_id] = device_name

    #this will update the entities dict with utilities like coolmaster and knx etc.
    for utilities in si_data['utility']:
        try:
            schema_device_name(utilities['device_name'])
        except Exception as e:
            print(utilities['device_name'] + "is not a valid device name")
            _LOGGER.info(e)

        try:
            schema_device_id(utilities['device_id'])
        except Exception as e:
            print(utilities['device_id'] + "is not a valid device id", )
            _LOGGER.info(e)
        if bool(schema_device_name(utilities['device_name'])) == True and bool(
                schema_device_id(utilities['device_id'])) == True:
            entities_dict.update({utilities['device_id']: utilities['device_name']})

    #retrieving Coolmaster's unit id/device_id for each room for media/room off APIs
    coolmaster_unit_dict = {}
    for room_info in si_data['room']:
        room_id = room_info['room_id']
        if 'utility_params_list' in room_info.keys():
            for utility_params_list in room_info["utility_params_list"]:
                for utility_param in utility_params_list:
                    if 'config_param' in utility_param:
                        config_params = utility_param["config_param"]
                        device_type = utility_param["device_type"]
                        if device_type == "thermostat":
                            for config_param in config_params:
                                unit_id = config_param['unit_id']
                                device_id = "device_" + unit_id
                                coolmaster_unit_dict[room_id] = device_id

    # Dictionary to fetch kodi device_id as required
    okas_boxes = si_data['okasBox']
    okas_mplayer_dict = {}
    okas_slave_ip_dict = {}
    for kodi in okas_boxes:
        okas_box_mplayer_id = kodi['okasbox_id']
        okas_box_type = kodi['okasboxtype']
        okas_box_room_id = kodi['room']
        okas_box_ip = kodi['okasboxip']
        # if okas_box_type != 'Master Controller':
        if okas_box_type != 'master':
            okas_mplayer_dict[okas_box_room_id] = okas_box_mplayer_id
            okas_slave_ip_dict[okas_box_room_id] = okas_box_ip

    def automation(call):
        #Initializing the variables as None
        channel_name = None
        songid = None
        movieid = None
        musicvideoid = None
        playlistid = None
        position = None
        resume=None
        speed = None
        room_id=None
        device_name=None

        room_id = call.data.get('room_id')
        multiroom_id = call.data.get('multiroom_id')
        device_name = call.data.get('device_name')
        channel_name = call.data.get('channel_name')
        songid = call.data.get('songid')
        movieid = call.data.get('movieid')
        musicvideoid = call.data.get('musicvideoid')
        playlistid = call.data.get('playlistid')
        position = call.data.get('position')
        resume = call.data.get('resume')
        speed = call.data.get('speed')
        udid = call.data.get('udid')
        song_type = call.data.get('song_type')
        action = call.data.get('action')
        zone = call.data.get('zone')
        is_multiroom = call.data.get('is_multiroom')

        class_obj.api_params(
                            schema_room_id,
                            si_data,
                            entities_dict,
                            okas_mplayer_dict,
                            okas_slave_ip_dict,
                            hass,
                            room_id,
                            multiroom_id,
                            device_name,
                            channel_name,
                            songid,
                            movieid,
                            musicvideoid,
                            playlistid,
                            position,
                            resume,
                            speed,
                            udid,
                            song_type,
                            action,
                            zone,
                            is_multiroom
                        )

        if is_multiroom:
            class_obj.multiroom_handler()
        else:
            class_obj.single_room_handler()


    # API for media off, room_off
    def media_room_off(call):
        # Dictionary to fetch kodi device_id as required
        incoming_macro_id = call.data.get("macro_id")
        attributes_dict = {}
        schema_macro_id = Schema({Required('macro_id'): All(int)})
        validate_macro_id = {'macro_id': incoming_macro_id}

        schema_macro_off_service_data = Schema({Required('room_id'): All(int),
                                                Required('device_id'): All(str, Length(min=1, max=21)),
                                                Required('device_name'): All(str, Length(min=1, max=21)),
                                                Optional('entities_dict'): All(dict),
                                                Optional('okas_mplayer_dict'): All(dict)
                                                })

        try:
            if bool(schema_macro_id(validate_macro_id)) is True:
                response_code = '200'
                response_message = response_codes[response_code]
                attributes_dict['response_code'] = response_code
                attributes_dict['response_message'] = response_message
                hass.states.set('sequence.room_'+str(room_id), attributes_dict)

        except Exception as e:
            _LOGGER.info(e)
            response_code = '400'
            response_message = response_codes[response_code]
            attributes_dict['response_code'] = response_code
            attributes_dict['response_message'] = str(e)
            hass.states.set('sequence.room_'+str(room_id), attributes_dict)

        for room_info in si_data['room']:
            current_room_id = room_info['room_id']
            if room_info['room_type'] != 'subroom':
                kodi_device_id = okas_mplayer_dict[current_room_id]
            for room_macros in room_info['roomMacro']:
                macro_type = room_macros['macro_type']
                macro_name = room_macros['macro_name']
                macro_id = room_macros['macro_id']
                macro_off_param_list = room_macros['macro_param']
                macro_off_actions_list = room_macros['macro_actions']
                macro_off_condition_room_id = macro_off_param_list['roomID']
                if macro_off_condition_room_id == current_room_id and incoming_macro_id == macro_id:
                    for macro_off_actions in macro_off_actions_list:
                        macro_off_room_id = macro_off_actions['roomID']
                        macro_off_service = macro_off_actions['service']
                        macro_off_device_number = macro_off_actions['deviceType']

                        # if 'mood' or 'okas_mplayer' or 'cover' not in macro_off_device_number:
                        if macro_off_device_number == 'mood':
                            _LOGGER.info('Execute mood')
                            MACRO_DOMAIN = "custom_scene"
                            MACRO_SERVICE = 'select_scene'
                            entity_id = MACRO_DOMAIN + ".room_" + str(macro_off_condition_room_id)
                            mood_id = "mood_"+str(macro_off_service)
                            scene_name = "scene." + str(mood_id)
                            mood_service_data = {"entity_id": entity_id, "scene_name": scene_name}
                            hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, mood_service_data, False)

                        elif macro_off_device_number == 'okas_mplayer':
                            kodi_method = str(macro_off_service)
                            OKAS_STOP_DOMAIN = 'media_player'
                            OKAS_STOP_SERVICE = 'kodi_call_method'
                            okas_stop_service_data = {"entity_id": "media_player." + str(kodi_device_id),"udid":'',
                                                      "method": "Player."+str(kodi_method), "playerid": playerid}
                            _LOGGER.info('stop okas_mplayer')
                            hass.services.call(OKAS_STOP_DOMAIN, OKAS_STOP_SERVICE, okas_stop_service_data, False)

                        #Cover action on device_off
                        elif 'configure' in str(macro_off_device_number):
                            _LOGGER.info('Execute covers')
                            MACRO_DOMAIN = "cover"
                            MACRO_SERVICE = str(macro_off_service)
                            entity_id = MACRO_DOMAIN + "." + str(macro_off_device_number)
                            service_data = {"entity_id": entity_id}
                            hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)

                        else:
                            macro_off_device_id = 'device_' + str(macro_off_device_number)
                            macro_off_device_name = entities_dict[macro_off_device_id]

                            if 'media_stop' in macro_off_service:
                                _LOGGER.info('Media stop in kaleidescape')
                                kaleidescape_device_id = 'device_' + str(macro_off_device_number)

                                # Call kaleidescape stop API
                                kaleidescape_entity_id = "kaleidescape." + str(kaleidescape_device_id)
                                kaleidescape_service_data = {"entity_id": kaleidescape_entity_id}
                                KALEIDESCAPE_STOP_DOMAIN = 'kaleidescape'
                                KALEIDESCAPE_STOP_SERVICE = 'media_stop'
                                hass.services.call(KALEIDESCAPE_STOP_DOMAIN, KALEIDESCAPE_STOP_SERVICE,
                                                   kaleidescape_service_data, False)

                            #Coolmaster entry for coolmaster off on room_off
                            elif 'cool_master' in macro_off_device_name:
                                _LOGGER.info('Turn off cool_master thermostat')
                                cool_master_device_unit_id = coolmaster_unit_dict[macro_off_room_id]
                                DOMAIN = macro_off_device_name
                                SERVICE = macro_off_service
                                entity_id = DOMAIN+'.'+cool_master_device_unit_id
                                service_data = {'entity_id' : entity_id }
                                hass.services.call(DOMAIN,SERVICE, service_data, False)

                            else:
                                _LOGGER.info('Turn off device')
                                entity_id = str(macro_off_device_name) + '.' + str(macro_off_device_id)
                                macro_off_service_data = {'entity_id': entity_id}
                                if hass.states.get(entity_id) != None and (hass.states.get(entity_id).state) != 'off':
                                    hass.services.call(macro_off_device_name, macro_off_service, macro_off_service_data,
                                                       False)

    def button_macro(call):
        macro_id = call.data.get("macro_id")
        attributes_dict = {}
        schema_macro_id = Schema({Required('macro_id'): All(int)})
        validate_macro_id = {'macro_id': macro_id}

        try:
            if bool(schema_macro_id(validate_macro_id)) is True:
                response_code = '200'
                response_message = response_codes[response_code]
                attributes_dict['response_code'] = response_code
                attributes_dict['response_message'] = response_message
                hass.states.set('sequence.room_'+str(room_id), attributes_dict)

        except Exception as e:
            _LOGGER.info(e)
            response_code = '400'
            response_message = response_codes[response_code]
            attributes_dict['response_code'] = response_code
            attributes_dict['response_message'] = str(e)
            hass.states.set('sequence.room_'+str(room_id), attributes_dict)

        _LOGGER.info('Executing button macro')
        for macro_info in si_data['Macros']:
            if macro_info['macro_id'] == macro_id:
                for macro_action in macro_info['macro_actions']:
                    SERVICE = macro_action['service']
                    device_type = macro_action['deviceType']
                    action_room_id = macro_action['roomID']
                    action_room_id = action_room_id - 1
                    macro_action_room_id = 'room_' + str(action_room_id)

                    if 'mood' in str(device_type):
                        MACRO_DOMAIN = "custom_scene"
                        MACRO_SERVICE = 'select_scene'
                        entity_id = MACRO_DOMAIN + "." + macro_action_room_id
                        scene_name = "scene.mood_" + str(SERVICE)
                        service_data = {"entity_id": entity_id, "scene_name": scene_name}
                        hass.services.call(MACRO_DOMAIN, MACRO_SERVICE, service_data, False)
                    elif ('CBL/SAT' in SERVICE) or ('Blu-ray' in SERVICE) or ('Media Player') in SERVICE:
                        SERVICE = macro_action['service']
                        macro_action_device_id = 'device_' + str(macro_action['deviceType'])
                        MACRO_DOMAIN = entities_dict[macro_action_device_id]
                        service_data = {"entity_id": str(MACRO_DOMAIN) + '.' + str(macro_action_device_id),
                                        "source":SERVICE}
                        entity_id = str(MACRO_DOMAIN) + '.' + str(macro_action_device_id)
                        if hass.states.get(entity_id) == None or hass.states.get(entity_id).state != 'on':
                            hass.services.call(MACRO_DOMAIN, 'select_source', service_data, False)
                    else:
                        SERVICE = macro_action['service']
                        macro_action_device_id = 'device_' + str(macro_action['deviceType'])
                        MACRO_DOMAIN = entities_dict[macro_action_device_id]
                        service_data = {"entity_id": str(MACRO_DOMAIN) + '.' + str(macro_action_device_id)}
                        entity_id = str(MACRO_DOMAIN) + '.' + str(macro_action_device_id)
                        if hass.states.get(entity_id) == None or hass.states.get(entity_id).state != 'on':
                            hass.services.call(MACRO_DOMAIN, SERVICE, service_data, False)

    def get_device_status(call):
        _LOGGER.info('Get device status of all rooms')
        room_config = si_data["room"]
        event_list = []

        for room_info in room_config:
            room_id = room_info["room_id"]
            light_state_list = []
            cool_master_state_list = []
            device_info = []

            for utilities in room_info["utility_params_list"]:
                for utility in utilities:
                    device_type = utility["device_type"]
                    configure_id = utility["configure_id"]
                    if device_type == "light":
                        state_object = hass.states.get(str(device_type) + "." + str(configure_id))
                        if state_object != None:
                            light_state_list.append(state_object.state)
                        else:
                            light_state_list.append('off')

                    elif device_type == "thermostat":
                        for config_param in utility['config_param']:
                            unit_id = config_param['unit_id']
                            device_id = "device_" + unit_id
                            state_object = hass.states.get("cool_master." + str(device_id))
                            if state_object != None:
                                cool_master_state_list.append(state_object.state)
                            else:
                                cool_master_state_list.append('off')

            source = None
            deviceName = None
            state = 'off'

            for device in room_info["device"]:
                if device['device_name'] == "marantz" or device['device_name'] == "trinov" or device['device_name'] == "loewe_tv":
                    device_name = device.get("device_name")
                    device_id = device.get("device_id")
                    state_object = hass.states.get(str(device_name) + "." + str(device_id))

                    if state_object != None:
                        state = state_object.state
                        if bool(state_object.attributes) == True:
                            attributes = state_object.attributes
                            if 'source' in attributes.keys():
                                source = attributes["source"]

                                for deviceparam in device["deviceparams"]:
                                    for config_param in deviceparam["config_param"]:
                                        if config_param["inputPort"] == source:
                                            deviceName = config_param["deviceName"]
                            else:
                                source = None
                    else:
                         state = 'off'
                         source = None
                         deviceName = None

            if 'on' in light_state_list:
                device_info.append({'device_type':'light','device_name':'knx','state':'on'})

            if 'on' in cool_master_state_list:
                device_info.append({'device_type':'thermostat','device_id':device_id,'device_name':'cool_master','state':'on'})

            if state == 'on':
                device_info.append({'device_name':'source_handler','device_id':device_id, 'input_device_name':deviceName,'state':state})
            event_list.append({'room_id':room_id,'devices':device_info})

        event_data = {'room':event_list}
        hass.bus.fire('get_entity_id_result', event_data=event_data)

    hass.services.register(DOMAIN, 'automation', automation)
    hass.services.register(DOMAIN, 'button_macro', button_macro)
    hass.services.register(DOMAIN, 'get_device_status', get_device_status)
    hass.services.register(DOMAIN, 'media_room_off', media_room_off)


    return True





