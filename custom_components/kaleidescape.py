import time, math, socket, struct, time, threading
import telnetlib
from builtins import dict, enumerate

from homeassistant.helpers import entity


class kaleidescape_class:


    def create_connection(self, device_id, ip, port):

        try:
            print("connection establishing")
            telnetObj = telnetlib.Telnet(ip, port)
            dict_telnet[device_id] =  telnetObj
            print("Printing the connection objects below")
            print(dict_telnet)
            return dict_telnet
        except Exception as error:
            print('Failed to create connection. Error message: ' + str(error))

    def transmit(self, command, object):
        try:
            print("command sent")
            object.write(command)
            print(command)
            time.sleep(1)
        except Exception as error:
            print ('Failed to transmit command. Error message: ' + str(error))
            print("Trying to reconnect")
            self.create_connection(device_id, ip, port)
            return

    # def receive(self,read_until_command, object):
    #     try:
    #         response = object.read_until(read_until_command, timeout=0.5)
    #         if response:
    #             print("response_list")
    #             print(response)
    #             return response
    #     except Exception as error:
    #         print ('Failed to receive command. Error message: ' + str(error))

    # def close_connection(self, object):
    #     try:
    #         object.close()
    #         print("connection closed")
    #     except Exception as error:
    #         print ('Failed to close connection. Error message: ' + str(error))
    #         return

# The domain of your component. Should be equal to the name of your component.
DOMAIN = 'kaleidescape'
ATTR_NAME='entity_id'

def setup(hass, config):
    """Set up is called when Home Assistant is loading our component."""

    global ip, port,device_id, dict_telnet, dict_conn, class_obj, entity_id
    kaleidescape_configs = config.get('kaleidescape')
    print("kaleidescape configs")
    print(kaleidescape_configs)
    dict_telnet = {}
    class_obj = kaleidescape_class()
    for config in kaleidescape_configs:
        ip = config['ip']
        port = config['port']
        device_id = config['device_id']
        unit_id=config['unit_id']
        entity_id= 'kaleidescape.'+str(device_id)
        class_obj.create_connection(device_id, ip, port)
        print(ip)
        print(port)
        print(device_id)
        print(unit_id)


    # def go_movie_list(call):
    #     entity_id = call.data.get('entity_id')
    #     object = dict_telnet[device_id]
    #     # entity_id = ('kaleidescape.' + str(device_id))
    #     value=class_obj.transmit((str(unit_id)+'/7/GO_MOVIE_LIST:\r').encode(), object)
    #     print("command to transmit")
    #     print(value)
    #     # response_list = class_obj.receive((str(device_id)+'/'+str(unit_id)+'/GO_MOVIE_LIST:\r').encode(), object)

    def go_movie_list(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/7/GO_MOVIE_LIST:\r').encode(), object)

    def go_music_list(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/7/GO_MUSIC_LIST:\r').encode(), object)

    def go_movie_covers(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/8/GO_MOVIE_COVERS:\r').encode(), object)

    def go_music_covers(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/8/GO_MUSIC_COVERS:\r').encode(), object)

    def shuffle_cover_art(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/SHUFFLE_COVER_ART:\r').encode(), object)

    def play(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/3/PLAY:\r').encode(), object)


    def pause(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/4/PAUSE:\r').encode(), object)
        # entity_id = ('kaleidescape.' + str(device_id))

    def stop(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/STOP:\r').encode(), object)

    def media_previous(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/PREVIOUS:\r').encode(), object)

    def media_next(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/NEXT:\r').encode(), object)

    def scan_forward(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/SCAN_FORWARD:\r').encode(), object)

    def scan_reverse(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/6/SCAN_REVERSE:\r').encode(), object)


    def right(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/1/RIGHT:\r').encode(), object)
        # class_obj.transmit((str(unit_id)+'/2/RIGHT_RELEASE:\r').encode(), object)

    def left(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/1/LEFT:\r').encode(), object)
        # class_obj.transmit((str(unit_id)+'/2/LEFT_RELEASE:\r').encode(), object)

    def up(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/1/UP:\r').encode(), object)
        # class_obj.transmit((str(unit_id)+'/2/UP_RELEASE:\r').encode(), object)


    def down(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/1/DOWN:\r').encode(), object)
        # class_obj.transmit((str(unit_id)+'/2/DOWN_RELEASE:\r').encode(), object)

    def select(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/0/SELECT:\r').encode(), object)

    def filter_list(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/0/FILTER_LIST:\r').encode(), object)

    def search(call):
        entity_id = call.data.get("entity_id")
        character = call.data.get("character")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/0/KEYBOARD_CHARACTER:'+str(character)+':\r').encode(), object)


    def backspace(call):
        entity_id = call.data.get("entity_id")
        print(entity_id)
        print(type(entity_id))
        if isinstance(entity_id, str):
            print("entity is string")
            device_id = (str(entity_id).split("."))[1]
            print(device_id)
        else:
            print("entity is list")
            device_id = (str(entity_id[0]).split("."))[1]
            print(device_id)
            entity_id = entity_id[0]
        object = dict_telnet[device_id]
        for config in kaleidescape_configs:
            device_id = entity_id.split('.')[1]
            if config['device_id'] == device_id:
                 unit_id= config['unit_id']
                 break
        print("object for go movie is given as:")
        print(object)
        class_obj.transmit((str(unit_id)+'/0/BACKSPACE'+':\r').encode(), object)






    hass.services.register(DOMAIN, 'go_movie_list', go_movie_list)
    hass.services.register(DOMAIN, 'go_music_list', go_music_list)
    hass.services.register(DOMAIN, 'go_movie_covers', go_movie_covers)
    hass.services.register(DOMAIN, 'go_music_covers', go_music_covers)
    hass.services.register(DOMAIN, 'shuffle_cover_art', shuffle_cover_art)
    hass.services.register(DOMAIN, 'play', play)
    hass.services.register(DOMAIN, 'pause', pause)
    hass.services.register(DOMAIN, 'stop', stop)
    hass.services.register(DOMAIN, 'media_previous', media_previous)
    hass.services.register(DOMAIN, 'media_next', media_next)
    hass.services.register(DOMAIN, 'scan_forward', scan_forward)
    hass.services.register(DOMAIN, 'scan_reverse', scan_reverse)
    hass.services.register(DOMAIN, 'right', right)
    hass.services.register(DOMAIN, 'left', left)
    hass.services.register(DOMAIN, 'up', up)
    hass.services.register(DOMAIN, 'down', down)
    hass.services.register(DOMAIN, 'select', select)
    hass.services.register(DOMAIN, 'filter_list', filter_list)
    hass.services.register(DOMAIN, 'search', search)
    hass.services.register(DOMAIN, 'backspace', backspace)

    #
    #Return boolean to indicate that initialization was successfully.
    return True
